unit ufrmProjectManage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxCalendar, cxLabel,
  RzButton, RzPanel, Vcl.ExtCtrls, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxClasses, cxGridCustomView, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxGrid, Data.Win.ADODB, cxDBLookupComboBox,ufrmBaseController,
  Vcl.StdCtrls, Vcl.Menus, Datasnap.Provider, Datasnap.DBClient, cxLookupEdit,
  cxDBLookupEdit;

type
  TfrmProjectManage = class(TfrmBaseController)
    RzToolbar10: TRzToolbar;
    RzSpacer77: TRzSpacer;
    RzBut4: TRzToolButton;
    RzSpacer78: TRzSpacer;
    RzBut5: TRzToolButton;
    RzSpacer79: TRzSpacer;
    RzBut6: TRzToolButton;
    RzSpacer80: TRzSpacer;
    RzBut12: TRzToolButton;
    tvView: TcxGridDBTableView;
    Lv: TcxGridLevel;
    Grid: TcxGrid;
    tvViewCol1: TcxGridDBColumn;
    tvViewCol2: TcxGridDBColumn;
    tvViewCol3: TcxGridDBColumn;
    tvViewCol4: TcxGridDBColumn;
    tvViewCol5: TcxGridDBColumn;
    RzSpacer1: TRzSpacer;
    cxLabel1: TcxLabel;
    RzToolButton1: TRzToolButton;
    RzPanel1: TRzPanel;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    qry: TADOQuery;
    ds: TDataSource;
    StatusBar1: TStatusBar;
    RzSpacer4: TRzSpacer;
    RzSpacer5: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzSpacer7: TRzSpacer;
    Old: TPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    qrystatus: TStringField;
    qryCode: TStringField;
    qryparent: TIntegerField;
    qryremarks: TStringField;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    tvViewColumn1: TcxGridDBColumn;
    N1: TMenuItem;
    N2: TMenuItem;
    RzSpacer8: TRzSpacer;
    RzToolButton5: TRzToolButton;
    qryIsDelete: TIntegerField;
    cxLookupComboBox1: TcxLookupComboBox;
    qrySignName: TStringField;
    qryInputDate: TDateField;
    qryProjectCode: TStringField;
    N3: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N6: TMenuItem;
    procedure FormShow(Sender: TObject);
    procedure tvViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzBut4Click(Sender: TObject);
    procedure RzBut12Click(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvViewCol4GetPropertiesForEdit(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AProperties: TcxCustomEditProperties);
    procedure qryAfterInsert(DataSet: TDataSet);
    procedure tvViewKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure MenuItem1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure tvViewCol2PropertiesPopup(Sender: TObject);
    procedure tvViewCol2PropertiesCloseUp(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure cxLookupComboBox1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure N6Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
  private
    { Private declarations }
    IsSave : Boolean; 
    function UpdateIsDelete(AQry : TADOQuery;AFields : string; IsDele : Integer):Boolean;
    function GetTableData(AQry : TADOQuery ; IsDelete : Integer ):Boolean;
  public
    { Public declarations }
    //g_ModuleIndex : Integer;
    IsEdit : Boolean;
  end;

var
  frmProjectManage: TfrmProjectManage;

implementation

uses
   global,uDataModule,ufrmProjectFinance,ufunctions;

{$R *.dfm}

function TfrmProjectManage.UpdateIsDelete(AQry : TADOQuery;AFields : string; IsDele : Integer):Boolean;
begin
  with AQry do 
  begin
    if State <> dsInactive then
    begin
      Edit;
      FieldByName(AFields).AsInteger := IsDele ;
      UpdateBatch();
      Requery();
    end;  
  end;

end;  

procedure TfrmProjectManage.cxLookupComboBox1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
var
  s : string;
  
begin
  inherited;
  if Key = 13 then
  begin
    s := Self.cxLookupComboBox1.Text;
    GridLocateRecord(Self.tvView,Self.qrySignName.FieldName,s);
  end;  
end;

procedure TfrmProjectManage.cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
var
  szRowIndex : Integer;
  szCol1Name : string;

begin
  inherited;
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szCol1Name := DataController.Values[szRowIndex,0];
      GridLocateRecord(Self.tvView,Self.qrySignName.FieldName,szCol1Name);
    end;
  end;

end;

//begin
//  inherited;
  {
  cxLookupComboBox1.Properties.ListSource//数据源
  cxLookupComboBox1.Properties.ListFieldNames//数据源中某字段名称
  cxLookupComboBox1.Properties.KeyFieldNames//数据源中某字段名称对应的ID号
  cxLookupComboBox1.EditValue//读出当前选的内容的ID号
  //-----------------自定义过滤
  incrementalfiltering,=false//,设置成false,不过虑就行,要选择不了.

  if trim(cxLookupComboBox1.EditText)='' then exit;
  //输入空,退出更新列表
  if cxLookupComboBox1.Properties.ListField.AsString=cxLookupComboBox1.EditText then exit;
  //输入和选择列表相同,退出更新列表

  ADOTable1.Filtered:=false;
  ADOTable1.Filter:='name like ''%'+cxLookupComboBox1.EditText+'%''';
  ADOTable1.Filtered:=True;
  }
//end;

procedure TfrmProjectManage.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  if IsSave then
  begin
    if Application.MessageBox( '当前有数据未保存，是否需要退出？', '提示:', MB_OKCANCEL + MB_ICONWARNING) = ID_CANCEL then
    begin
      Self.qry.Close;
      CanClose := False;
    end;
  end;
end;

procedure TfrmProjectManage.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 27  then
  begin
    Close;
  end;
end;

function  TfrmProjectManage.GetTableData(AQry : TADOQuery ; IsDelete : Integer ):Boolean;
begin
  with AQry do
  begin
    Close;
    {
    SQL.Text := 'Select * from '     + g_Table_Projects_Tree  +
                ' AS PT right join ' + g_Table_Project_Status +
                ' AS PS on PT.SignName = PS.ProjectName WHERE PT.PID=0 and IsDelete = '+ IntToStr( IsDelete ) +';' ;
    }
    SQL.Text := 'Select * from ' + g_Table_Projects_Tree  +
                ' WHERE PID = 0 and IsDelete = '+ IntToStr( IsDelete ) +';' ;

    Open;
  end;
end;

procedure TfrmProjectManage.FormShow(Sender: TObject);
begin
//工程状态
//sk_Project_Status
//  GetTableData(Self.qry,0);

  Self.RzToolButton5.Click;

  with Self.ADOQuery1 do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from ' +  g_Table_Maintain_Project;
    Open;
  end;

end;

procedure TfrmProjectManage.MenuItem1Click(Sender: TObject);
begin
  inherited;
  //历史工程
  GetTableData(Self.qry,1);
  Self.RzBut4.Enabled := False;
  Self.RzBut5.Enabled := False;
  Self.RzBut6.Enabled := False;
  Self.RzToolButton4.Enabled := False;
  Self.N1.Enabled := True;
  Self.MenuItem2.Enabled := True;
end;

procedure TfrmProjectManage.MenuItem2Click(Sender: TObject);
var
  szTableList : array[0..20] of TTableList;
  szCodeList  : string;
  szParent : Integer;
  szCode : string;

begin
  inherited;
  //彻底删除
  if Application.MessageBox( '是否需要要彻底删除数据，删除后无法恢复？', '提示:',
  MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    if Self.tvView.Controller.SelectedRowCount = 1 then
    begin
      with Self.tvView.DataController.DataSet do
      begin
        if not IsEmpty then
        begin
          szCode := FieldByName('Code').AsString;
          szParent := FieldByName('parent').AsInteger;
        end;

      end;

      DM.getTreeCode(g_Table_Projects_Tree, szParent,szCodeList); //获取要删除的id表

      szTableList[0].ATableName := g_Table_Project_Worksheet;    //统计考勤
      szTableList[0].ADirectory := g_DirWork;

      szTableList[1].ATableName := g_Table_Project_BuildStorage; //统计仓库
      szTableList[1].ADirectory := g_DirBuildStorage;

      szTableList[2].ATableName := g_Table_Project_Lease; //统计机械
      szTableList[2].ADirectory := g_Dir_Project_Lease;

      szTableList[3].ATableName := g_Table_Project_Sanction;  //统计奖罚
      szTableList[3].ADirectory := g_dir_Sanction;

      szTableList[4].ATableName := g_Table_Project_PactInfo;  //合同信息
      szTableList[4].ADirectory := g_dir_PactInfo;

      szTableList[5].ATableName := g_Table_Project_AllocateBankroll; //拨款申请
      szTableList[5].ADirectory := g_dir_AllocateBankroll;

      szTableList[6].ATableName := g_Table_Project_Survey;  //施工工程概况
      szTableList[6].ADirectory := g_dir_Survey;

      szTableList[7].ATableName := g_Table_Project_EngineeringQuantity;
      szTableList[7].ADirectory := g_Dir_Project_Quantity;

      szTableList[8].ATableName := g_Table_Project_EngineeringDetailed;
      szTableList[8].ADirectory := '';

      szTableList[9].ATableName := g_Table_Project_Mechanics;
      szTableList[9].ADirectory := g_Dir_Project_Mechanics;

      szTableList[10].ATableName := g_Table_Project_BuildFlowingAccount;
      szTableList[10].ADirectory := g_Dir_Project_Finance; //

      szTableList[11].ATableName := g_Table_Project_Tender;  //招标
      szTableList[11].ADirectory := g_Dir_Project_Tender;

      szTableList[12].ATableName := g_Table_Project_Quality; //监督
      szTableList[12].ADirectory := g_Dir_Project_Quality;

      szTableList[13].ATableName := g_Table_Project_Complete;//竣工
      szTableList[13].ADirectory := g_Dir_Project_Complete;

      szTableList[14].ATableName := 'sk_ArchivesManage';
      szTableList[14].ADirectory := 'ArchivesManage';

      szTableList[15].ATableName := g_Table_Maintain_Work;  //合同考勤
      szTableList[15].ADirectory := '';

      szTableList[16].ATableName := g_Table_Maintain_Science;   //合同材料
      szTableList[16].ADirectory := '';

      szTableList[17].ATableName := g_Table_Maintain_SubContract; //合同分包
      szTableList[17].ADirectory := '';

      szTableList[18].ATableName := g_Table_Maintain_Concrete;  //合同混凝土
      szTableList[18].ADirectory := '';

      szTableList[19].ATableName := g_Table_Project_Concrete;  //统计混凝土
      szTableList[19].ADirectory := g_Dir_Project_Concrete;

      szTableList[20].ATableName := g_Table_Project_Concrete_GroupMoney;//混凝土组价格
      szTableList[20].ADirectory := '';

      DeleteTableFile(szTableList,szCodeList);

      DM.InDeleteData(g_Table_Projects_Tree,szCodeList);
      Self.ADOQuery1.Requery();
      Self.qry.Requery();
    end else
    begin
      if Self.tvView.Controller.SelectedRowCount > 1 then
      begin
        Application.MessageBox( '禁止同时删除多条工程，只允许一次一条！', '提示:', MB_OKCANCEL + MB_ICONWARNING) ;
      end;
    end;

  end;

end;

procedure TfrmProjectManage.N1Click(Sender: TObject);
begin
  inherited;
  //恢复工程
  UpdateIsDelete(Self.qry,Self.qryIsDelete.FieldName,0);    
end;

procedure TfrmProjectManage.N4Click(Sender: TObject);
begin
  inherited;
  Self.RzBut6.Click;
end;

procedure TfrmProjectManage.N5Click(Sender: TObject);
begin
  inherited;
  Self.RzBut5.Click;
end;

procedure TfrmProjectManage.N6Click(Sender: TObject);
begin
  inherited;
  Self.RzBut4.Click;
end;

procedure TfrmProjectManage.qryAfterInsert(DataSet: TDataSet);
var
  parent : Integer;
  szColName : string;

begin
  with DataSet do
  begin
    if State <> dsInactive then
    begin
      parent := DM.getLastId(g_Table_Projects_Tree);
      FieldByName('Code').Value   := GetRowCode(g_Table_Projects_Tree,'Code');
      FieldByName('parent').Value := parent;
      szColName := Self.tvViewCol3.DataBinding.FieldName;
      FieldByName(szColName).Value:= Date;
    end;
  end;
end;


procedure TfrmProjectManage.RzBut12Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmProjectManage.RzBut4Click(Sender: TObject);

var
  s : string;
  szRowCount : Integer;
  
begin

  if Sender = RzBut4 then
  begin
    //新增
    with Self.qry do
    begin
      if State <> dsInactive then
      begin
        if State in [dsInsert,dsEdit] then
        begin
          Application.MessageBox( '当前数据还未编辑完成,无法新增！', '提示:', MB_OKCANCEL + MB_ICONWARNING) ;
        end else
        begin

          Append;
          Self.tvView.Columns[2].Focused := True;
          Self.Grid.SetFocus;
          keybd_event(VK_RETURN,0,0,0);
          IsSave := True;
        end;

      end;
    end;   
  end else
  if Sender = RzBut5 then
  begin
    //保存
    {
    IsDeleteEmptyData(Self.qry,
                      g_Table_Projects_Tree ,
                      Self.tvViewCol2.DataBinding.FieldName);
    }
    if  Self.qry.IsEmpty then Exit;

    Self.qry.UpdateBatch(arAll);
    IsSave := false;
    Application.MessageBox( '当前数据保存完成', '提示:', MB_OKCANCEL + MB_ICONWARNING) ;
  end else
  if Sender = RzBut6 then
  begin
    if Application.MessageBox( '是否需要要删除数据，删除后可在历史工程查看已删除数据？', '提示:',
    MB_OKCANCEL + MB_ICONWARNING) = IDOK then
    begin
        szRowCount := Self.tvView.Controller.SelectedRowCount;
        if szRowCount = 1 then
        begin
          UpdateIsDelete(Self.qry,Self.qryIsDelete.FieldName,1);
        end else
        begin
          if szRowCount > 1 then
          begin
            Application.MessageBox( '禁止删除多条工程，只允许一条一条的删除！', '提示:', MB_OKCANCEL + MB_ICONWARNING) ;
          end;
        end;
    end;
  end else
  if Sender = RzToolButton4 then
  begin
    with Self.qry do
    begin
      if State <> dsInactive then
      begin
        Edit;
      end;

    end;

  end else
  if Sender = RzToolButton1 then
  begin
    //搜索
    s := Self.cxLookupComboBox1.Text;
    GridLocateRecord(Self.tvView,Self.qrySignName.FieldName,s);
  end;

end;

procedure TfrmProjectManage.RzToolButton5Click(Sender: TObject);
begin
  inherited;
  GetTableData(Self.qry,0);

  Self.N1.Enabled := False;
  Self.MenuItem2.Enabled := False;
  Self.RzBut4.Enabled := True;
  Self.RzBut5.Enabled := True;
  Self.RzBut6.Enabled := True;
  Self.RzToolButton4.Enabled := True;
end;

procedure TfrmProjectManage.tvViewCol1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmProjectManage.tvViewCol2PropertiesCloseUp(Sender: TObject);
var
  szCol1Name:string;
  szRowIndex:Integer;
  szColName :string;
  szRecordCount : Integer;

begin
  inherited;

  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szCol1Name := DataController.Values[szRowIndex,0];
      szColName := Self.tvViewCol2.DataBinding.FieldName;
      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select count(*) from '+ g_Table_Projects_Tree +' where ' +
                    szColName + '="' + szCol1Name + '"';
        Open;
        if RecordCount > 0 then
        begin
          szRecordCount := Fields[0].AsInteger;
          with Self.qry do
          begin

            if (State = dsInsert) or (State = dsEdit) then
            begin

              if szRecordCount = 0 then
              begin
              //  FieldByName(Self.qryProjectName.FieldName).Value := szCol1Name;
                FieldByName(Self.qrySignName.FieldName).Value := szCol1Name;
              end else
              begin
                FieldByName(Self.qrySignName.FieldName).Value := '';
                Application.MessageBox( '已有相同工程名称', '提示:', MB_OKCANCEL + MB_ICONWARNING) ;
              end;

            end;

          end;

        end;

      end;

    end;

  end;

end;

procedure TfrmProjectManage.tvViewCol2PropertiesPopup(Sender: TObject);
begin
  inherited;
  Self.ADOQuery1.Requery();
end;

procedure TfrmProjectManage.tvViewCol4GetPropertiesForEdit(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AProperties: TcxCustomEditProperties);
begin
// 工程立项中、施工进度中丶主体交工中丶竣工验收中丶审计报告中丶历史档案馆
  with (Sender.Properties AS TcxComboBoxProperties) do
  begin
    Items.Clear;
    Items.Add('工程立项中');
    Items.Add('施工进度中');
    Items.Add('主体验收中');
    Items.Add('竣工验收中');
    Items.Add('审计报告中');
    Items.Add('历史档案馆');
  end;

end;

procedure TfrmProjectManage.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  if AItem.Index = Self.tvViewCol1.Index then
  begin
    AAllow := False;
  end else
  begin

    with Self.qry do
    begin
      if State <> dsInactive then
      begin
        if (State <> dsEdit)   and
           (State <> dsInsert) and
           (AItem.Index <> Self.tvViewCol4.Index)  then AAllow := False;
      end;

    end;

  end;   
   
end;

procedure TfrmProjectManage.tvViewKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzToolButton4.Click;
  end;  
end;

end.
