unit ufrmPurchaseDetailed;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,ufrmBasePurchase, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCheckBox,
  cxCalendar, cxDBLookupComboBox, cxDropDownEdit, cxMemo, cxCurrencyEdit,
  cxSpinEdit, Data.Win.ADODB, Datasnap.DBClient, Vcl.Menus, frxClass, frxDBSet,
  cxDBEdit, Vcl.StdCtrls, cxGroupBox, Vcl.ExtCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxMaskEdit, cxLabel, RzButton, RzPanel, cxButtons,
  Datasnap.Provider, cxLookupEdit, cxDBLookupEdit;

type
  TfrmPurchaseDetailed = class(TfrmBasePurchase)
    RzSpacer2: TRzSpacer;
    RzToolButton1: TRzToolButton;
    procedure FormCreate(Sender: TObject);
    procedure RzToolButton85Click(Sender: TObject);
  private
    { Private declarations }

  public
    { Public declarations }
  end;

var
  frmPurchaseDetailed: TfrmPurchaseDetailed;

implementation

uses
   global,uProjectFrame,ufunctions,uDataModule,ufrmIsViewGrid;

{$R *.dfm}

procedure TfrmPurchaseDetailed.FormCreate(Sender: TObject);
begin
  inherited;
  IsEidt := True;
end;

procedure TfrmPurchaseDetailed.RzToolButton85Click(Sender: TObject);
begin
  inherited;
//
end;

end.
