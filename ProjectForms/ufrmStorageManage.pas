unit ufrmStorageManage;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,ufrmBasePurchase, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB,
  cxDBData, cxTextEdit, cxCheckBox, cxCalendar,
  cxDropDownEdit, cxMemo, cxCurrencyEdit, cxSpinEdit, cxContainer, Vcl.Menus,
  Data.Win.ADODB, cxDBEdit, Vcl.StdCtrls, cxButtons, cxLabel, cxMaskEdit,
  cxGroupBox, Vcl.ExtCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, RzButton, RzPanel,
  cxLookupEdit,  Vcl.ComCtrls, dxCore, cxDateUtils, cxRadioGroup,
  Datasnap.DBClient, cxDBLookupComboBox, cxDBLookupEdit;

type
  TfrmStorageManage = class(TfrmBasePurchase)
    RzPanel5: TRzPanel;
    RzPanel6: TRzPanel;
    RzPanel7: TRzPanel;
    RzPanel8: TRzPanel;
    RzPanel9: TRzPanel;
    RzPanel10: TRzPanel;
    RzPanel11: TRzPanel;
    RzSpacer2: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzPanel12: TRzPanel;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel8: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxDateEdit2: TcxDateEdit;
    cxRadioButton1: TcxRadioButton;
    cxRadioButton2: TcxRadioButton;
    cxButton5: TcxButton;
    cxLabel9: TcxLabel;
    ClientDataSet1: TClientDataSet;
    DataSource1: TDataSource;
    cxLabel10: TcxLabel;
    cxComboBox3: TcxComboBox;
    RzPanel14: TRzPanel;
    RzPanel15: TRzPanel;
    cxLookupComboBox1: TcxLookupComboBox;
    cxLookupComboBox2: TcxLookupComboBox;
    cxLookupComboBox3: TcxLookupComboBox;
    cxLabel11: TcxLabel;
    cxComboBox4: TcxComboBox;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzSpacer4: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer5: TRzSpacer;
    procedure cxButton5Click(Sender: TObject);
    procedure RzToolButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems3GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure cxLookupComboBox2PropertiesCloseUp(Sender: TObject);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxLookupComboBox3PropertiesCloseUp(Sender: TObject);
    procedure cxPopupEdit1PropertiesCloseUp(Sender: TObject);
    procedure cxPopupEdit2PropertiesCloseUp(Sender: TObject);
    procedure cxComboBox3PropertiesCloseUp(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure tvViewColumn4PropertiesInitPopup(Sender: TObject);
    procedure tvViewColumn4PropertiesCloseUp(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure RzPanel5Resize(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    dwTypes : Integer;

    procedure WndProc(var Message: TMessage); override;  // 第一优先权
    function VisibleCaption():string;
    function SelectData() : Boolean;
  end;

var
  frmStorageManage: TfrmStorageManage;



implementation

uses
  global,uDataModule;

{$R *.dfm}

function TfrmStorageManage.SelectData() : Boolean;
var
  szSignName : string;
  szStuffName: string;
  szspec : string;
  szTabTypes : string;
  szTabCategory : string;

  DD1 , DD2 , DD3 , DD4 , DD5 : string;

begin
  szSignName := Self.cxLookupComboBox1.Text;  //供应单位名称
  szStuffName:= Self.cxLookupComboBox2.Text;  //材料名称
  szspec := Self.cxLookupComboBox3.Text;
  szTabTypes := Self.cxComboBox3.Text;
  szTabCategory := Self.cxComboBox4.Text;

  if szSignName <> '' then  DD1 := ' AND '+ Self.tvViewColumn15.DataBinding.FieldName + '="' + szSignName + '"'; //供应单位名称

  if szStuffName <> '' then DD2 := ' AND '+ Self.tvViewColumn4.DataBinding.FieldName + '="' + szStuffName +'"'; //材料名称

  if szspec <> '' then  DD3 := ' AND ' + Self.tvViewColumn5.DataBinding.FieldName + '="' + szspec + '"';

  if szTabTypes <> '' then DD4 := ' AND ' + Self.tvViewColumn16.DataBinding.FieldName + '="' + szTabTypes + '"';

  if szTabCategory <> '' then DD5 := ' AND ' + Self.tvViewColumn26.DataBinding.FieldName + '="' + szTabCategory + '"';

  sqltext := 'Select * from ' + g_Table_Project_BuildStorage +
             ' Where Code ="' +  g_PostCode  + '"' +
             DD1 + DD2 + DD3 + DD4 + DD5;
  with Self.Storage do
  begin
    Close;
    SQL.Text := sqltext;
    Open;
  end;
  VisibleCaption;
end;

function TfrmStorageManage.VisibleCaption():string;
var
  szTabTypes : Variant;
  szCount : Double;

begin
  EnterStorage := 0;
  ComeStorage  := 0;
  ExistStorage := 0;
  ReturnCount  := 0;

  with Self.Storage do
  begin
    if State <> dsInactive then
    begin
      First;
      while not Eof do
      begin
        Application.ProcessMessages;
        szTabTypes := FieldByName(Self.tvViewColumn16.DataBinding.FieldName).Value;
        szCount := FieldByName(Self.tvViewColumn8.DataBinding.FieldName).AsFloat;

        if ( VarToStr( szTabTypes ) = '入库') or (VarToStr(szTabTypes) = '进库') then
        begin
          EnterStorage := EnterStorage + szCount;
        end else
        if VarToStr(szTabTypes) = '出库' then
        begin
          ComeStorage := ComeStorage + szCount;
        end else
        if VarToStr(szTabTypes) = '退还' then
        begin
          ReturnCount := ReturnCount + szCount;
        end;
        Next;
      end;

    end;

  end;
  Self.RzPanel6.Caption := FloatToStr(EnterStorage);
  Self.RzPanel11.Caption:= FloatToStr(ComeStorage);
  Self.RzPanel15.Caption:= FloatToStr(ReturnCount);
  Self.RzPanel9.Caption := FloatToStr(EnterStorage - ComeStorage + ReturnCount);
end;

procedure  TfrmStorageManage.WndProc(var Message: TMessage);
var
  pMsg : PProject;
  szFieldName : string;
  szTotal : string;

begin
  case Message.Msg of
    WM_FrameClose:
    begin

    end;
    WM_FrameView :
    begin
      pMsg := PProject(Message.LParam);
      g_PostCode    := pMsg.dwCode;
      g_ProjectName := pMsg.dwName;
      Self.RzToolButton3.Click;
   //   g_sqlText := 'Select * from ' + g_Table_Project_BuildStorage + ' Where Code ="' +  g_PostCode + '"';
      g_sqlText := 'Select * from ' + g_Table_Project_BuildStorage + ' Where 1=2';
      with Self.Storage do
      begin
        Close;
        SQL.Text := g_sqlText;
        Open;
      end;

      with Self.ADOMaking do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from '+ g_Table_MakingsList ; // + ' Group BY MakingCaption';
        Open;
      end;

      with Self.ADOSpec do
      begin
        Close;
        SQL.Clear;
      //  SQL.Text := 'Select spec from '+ g_Table_MakingsList + ' Group BY spec';
        SQL.Text := 'Select * from unit where chType=7';
        Open;
      end;

      VisibleCaption;
    end;
    WM_FrameDele :
    begin
      pMsg   := PProject(Message.LParam);
    end;
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

procedure TfrmStorageManage.cxButton5Click(Sender: TObject);
var
  szCompanyName : string;
  szMaterialName: string;
  sqltext   : string;
  szColName : string;
  szSpec : string;
  SpecSql: string;

begin
  inherited;
  if Self.cxRadioButton2.Checked then
  begin
    SelectData;
    {
    szCompanyName := Self.cxLookupComboBox1.Text;
    szMaterialName:= Self.cxLookupComboBox2.Text;
    SpecSql:= '';
    szSpec := Self.cxLookupComboBox3.Text;
    if szSpec <> '' then
      SpecSql := ' AND ' + Self.tvViewColumn5.DataBinding.FieldName + '="' + szSpec + '"';

    sqltext := 'Select * from ' + g_Table_Project_BuildStorage +
               ' Where Code ="' +  g_PostCode +
               '"'  +' and ' + Self.tvViewColumn15.DataBinding.FieldName +
               '="' + szCompanyName + '"' +
               ' AND ' + Self.tvViewColumn4.DataBinding.FieldName +
                '="' + szMaterialName +'"' + SpecSql;

    with Self.Storage do
    begin
      Close;
      SQL.Text := sqltext;
      Open;
    end;

    VisibleCaption;
    }
  end else
  if Self.cxRadioButton1.Checked then
  begin
    szColName   := Self.tvViewColumn2.DataBinding.FieldName;

    sqltext := 'Select *from ' + g_Table_Project_BuildStorage + ' Where Code ="' +  g_PostCode + '"' +' and ' + szColName +' between :t1 and :t2';
    SearchDateRange(Self.cxDateEdit1.Text,Self.cxDateEdit1.Text,sqltext,Self.Storage);
    VisibleCaption;
  end;

end;

procedure TfrmStorageManage.cxComboBox3PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  SelectData();
end;

procedure TfrmStorageManage.cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  if Self.cxLookupComboBox1.Text <> '' then
  begin
    SelectData();
  end;
end;

procedure TfrmStorageManage.cxLookupComboBox2PropertiesCloseUp(Sender: TObject);
var
  szColContentA : Variant;
  szColContentB : Variant;
  szColContentC : Variant;
  szColContentD : Variant;
  szRowIndex:Integer;
  szColName : string;
  szCount : Variant;

begin
  inherited;
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
      szColContentA :=  DataController.Values[szRowIndex,0] ; //名称
      szColContentD :=  DataController.Values[szRowIndex,2] ; //规格
      if szColContentA <> null then
      begin
        Self.cxLookupComboBox2.EditValue := szColContentA;
        Self.cxLookupComboBox3.EditValue := szColContentD;
      end;

    end;

  end;

  if Self.cxLookupComboBox2.Text <> '' then SelectData();
end;

procedure TfrmStorageManage.cxLookupComboBox3PropertiesCloseUp(Sender: TObject);
var
  szSpec , SpecSql : string;

begin
  inherited;
  if Self.cxLookupComboBox3.Text <> '' then SelectData();
end;

procedure TfrmStorageManage.cxPopupEdit1PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  SelectData();
end;

procedure TfrmStorageManage.cxPopupEdit2PropertiesCloseUp(Sender: TObject);
begin
  inherited;
  SelectData();
end;

procedure TfrmStorageManage.FormCreate(Sender: TObject);
begin
  inherited;
  Self.cxDateEdit1.Date := Date;
  Self.cxDateEdit2.Date := Date;
  Self.cxComboBox3.Enabled := False;

  Self.RzToolButton2.Visible := True;
  Self.RzToolButton90.Visible:= True;
  Self.RzToolButton85.Visible:= True;

  case dwSelectTypes of
    1:
    Self.cxComboBox3.ItemIndex := 1;
    2: Self.cxComboBox3.ItemIndex := 2;
    3: Self.cxComboBox3.ItemIndex := 3;
    4:
    begin
      Self.RzToolButton2.Visible := False;
      Self.RzToolButton90.Visible:= False;
      Self.RzToolButton85.Visible:= False;

      Self.cxComboBox3.Enabled := True;
    end;
  end;
end;

procedure TfrmStorageManage.RzPanel5Resize(Sender: TObject);
var
  i : Double;

begin
  inherited;
  i := ( Self.RzPanel5.Width -
      (Self.RzPanel8.Width +
       Self.RzPanel7.Width +
       Self.RzPanel14.Width +
       Self.RzPanel10.Width ) ) / 4;
  Self.RzPanel6.Width := Round(i);
  Self.RzPanel11.Width:= Round(i);
  Self.RzPanel9.Width := Round(i);
  Self.RzPanel15.Width:= Round(i);
end;

procedure TfrmStorageManage.RzToolButton1Click(Sender: TObject);
begin
  inherited;

  with Self.RzPanel12 do
  begin
    if Visible then
      Visible := False
    else
      Visible := True;

  end;

end;

procedure TfrmStorageManage.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  ADOIsEdit(Self.tvViewColumn28.EditValue,Self.Storage);
end;

procedure TfrmStorageManage.RzToolButton3Click(Sender: TObject);
begin
  inherited;
  Self.cxLookupComboBox1.Clear;
  Self.cxLookupComboBox2.Clear;
  Self.cxLookupComboBox3.Clear;
  if dwSelectTypes = 4 then Self.cxComboBox3.Text := '';

  Self.cxComboBox4.Text := '';
  Self.Storage.Close;
end;

procedure TfrmStorageManage.tvViewColumn4PropertiesCloseUp(Sender: TObject);
var
  szColContentA : Variant;
  szColContentB : Variant;
  szColContentC : Variant;

  szRowIndex:Integer;
  szColName : string;
  szCount : Variant;

begin
  with (Sender AS TcxLookupComboBox).Properties.Grid do
  begin
    szRowIndex := FocusedRowIndex;
    if szRowIndex >= 0 then
    begin
    //  szColContentA :=  DataController.Values[szRowIndex,0] ; //名称
      szColContentB :=  DataController.Values[szRowIndex,2] ; //规格
    //  szColContentC :=  DataController.Values[szRowIndex,3] ; //单位
      Self.tvViewColumn5.EditValue := szColContentB; //规格
    //  ShowMessage(VarToStr(szColContentA));
       {
      if szColContentA <> null then
      begin
        with Self.Storage do
        begin
          if (State <> dsInactive) then
          begin
            if (State <> dsInsert) or (State <> dsEdit) then  Edit;

            if szColContentA <> null then  Self.tvViewColumn4.EditValue := szColContentA; //名称
            if szColContentC <> null then  Self.tvViewColumn5.EditValue := szColContentB; //规格

          end;

        end;

      end;
       }
    end;

  end;

end;


procedure TfrmStorageManage.tvViewColumn4PropertiesInitPopup(Sender: TObject);
begin
//  inherited;
//
end;

procedure TfrmStorageManage.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  inherited;
  AAllow := ADOBanEditing(Self.Storage);
end;

procedure TfrmStorageManage.tvViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems3GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  inherited;
  if AValue <> null then
  begin
  //  VisibleCaption;
  end;
end;

end.
