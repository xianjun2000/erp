unit ufrmN1;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, RzPanel, RzTabs, ComCtrls, StdCtrls, RzButton, Mask,
  RzEdit, RzBckgnd, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  cxTextEdit, cxDBEdit, DB, ADODB, cxMemo, cxMaskEdit, cxDropDownEdit,
  cxCurrencyEdit, cxCalendar, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint, cxGroupBox, dxCore, cxDateUtils, Vcl.Menus, cxButtons,
  cxLabel;

type
  TfrmWorkWeight = class(TForm)
    cxGroupBox2: TcxGroupBox;
    cxLabel1: TcxLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    cxLabel2: TcxLabel;
    cxComboBox1: TcxComboBox;
    cxLabel3: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxTextEdit1: TcxTextEdit;
    cxTextEdit2: TcxTextEdit;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxButton1: TcxButton;
    cxTextEdit3: TcxTextEdit;
    cxLabel9: TcxLabel;
    cxMemo1: TcxMemo;
    cxButton2: TcxButton;
    cxButton3: TcxButton;
    cxCurrencyEdit2: TcxCurrencyEdit;
    Label8: TLabel;
    procedure FormShow(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure cxButton3Click(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    g_ProjectDir : string;
    g_PostCode   : string;
    g_PostNumbers: string;
    g_IsModify   : Boolean;
    g_TableName  : string;
    g_Handte     : THandle;
  end;

var
  frmWorkWeight: TfrmWorkWeight;

implementation

uses
  global,uDataModule;

{$R *.dfm}

procedure TfrmWorkWeight.cxButton1Click(Sender: TObject);
var
  s : string;
begin
  //打开目录
  s := Concat(g_ProjectDir,'\' + g_PostNumbers);
  CreateOpenDir(Handle,s,True);
end;

procedure TfrmWorkWeight.cxButton2Click(Sender: TObject);
var
  s : string;
  szCaption : string;
  szContent : string;
  szPrice   : Currency;
  szValue   : Single;
  szRemarks : string;
  szUnit    : string;
  szTruck   : Integer;
  szDate    : string ;
begin
  szUnit := Self.cxComboBox1.Text;
  if Length(szUnit) = 0 then
  begin
    Application.MessageBox('请选择单位',m_title,MB_OK + MB_ICONHAND);
    Self.cxComboBox1.DroppedDown := True;
  end
  else
  begin
     szValue   := Self.cxCurrencyEdit2.Value;
     szPrice   := Self.cxCurrencyEdit1.Value;
     szCaption := Self.cxTextEdit1.Text;  //名称
     szContent := Self.cxTextEdit2.Text; //内容
     szRemarks := Self.cxMemo1.Text;  //备注
     szDate    := Self.cxDateEdit1.Text;
     if g_IsModify then
     begin
      s := Format('Update '+ g_TableName +' set Caption="%s",'  +
                                           'Content="%s",'  +
                                           'pay_time="%s",' +
                                           'Price=''%m'','  +
                                           'num=''%f'','    +
                                           'Units="%s",'    +
                                           'remarks="%s"'  +
                                            ' where numbers="%s"',[
                                                         szCaption,
                                                         szContent,
                                                         szDate,
                                                         szPrice,
                                                         szValue,
                                                         szUnit,
                                                         szRemarks,
                                                         g_PostNumbers
                                                         ]);

        OutputLog(s);
        with DM.Qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := s;
          if ExecSQL <> 0 then
          begin
            m_IsModify := True;
            SendMessage(g_Handte,WM_LisView,0,0);
            Application.MessageBox('修改成功',m_title,MB_OK + MB_ICONQUESTION );
          end else
          begin
            Application.MessageBox('修改失败',m_title,MB_OK + MB_ICONQUESTION );
          end;
        end;

     end else
     begin
        if DM.IsNumbers(g_TableName,g_PostNumbers) then
        begin
          Application.MessageBox('编号已存在',m_title,MB_OK + MB_ICONHAND);
        end
        else
        begin
          s := Format( 'Insert into '+ g_TableName  +' (code,'+
                                              'numbers,'   +
                                              'Caption,'   +
                                              'Content,'   +
                                              'pay_time,'  +
                                              'End_time,'  +
                                              'Price,'      +
                                              'num,'       +
                                              'Units,'     +
                                              'pacttype,'  +
                                              'remarks'    +
                                              ') values("%s","%s","%s","%s","%s","%s",''%m'',''%f'',"%s","%d","%s")',[
                                                                      g_PostCode,
                                                                      g_PostNumbers,
                                                                      szCaption,
                                                                      szContent,
                                                                      szDate,
                                                                      FormatDateTime('yyyy-MM-dd',Now),
                                                                      szPrice,
                                                                      szValue,
                                                                      szUnit,
                                                                      g_pacttype,
                                                                      szRemarks
                                                                      ] );


          OutputLog(s);
          with DM.Qry do
          begin
            Close;
            SQL.Clear;
            SQL.Text := s;
            if 0 <> ExecSQL then
            begin
              m_IsModify := True;
              g_PostNumbers := DM.getDataMaxDate(g_TableName);
              SendMessage(g_Handte,WM_LisView,0,0);
              Application.MessageBox('添加成功',m_title,MB_OK + MB_ICONQUESTION );
            end else
            begin
              Application.MessageBox('添加失败',m_title,MB_OK +  MB_ICONHAND);
            end;
          end;

        end;

     end;

  end;

end;

procedure TfrmWorkWeight.cxButton3Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmWorkWeight.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.cxTextEdit3.Text := MoneyConvert(Self.cxCurrencyEdit1.Value);
end;

procedure TfrmWorkWeight.FormCreate(Sender: TObject);
var
  s : string;
  i : Integer;

begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from unit where chType=0';
    Open;

    if 0 <> RecordCount then
     begin
       Self.cxComboBox1.Clear;
       while not Eof do
       begin
         s := FieldByName('name').AsString;
         Self.cxComboBox1.Properties.Items.Add( s );
         Next;
       end;
       i := Self.cxComboBox1.Properties.Items.IndexOf('m2');
       if i < 0 then  i := 0;

       Self.cxComboBox1.ItemIndex := i;
     end;

  end;

end;

procedure TfrmWorkWeight.FormShow(Sender: TObject);
begin

end;

//sk_CalculatingFormula  计算公式
//sk_EngineeringQuantity 工程量

end.
