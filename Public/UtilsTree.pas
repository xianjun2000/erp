unit UtilsTree;
//新改进
interface
uses
  Windows, Messages, SysUtils, Variants, Classes, ComCtrls, DB, ADODB,
  FillThrdTree;
type
  TNewUtilsTree = class
    TreeView: TTreeView;
    NData: TNodeData;
    Conn: TADOConnection;
    Query: TADOQuery;
    constructor Create(ATree: TTreeView; AConn: TADOConnection;
                              ATableName     : string;
                              ARootText      : string = '节点管理';
                              AFieldIndex    : string = 'parent';
                              AFieldPID      : string = 'PID';
                              AFieldSignName : string = 'SignName';
                              AFieldCode     : string = 'Code';
                              AFieldInputDate: string = 'InputDate';
                              AProjectName   : string = 'ProjectName';
                              AProjectCode   : string = 'ProjectCode';
                              AFieldRemarks  : string = 'Remarks');

    destructor Destroy;
  private
    TableName       : string;
    FieldIndex      : string;
    FieldPID        : string;
    FieldSignName   : string;
    FieldCode       : string;
    FieldInputDate  : string;
    FieldProjectName  : string;
    FieldProjectCode  : string;
    FieldRemarks    : string;
    FieldPatType    : string;
    RootText : string;
    sqlStr, errStr: string;
    function getMaxIndex(ATableName : string): integer;
    function execSQL(sqlStr: string; var errMsg: string): boolean;
  public
    //按照节点在数据库中的编号查找节点
    function FindTreeNode(Tree: TTreeView; Index: integer): TTreeNode;
    //删除子树
    function DeleteTree(Node: TTreeNode): Boolean;
    //查找节点在数据库中的数据
    function getTreeNodeData(TreeNode: TTreeNode): TNodeData;
    //添加节点
    function AddNode(ACode , ARemarks  : string;
                             AInputDate: TDateTime;
                             ASignName : string = '新节点';
                             Index: integer = -1;ANode: TTreeNode = nil): Boolean;


    function AddChildNode(ACode , ARemarks : string ;
                          AInputDate : TDateTime;
                          ASignName : string = '新节点';
                          Index: integer = -1; ANode: TTreeNode = nil): Boolean;
    //修改节点
    function ModifyNodeData(NewSignName , NewCode:string;
                            NewInputDate : TDateTime; NewRemarks : string;
                            Node:TTreeNode): boolean;

    function ModifyNodeSignName(NewSignName : string;Node: TTreeNode): boolean;
    function ModifyProjectInfo(Name,Code : string;Node: TTreeNode): boolean;
    function ModifyNodePID(PID: integer; Node: TTreeNode): boolean;
    //填充节点
    procedure GetTreeTable(AParent : Integer);
  end;

implementation
//--------------------------------------------------------------
constructor TNewUtilsTree.Create(ATree: TTreeView; AConn: TADOConnection;
                              ATableName     : string;
                              ARootText      : string = '节点管理';
                              AFieldIndex    : string = 'parent';
                              AFieldPID      : string = 'PID';
                              AFieldSignName : string = 'SignName';
                              AFieldCode     : string = 'Code';
                              AFieldInputDate: string = 'InputDate';
                              AProjectName   : string = 'ProjectName';
                              AProjectCode   : string = 'ProjectCode';
                              AFieldRemarks  : string = 'Remarks');
begin
  TreeView := ATree;
  Conn := AConn;
  TableName     := ATableName;
  FieldIndex    := AFieldIndex;
  FieldPID      := AFieldPID;
  FieldCode     := AFieldCode;
  FieldInputDate:= AFieldInputDate;
  FieldProjectName  := AProjectName;
  FieldProjectCode  := AProjectCode;
  FieldRemarks  := AFieldRemarks;
  FieldSignName := AFieldSignName;
  RootText      := ARootText;
  Query         := TADOQuery.Create(nil);
  Query.Connection := Conn;
end;
//--------------------------------------------------------------
destructor TNewUtilsTree.Destroy;
begin
  Query.Free;
  inherited Destroy;
end;
//--------------------------------------------------------------
function TNewUtilsTree.FindTreeNode(Tree: TTreeView; Index: integer): TTreeNode;
var i: integer;
begin
  Result := nil;
  for i := 0 to Tree.Items.Count - 1 do
    if PNodeData(Tree.Items.Item[i].Data)^.Index = Index then
      Result := Tree.Items.Item[i];
end;


function TNewUtilsTree.getMaxIndex(ATableName : string): integer;
var
  i: integer;
  //PNode: PNodeData;
  errMsg : string;
  s : string;
  Query : TADOQuery;

begin
  Result := 0;
  Query := TADOQuery.Create(nil);
  try
    Query.Connection := Conn;
    s := 'SELECT MAX(parent) AS P FROM ' + ATableName;
    with Query do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      Open;
      if RecordCount <> 0 then
      begin
         Result := FieldByName('P').AsInteger;
         Inc(Result);
      end else
      begin
        Result := 1;
      end;

    end;

  finally
    Query.Free;
  end;
end;
//--------------------------------------------------------------
function TNewUtilsTree.DeleteTree(Node: TTreeNode): Boolean;

  function DelTableFromIndex(index: integer): boolean;
  var
    sqlStr: string;
    errSTr: string;
  begin
    Result := False;
    SQLStr := 'Delete from ' + TableName + ' where ' + FieldIndex + ' = ' + IntToStr(index);
    if execSQL(sqlStr, errStr) then Result := True;
  end;
  //------------------------------------------------------------
  function DelTreeFromDB(ParentID: integer): Boolean;
  var
     Query: TADOQuery;
  begin
    Query := nil;
    Result := False;
    try
      Query := TADOQuery.Create(nil);
      Query.Connection := Conn;
      Query.SQL.Text := 'select * from ' + TableName + ' where PID = ' + IntToStr(ParentID);
      if Query.Active then Query.Close;
      Query.Open;
      while Query.Eof = False do
      begin
        DelTreeFromDB(Query.FieldValues[FieldIndex]);
        Result := DelTableFromIndex(Query.FieldByName(FieldIndex).AsInteger);
        Query.Next;
      end;
    finally
      Query.Free;
    end;
    Result := DelTableFromIndex(ParentID);
  end;
  //------------------------------------------------------------
begin

  Result := False;
  if Node.AbsoluteIndex = 0 then
  begin
    raise Exception.Create('禁止删除根节点！');
    Exit;
  end;

  if DelTreeFromDB(PNodeData(Node.Data)^.Index) then
  begin
    TreeView.Items.Delete(Node);
    Result := True;
  end;

end;
//--------------------------------------------------------------
function TNewUtilsTree.getTreeNodeData(TreeNode: TTreeNode): TNodeData;
begin
  if TreeNode = nil then
    Result := PNodeData(TreeView.Items.Item[0].Data)^
  else
    Result := PNodeData(TreeNode.Data)^;
end;

//--------------------------------------------------------------
function TNewUtilsTree.AddNode(ACode , ARemarks : string;
                               AInputDate: TDateTime;
                               ASignName : string = '新节点';
                               Index: integer = -1;ANode: TTreeNode = nil): Boolean;
var
  Node, NewNode: TTreeNode;
  PData: PNodeData;

begin

  Result := True;
  try
    if ANode = nil then Node := TreeView.Selected
    else Node := ANode;
    New(PData);
    PData^.Index   := getMaxIndex(TableName);
    PData^.SignName := ASignName;
    PData^.Code     := ACode;
    PData^.InputDate:=AInputDate;
    PData^.remarks := ARemarks;
  //  FieldInputDate
    sqlStr := 'Insert into ' + TableName + ' (' + FieldIndex    + ',' +
                                                  FieldCode     + ',' +
                                                  FieldRemarks  + ',' +
                                                  FieldPID      + ',' +
                                                  FieldSignName +
                                            ')' + ' values(' +
                                                 IntToStr(PData^.Index) + ',"' +
                                                 ACode    + '",' +
                                                 ARemarks +  '",';



    if Node = nil then Node := TreeView.Items.Item[0];
    if Node.Level = 0 then
      sqlStr := sqlStr + '0,"' + ASignName + '")'
    else
      sqlStr := sqlStr + IntToStr(PNodeData(Node.Parent.Data)^.Index) + ',"' + ASignName + '")';

    if execSQL(sqlStr, errStr) then
    begin
      if Node.Level = 0 then
        NewNode := TreeView.Items.AddChildObject(Node, ASignName, PData)
      else NewNode := TreeView.Items.AddObject(Node, ASignName, PData);
      NewNode.ImageIndex := 1;
      NewNode.SelectedIndex := 2;
      TreeView.Selected.Expanded := True;
    end;

  except
    Result := False;
  end;
end;
//--------------------------------------------------------------
function TNewUtilsTree.AddChildNode(ACode , ARemarks:string;
                                    AInputDate : TDateTime;
                                    ASignName: string = '新节点';
                                    Index: integer = -1; ANode: TTreeNode = nil): Boolean;
var
  Node, NewNode: TTreeNode;
  PData: PNodeData;

begin

  Result := True;
  try
    if ANode = nil then Node := TreeView.Selected
    else Node := ANode;

    New(PData);
    PData^.Index     := getMaxIndex(TableName);
    PData^.SignName  := ASignName;
    PData^.Code      := ACode;
    PData^.InputDate := AInputDate;
    PData^.remarks   := ARemarks;
    sqlStr := 'Insert into ' + TableName + ' (' + FieldIndex     + ',' +
                                                  FieldCode      + ',' +
                                                  FieldInputDate + ',' +
                                                  FieldRemarks   + ',' +
                                                  FieldPID       + ',' +
                                                  FieldSignName  +
                                             ')' + ' values(' +
                                                  IntToStr(PData^.Index) + ',"' +
                                                  ACode    + '","' +
                                                  FormatDateTime('YYYY-MM-dd', AInputDate ) +'","' +
                                                  ARemarks + '",' ;



    if Node = nil then Node := TreeView.Items.Item[0];
    if Node.Level = 0 then
      sqlStr := sqlStr + '0,"' + ASignName + '")'
    else
      sqlStr := sqlStr + IntToStr(PNodeData(Node.Data)^.Index) + ',"' + ASignName + '")';

    if execSQL(sqlStr, errStr) then
    begin
      NewNode := TreeView.Items.AddChildObject(Node, ASignName, PData);
      NewNode.ImageIndex    := 1;
      NewNode.SelectedIndex := 2;
      NewNode.Expand(True);
      NewNode.Expanded := True;
      NewNode.Selected := True;
      NewNode.EditText;
    end;

  except
    Result := False;
  end;

end;
//--------------------------------------------------------------
//执行SQL语言
function TNewUtilsTree.ExecSQL(sqlStr: string; var errMsg: string): boolean;
begin
  result := false;
  errMsg := '执行成功！';
  try
    Query.SQL.Text := sqlStr;
    if Query.Active then Query.Close;
    Query.ExecSQL;
    result := true;
  except
    on e: Exception do
      errMsg := e.Message;
  end;
end;
//------------------------------------------------------------------------------
procedure TNewUtilsTree.GetTreeTable(AParent : Integer);
var
  H: TNewFillTree;
begin
  H := TNewFillTree.Create(TreeView,Conn ,
                           TableName,
                           RootText,
                           FieldIndex ,
                           FieldPID,
                           FieldSignName,
                           FieldCode,
                           FieldInputDate,
                           FieldProjectName,
                           FieldProjectCode,
                           FieldRemarks,
                           AParent
                           );
end;

//------------------------------------------------------------------------------
//修改数据
function TNewUtilsTree.ModifyNodeData(NewSignName , NewCode:string;NewInputDate : TDateTime; NewRemarks : string; Node:TTreeNode): boolean;
begin
  Result := False;
  sqlStr := 'Update ' + TableName + ' set ' + FieldSignName + '="' + NewSignName + '",' +
                                              FieldCode     + '="' + newCode    + '",' +
                                              FieldInputDate+ '="' + FormatDateTime('YYYY-MM-dd',NewInputDate) + '",'+
                                              FieldRemarks  + '="' + NewRemarks + '" where ' +
                                              FieldIndex    + ' = ' + IntToStr( PNodeData(Node.Data)^.Index );

  if execSQL(sqlStr, errStr) then
  begin
    Node.Text := NewSignName;
    PNodeData(Node.Data)^.SignName   := NewSignName;
    PNodeData(Node.Data)^.Code      := NewCode;
    PNodeData(Node.Data)^.remarks   := NewRemarks;
    Result := True;
  end;

end;
//修改名字
function TNewUtilsTree.ModifyNodeSignName(NewSignName : string;Node: TTreeNode): boolean;
begin
  sqlStr := 'Update ' + TableName + ' set ' + FieldSignName + '="' + NewSignName + '" where '
    + FieldIndex + ' = ' + IntToStr(PNodeData(Node.Data)^.Index);
  if execSQL(sqlStr, errStr) then
  begin
    Node.Text := NewSignName;
    PNodeData(Node.Data)^.SignName := NewSignName;
  end;

end;

function TNewUtilsTree.ModifyProjectInfo(Name,Code : string;Node: TTreeNode): boolean;
begin
  sqlStr := 'Update ' + TableName + ' set ' + FieldProjectName + '="' + Name + '" where '
    + FieldIndex + ' = ' + IntToStr(PNodeData(Node.Data)^.Index);
  if execSQL(sqlStr, errStr) then
  begin
    PNodeData(Node.Data)^.ProjectName := Name;
    PNodeData(Node.Data)^.ProjectCode := Code;
  end;

end;

//------------------------------------------------------------------------------
function TNewUtilsTree.ModifyNodePID(PID: integer; Node: TTreeNode): boolean;

  function InChildNode(APID: integer; ANode: TTreeNode): boolean;
  var loop, NStart, NEnd: integer;
  begin
    Result := False;
    NStart := Node.AbsoluteIndex;
    if Node.getNextSibling = nil then NEnd := TreeView.Items.Count - 1
    else NEnd := Node.GetNextSibling.AbsoluteIndex;
    for loop := NStart + 1 to NEnd - 1 do
    begin
      Result := Result or (PNodeData(TreeView.Items.Item[loop].Data)^.Index = PID);
    end;
  end;

begin
  Result := False;

  //如果是根节点则忽略
  if Node.AbsoluteIndex = 0 then Exit;
  //如果PID不变则忽略
  if PID = PNodeData(Node.Parent.Data)^.Index then Exit;
  //如果父节点是自己也则忽略
  if PID = PNodeData(Node.Data)^.Index then Exit;
  //如果父节点编号是它其中一级子节点的编号，则忽略
  if InChildNode(PID, Node) then Exit;

  sqlStr := 'Update ' + TableName + ' set ' + FieldPID + '="' + IntToStr(PID) + '" where '
    + FieldIndex + ' = ' + IntToStr(PNodeData(Node.Data)^.Index);
  if execSQL(sqlStr, errStr) then Result := True;
end;

end.

