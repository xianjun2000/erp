unit ufrmCostSubject;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, Vcl.ComCtrls, RzButton, RzPanel, Vcl.ExtCtrls,
  Data.Win.ADODB,ufrmBaseController, cxTextEdit;

type
  TfrmCostSubject = class(TfrmBaseController)
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton5: TRzToolButton;
    StatusBar1: TStatusBar;
    Grid: TcxGrid;
    tvTable: TcxGridDBTableView;
    tvTableColumn1: TcxGridDBColumn;
    tvTableColumn2: TcxGridDBColumn;
    Lv: TcxGridLevel;
    ADOCost: TADOQuery;
    DataCost: TDataSource;
    procedure FormActivate(Sender: TObject);
    procedure ADOCostAfterOpen(DataSet: TDataSet);
    procedure RzToolButton1Click(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure RzToolButton3Click(Sender: TObject);
    procedure tvTableEditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tvTableKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvTableColumn1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzToolButton5Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure tvTableEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmCostSubject: TfrmCostSubject;

implementation

uses
   uDataModule,global;

{$R *.dfm}

procedure TfrmCostSubject.ADOCostAfterOpen(DataSet: TDataSet);
begin
  TADOQuery(DataSet).Properties.Get_Item('Update Criteria').Value :=0;
end;

procedure TfrmCostSubject.FormActivate(Sender: TObject);
begin
//sk_Maintain_Cost
  with Self.ADOCost do
  begin
    Close;
    SQL.Text := 'Select * from ' + g_Table_Maintain_Cost;
    Open;
  end;
end;

procedure TfrmCostSubject.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Self.ADOCost.Close;
end;

procedure TfrmCostSubject.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then  Close;
  
end;

procedure TfrmCostSubject.RzToolButton1Click(Sender: TObject);
begin
  with Self.ADOCost do
  begin
    if State <> dsInactive then
    begin
      Append;
      Self.tvTableColumn2.FocusWithSelection;
      Self.Grid.SetFocus;
      keybd_event(VK_RETURN,0,0,0);
    end;
  end;
end;

procedure TfrmCostSubject.RzToolButton2Click(Sender: TObject);
begin
  IsDeleteEmptyData(Self.ADOCost ,
                   g_Table_Maintain_Cost ,
                   Self.tvTableColumn2.DataBinding.FieldName);
end;

procedure TfrmCostSubject.RzToolButton3Click(Sender: TObject);
begin
  IsDelete(Self.tvTable);
end;

procedure TfrmCostSubject.RzToolButton5Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCostSubject.tvTableColumn1GetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);

end;

procedure TfrmCostSubject.tvTableEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  if AItem.Index = Self.tvTableColumn1.Index then
  begin
    AAllow := False;
  end;
end;

procedure TfrmCostSubject.tvTableEditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 13) and (AItem.Index = Self.tvTableColumn2.Index) then
  begin
    if Self.tvTable.Controller.FocusedRow.IsLast  then
    begin
      with Self.ADOCost do
      begin
        if State <> dsInactive then
        begin
          Append;
          Self.tvTableColumn2.FocusWithSelection;
        end;
      end;
    end;
  end;
end;

procedure TfrmCostSubject.tvTableKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  begin
    Self.RzToolButton3.Click;
  end;
end;

end.
