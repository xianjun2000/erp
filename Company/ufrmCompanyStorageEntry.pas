unit ufrmCompanyStorageEntry;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,ufrmBaseStorage, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxTextEdit, cxCheckBox,
  cxCalendar, cxDBLookupComboBox, cxDropDownEdit, cxMemo, cxCurrencyEdit,
  cxSpinEdit, Vcl.Menus, frxClass, frxDBSet, cxDBEdit, Vcl.StdCtrls, cxGroupBox,
  Vcl.ExtCtrls, cxGridLevel, cxGridCustomTableView, cxGridTableView,
  cxGridDBTableView, cxClasses, cxGridCustomView, cxGrid, cxMaskEdit, cxLabel,
  RzButton, RzPanel, Data.Win.ADODB, Datasnap.DBClient;

type
  TfrmCompanyStorageEntry = class(TfrmBaseStorage)
    tv: TTreeView;
    RzToolbar5: TRzToolbar;
    RzSpacer27: TRzSpacer;
    RzBut1: TRzToolButton;
    RzSpacer28: TRzSpacer;
    RzBut2: TRzToolButton;
    RzSpacer29: TRzSpacer;
    RzBut3: TRzToolButton;
    RzToolButton1: TRzToolButton;
  private
    { Private declarations }
  public
    { Public declarations }
    dwTypes : string;

  end;

var
  frmCompanyStorageEntry : TfrmCompanyStorageEntry;

implementation

{$R *.dfm}

end.
