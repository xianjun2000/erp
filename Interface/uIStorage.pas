unit uIStorage;

interface

type
  IStorage = interface(IInterface)
    ['{00B2FD03-9B4A-43D3-BDB5-014CDC22FB7A}']
    {此接口声明了一个 Name 属性; 因为接口没有字段, read/write 都只能从方法}
    function GetName : string;
    procedure SetName(val : string);
    property Name : string read GetName write SetName;
  end;


implementation

end.
