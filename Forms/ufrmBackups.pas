unit ufrmBackups;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, RzStatus, BMDThread;
type
  TBackupsThread = class(TThread)
    protected
    procedure Execute; override;
  end;
type
  TfrmBackups = class(TForm)
    //BMDThread1: TBMDThread;
    Label1: TLabel;
    BMDThread1: TBMDThread;
    procedure FormShow(Sender: TObject);

    procedure BMDThread1Execute(Sender: TObject; Thread: TBMDExecuteThread;
      var Data: Pointer);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure BackupToFile(StrBackupPathFile:TFileName;infoLabel:TLabel);
  end;

var
  frmBackups: TfrmBackups;

implementation

uses
   uDataModule,Comobj,global;

{$R *.dfm}

function GetTmpPath:AnsiString;
var
  arr: array[0..MAX_PATH] of Char;
  num: DWORD;
begin
  num := GetTempPath(MAX_PATH, arr);
  result:=arr;
end;

function GetTmpFile:AnsiString;
var sPath:AnsiString;
  arr: array[0..MAX_PATH] of Char;
begin
  sPath := getTmpPath;
  GetTempFileName(PWideChar(sPath), PWideChar('sys.tmp.'), 0, arr);
  Result:=arr;
end;


procedure TBackupsThread.Execute;
var
  i: Integer;

begin

  for i := 0 to 9 do
    if not Terminated then
      Sleep(1000)
    else
      Break;

end;


procedure TfrmBackups.BackupToFile(StrBackupPathFile:TFileName;infoLabel:TLabel);
var
  SourceFile, strTheTempFileName: string;
  SuccessFlag : Boolean;
  Compact     : OleVariant;

begin

  strTheTempFileName := GetTmpFile;//GetTmpFileName('DD');
  SourceFile := GetFilePath + 'data.mdb';//pstrJETaxDataFullName;
  if not FileExists(SourceFile) then
  begin
    Application.MessageBox(PChar('源数据读取不成功，备份失败！'),PChar('备份失败'), MB_ICONINFORMATION);
    Exit;
  end;

  SuccessFlag := True;
  infoLabel.Caption := '正在做备份前的数据整理，请等待......';
  infoLabel.Update;
  Sleep(500);
  try
    {
    try
      Compact := CreateOleObject('DAO.DBEngine.36');
      if FileExists(strTheTempFileName) then DeleteFile(strTheTempFileName);
      Compact.CompactDatabase(SourceFile, strTheTempFileName);
    except
      SuccessFlag := False;
      Application.MessageBox(PChar('备份前的数据库整理失败！' + #13
                            + '原因可能是你没有安装Microsft Access系统或者用其它方式打开了数据库,' + #13
                            + '按确定继续备份操作...'),PChar('备份失败'), MB_ICONINFORMATION);
      Exit;
      
    end;
    Sleep(500);
    if SuccessFlag then
    begin
      try
        if DeleteFile(SourceFile) then
          Compact.CompactDatabase(strTheTempFileName, SourceFile);
      except
      end;
    end;
    }
    infoLabel.Caption := '正在备份，请等待......';
    //RadioGroup1.Hint := Label1.Caption;
    infoLabel.Update;
    //Button3.Visible := false;
    Sleep(500);
    if not (CopyFile(Pchar(SourceFile), Pchar(StrBackupPathFile), false)) then
      SuccessFlag := False
    else
      SuccessFlag := True;
  finally
    if FileExists(strTheTempFileName) then
      DeleteFile(strTheTempFileName)
    else
      try
        DeleteFile(strTheTempFileName + '.mdb')
      except
      end;
  end;
  Sleep(500);
  Compact:=unassigned;
  if SuccessFlag then
  begin
    infoLabel.Caption := '数据备份成功！';
    infoLabel.Update;
  //  Application.MessageBox(PChar('数据备份成功！'),PChar('备份成功'), MB_ICONINFORMATION);
    Close;
  end
  else
  begin
    infoLabel.Caption := '数据备份失败！';
    infoLabel.Update;
    Application.MessageBox(PChar('数据备份失败！'),PChar('备份失败'), MB_ICONINFORMATION);
  end;
end;

procedure TfrmBackups.BMDThread1Execute(Sender: TObject;
  Thread: TBMDExecuteThread; var Data: Pointer);
begin
  Self.Label1.Caption := '正在关闭数据库...';
  while DM.ADOconn.Connected  do
  begin
    DM.ADOconn.Cancel;
    DM.ADOconn.Close;
    Sleep(1000);
  end;
  Sleep(1000);
  BackupToFile('Backup.mdb',Self.Label1);
end;

procedure TfrmBackups.FormShow(Sender: TObject);
begin
  Self.BMDThread1.Start;
end;

end.
