object frmGroupUnitPrice: TfrmGroupUnitPrice
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #32452#21512#21333#20215
  ClientHeight = 437
  ClientWidth = 378
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object RzToolbar1: TRzToolbar
    Left = 0
    Top = 0
    Width = 378
    Height = 27
    Images = DM.cxImageList1
    RowHeight = 23
    BorderInner = fsNone
    BorderOuter = fsGroove
    BorderSides = [sdTop]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 0
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer8
      RzToolButton9
      RzSpacer9
      RzToolButton13
      RzSpacer11
      RzToolButton12
      RzSpacer12
      RzToolButton11
      RzSpacer1
      RzToolButton1)
    object RzSpacer8: TRzSpacer
      Left = 4
      Top = 1
    end
    object RzToolButton9: TRzToolButton
      Left = 12
      Top = 1
      Width = 58
      Hint = #26032#22686
      SelectionColorStop = 16119543
      ImageIndex = 37
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #26032#22686
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton9Click
    end
    object RzSpacer9: TRzSpacer
      Left = 70
      Top = 1
    end
    object RzToolButton11: TRzToolButton
      Left = 210
      Top = 1
      Width = 58
      Hint = #21024#38500
      SelectionColorStop = 16119543
      ImageIndex = 51
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #21024#38500
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton11Click
    end
    object RzSpacer11: TRzSpacer
      Left = 136
      Top = 1
    end
    object RzToolButton12: TRzToolButton
      Left = 144
      Top = 1
      Width = 58
      Hint = #30830#23450
      SelectionColorStop = 16119543
      ImageIndex = 17
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #30830#23450
      ParentShowHint = False
      ShowHint = True
      OnClick = RzToolButton12Click
    end
    object RzSpacer12: TRzSpacer
      Left = 202
      Top = 1
    end
    object RzToolButton13: TRzToolButton
      Left = 78
      Top = 1
      Width = 58
      Hint = #32534#36753
      SelectionColorStop = 16119543
      ImageIndex = 0
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #32534#36753
      ParentShowHint = False
      ShowHint = True
    end
    object RzSpacer1: TRzSpacer
      Left = 268
      Top = 1
    end
    object RzToolButton1: TRzToolButton
      Left = 276
      Top = 1
      Width = 58
      SelectionColorStop = 16119543
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986
      OnClick = RzToolButton1Click
    end
  end
  object Grid: TcxGrid
    Left = 0
    Top = 27
    Width = 378
    Height = 410
    Align = alClient
    TabOrder = 1
    object tView: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = ds
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Format = #165',0.00;'#165'-,0.00'
          Kind = skSum
          OnGetText = tViewTcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
          Column = tViewColumn4
        end>
      DataController.Summary.SummaryGroups = <>
      OptionsBehavior.FocusCellOnTab = True
      OptionsBehavior.FocusFirstCellOnNewRecord = True
      OptionsBehavior.GoToNextCellOnEnter = True
      OptionsData.CancelOnExit = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsSelection.CellMultiSelect = True
      OptionsView.DataRowHeight = 23
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 23
      OptionsView.Indicator = True
      object tViewColumn1: TcxGridDBColumn
        Caption = #24207#21495
        OnGetDisplayText = tViewColumn1GetDisplayText
        Width = 40
      end
      object tViewColumn2: TcxGridDBColumn
        Caption = #32452#21512#31867#22411
        DataBinding.FieldName = 'CaptionType'
        PropertiesClassName = 'TcxComboBoxProperties'
        Properties.DropDownListStyle = lsEditFixedList
        RepositoryItem = DM.TabBox
        OnGetPropertiesForEdit = tViewColumn2GetPropertiesForEdit
        Width = 102
      end
      object tViewColumn3: TcxGridDBColumn
        Caption = #32452#21512#35268#26684
        DataBinding.FieldName = 'spec'
        PropertiesClassName = 'TcxLookupComboBoxProperties'
        Properties.DropDownWidth = 280
        Properties.ImmediateDropDownWhenActivated = True
        Properties.KeyFieldNames = 'Spec'
        Properties.ListColumns = <
          item
            Caption = #21830#21697#21517#31216
            Width = 150
            FieldName = 'GoodsName'
          end
          item
            Caption = #35268#26684
            Width = 70
            FieldName = 'Spec'
          end
          item
            Caption = #21333#20215
            Width = 60
            FieldName = 'UnitPrice'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListSource = ds1
        Properties.OnCloseUp = tViewColumn3PropertiesCloseUp
        Width = 107
      end
      object tViewColumn4: TcxGridDBColumn
        Caption = #21333#20215
        DataBinding.FieldName = 'UnitPrice'
        PropertiesClassName = 'TcxCurrencyEditProperties'
        Width = 96
      end
      object tViewColumn5: TcxGridDBColumn
        DataBinding.FieldName = 'ParentId'
        Visible = False
        Width = 60
      end
    end
    object Lv: TcxGridLevel
      GridView = tView
    end
  end
  object ds: TDataSource
    Left = 168
    Top = 288
  end
  object ds1: TDataSource
    DataSet = ds2
    Left = 168
    Top = 104
  end
  object ds2: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 120
    Top = 104
  end
end
