object frmReport: TfrmReport
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #25253#34920#31649#29702
  ClientHeight = 481
  ClientWidth = 384
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object RzPanel1: TRzPanel
    Left = 0
    Top = 29
    Width = 384
    Height = 417
    Align = alTop
    BorderOuter = fsFlat
    TabOrder = 0
    object RzPanel2: TRzPanel
      Left = 1
      Top = 1
      Width = 265
      Height = 415
      Align = alLeft
      BorderOuter = fsFlat
      BorderSides = [sdRight]
      TabOrder = 0
      object cxGroupBox1: TcxGroupBox
        Left = 0
        Top = 0
        Align = alClient
        Caption = #25253#34920#26679#24335
        TabOrder = 0
        Height = 415
        Width = 264
        object Grid: TcxGrid
          Left = 2
          Top = 18
          Width = 260
          Height = 395
          Align = alClient
          TabOrder = 0
          object tvView: TcxGridDBTableView
            OnDblClick = tvViewDblClick
            Navigator.Buttons.CustomButtons = <>
            OnEditing = tvViewEditing
            DataController.DataSource = ds
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsView.DataRowHeight = 23
            OptionsView.GroupByBox = False
            OptionsView.HeaderHeight = 23
            OptionsView.Indicator = True
            object tvViewColumn1: TcxGridDBColumn
              Caption = #24207#21495
              PropertiesClassName = 'TcxTextEditProperties'
              Properties.Alignment.Horz = taCenter
              OnGetDisplayText = tvViewColumn1GetDisplayText
              Options.Filtering = False
              Options.FilteringAddValueItems = False
              Options.FilteringFilteredItemsList = False
              Options.FilteringMRUItemsList = False
              Options.FilteringPopup = False
              Options.FilteringPopupMultiSelect = False
              Options.FilteringWithFindPanel = False
              Options.Moving = False
              Options.Sorting = False
              Width = 42
            end
            object tvViewColumn3: TcxGridDBColumn
              Caption = #27169#26495#21517#31216
              DataBinding.FieldName = 'ReportName'
              Options.Filtering = False
              Options.Moving = False
              Options.Sorting = False
              Width = 204
            end
            object tvViewColumn2: TcxGridDBColumn
              Caption = #25991#20214#21517
              DataBinding.FieldName = 'ReportFile'
              Width = 60
            end
            object tvViewColumn4: TcxGridDBColumn
              Caption = #26159#21542#31435#21363#25171#21360
              DataBinding.FieldName = 'IsDirectPrint'
              Width = 60
            end
          end
          object lv: TcxGridLevel
            GridView = tvView
          end
        end
      end
    end
    object cxButton1: TcxButton
      Left = 280
      Top = 16
      Width = 89
      Height = 35
      Caption = #30452#25509#25171#21360
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 6
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 1
      OnClick = cxButton1Click
    end
    object cxButton2: TcxButton
      Left = 280
      Top = 71
      Width = 89
      Height = 35
      Caption = #25253#34920#35774#35745
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 5
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 2
      OnClick = cxButton1Click
    end
    object cxButton3: TcxButton
      Left = 280
      Top = 126
      Width = 89
      Height = 35
      Caption = #25171#21360#39044#35272
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      OptionsImage.ImageIndex = 7
      OptionsImage.Images = DM.cxImageList1
      TabOrder = 3
      OnClick = cxButton1Click
    end
    object cxButton4: TcxButton
      Left = 280
      Top = 308
      Width = 89
      Height = 38
      Caption = #22797#21046#27169#26495
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      TabOrder = 4
      Visible = False
    end
    object cxButton5: TcxButton
      Left = 280
      Top = 360
      Width = 89
      Height = 37
      Caption = 'cxButton1'
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      TabOrder = 5
      Visible = False
    end
  end
  object RzPanel3: TRzPanel
    Left = 0
    Top = 446
    Width = 384
    Height = 35
    Align = alClient
    BorderOuter = fsFlat
    BorderSides = [sdLeft, sdRight, sdBottom]
    TabOrder = 1
    object cxLabel1: TcxLabel
      Left = 8
      Top = 7
      Caption = #27169#26495#36335#24452#65306
    end
    object ReportFilePath: TcxTextEdit
      Left = 64
      Top = 7
      TabOrder = 1
      Width = 191
    end
    object cxButton6: TcxButton
      Left = 287
      Top = 6
      Width = 50
      Height = 22
      Caption = #27983#35272
      LookAndFeel.NativeStyle = False
      LookAndFeel.SkinName = 'Office2013White'
      TabOrder = 2
      OnClick = cxButton6Click
    end
  end
  object RzToolbar4: TRzToolbar
    Left = 0
    Top = 0
    Width = 384
    Height = 29
    AutoStyle = False
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsNone
    BorderSides = [sdBottom]
    BorderWidth = 0
    Caption = #38468#20214
    GradientColorStyle = gcsCustom
    ParentShowHint = False
    ShowHint = True
    TabOrder = 2
    Visible = False
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer26
      RzToolButton1
      RzSpacer1
      RzToolButton24
      RzSpacer3
      RzToolButton3)
    object RzSpacer26: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzToolButton24: TRzToolButton
      Left = 96
      Top = 2
      Width = 93
      SelectionColorStop = 16119543
      SelectionFrameColor = clBtnShadow
      ImageIndex = 6
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #25171#21360#26426#37197#32622
    end
    object RzSpacer3: TRzSpacer
      Left = 189
      Top = 2
    end
    object RzToolButton3: TRzToolButton
      Left = 197
      Top = 2
      Width = 60
      SelectionColorStop = 16119543
      ImageIndex = 8
      ShowCaption = True
      UseToolbarButtonSize = False
      UseToolbarShowCaption = False
      Caption = #36864#20986'&'
    end
    object RzSpacer1: TRzSpacer
      Left = 88
      Top = 2
    end
    object RzToolButton1: TRzToolButton
      Left = 12
      Top = 2
      Width = 76
      ImageIndex = 79
      ShowCaption = True
      UseToolbarShowCaption = False
      Caption = #26679#24335#31649#29702
      OnClick = RzToolButton1Click
    end
  end
  object frxDBDataset: TfrxDBDataset
    UserName = #36827#38144#31649#29702
    CloseDataSource = False
    BCDToCurrency = False
    Left = 164
    Top = 72
  end
  object frxDesigner: TfrxDesigner
    DefaultScriptLanguage = 'PascalScript'
    DefaultFont.Charset = DEFAULT_CHARSET
    DefaultFont.Color = clWindowText
    DefaultFont.Height = -13
    DefaultFont.Name = 'Arial'
    DefaultFont.Style = []
    DefaultLeftMargin = 10.000000000000000000
    DefaultRightMargin = 10.000000000000000000
    DefaultTopMargin = 10.000000000000000000
    DefaultBottomMargin = 10.000000000000000000
    DefaultPaperSize = 9
    DefaultOrientation = poPortrait
    GradientEnd = 11982554
    GradientStart = clWindow
    TemplatesExt = 'fr3'
    Restrictions = []
    RTLLanguage = False
    MemoParentFont = False
    Left = 104
    Top = 128
  end
  object frxReport: TfrxReport
    Version = '6.0.7'
    DotMatrixReport = False
    IniFile = '\Software\Fast Reports'
    PreviewOptions.Buttons = [pbPrint, pbLoad, pbSave, pbExport, pbZoom, pbFind, pbOutline, pbPageSetup, pbTools, pbEdit, pbNavigator, pbExportQuick]
    PreviewOptions.Zoom = 1.000000000000000000
    PrintOptions.Printer = #39044#35774
    PrintOptions.PrintOnSheet = 0
    ReportOptions.CreateDate = 43135.924651481500000000
    ReportOptions.LastChange = 43223.604149398110000000
    ScriptLanguage = 'PascalScript'
    ScriptText.Strings = (
      'begin'
      'end.')
    OnGetValue = frxReportGetValue
    OnUserFunction = frxReportUserFunction
    Left = 104
    Top = 72
    Datasets = <
      item
        DataSet = frxDBDataset
        DataSetName = #36827#38144#31649#29702
      end>
    Variables = <
      item
        Name = ' '#24448#26469#21333#20301
        Value = Null
      end
      item
        Name = #21457#36135#21333#20301
        Value = Null
      end
      item
        Name = #25910#36135#21333#20301
        Value = Null
      end
      item
        Name = #21517#31216
        Value = Null
      end
      item
        Name = #31080#25454#31867#21035
        Value = Null
      end
      item
        Name = #21333#25454#32534#21495
        Value = Null
      end
      item
        Name = #24320#31080#26085#26399
        Value = Null
      end
      item
        Name = #32467#31639#29366#24577
        Value = Null
      end
      item
        Name = #31080#25454#29366#24577
        Value = Null
      end
      item
        Name = #31614#25910#20154
        Value = Null
      end
      item
        Name = #32463#25163#20154
        Value = Null
      end
      item
        Name = #21457#36135#20154
        Value = Null
      end
      item
        Name = #23457#26680#31614#23383
        Value = Null
      end
      item
        Name = #31614#25910#26085#26399
        Value = Null
      end
      item
        Name = #22791#27880
        Value = Null
      end
      item
        Name = #25910#36135#20154
        Value = Null
      end
      item
        Name = #20132#26131#26041#24335
        Value = Null
      end
      item
        Name = 'Title'
        Value = Null
      end>
    Style = <>
    object Data: TfrxDataPage
      Height = 1000.000000000000000000
      Width = 1000.000000000000000000
    end
    object Page1: TfrxReportPage
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlack
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      PaperWidth = 241.000000000000000000
      PaperHeight = 140.000000000000000000
      PaperSize = 256
      LeftMargin = 20.000000000000000000
      RightMargin = 20.000000000000000000
      TopMargin = 10.000000000000000000
      BottomMargin = 10.000000000000000000
      MirrorMargins = True
      BackPictureStretched = False
      Frame.ShadowWidth = 2.000000000000000000
      Frame.Typ = [ftTop, ftBottom]
      object Master: TfrxMasterData
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -12
        Font.Name = 'Arial'
        Font.Style = []
        Height = 20.787401570000000000
        ParentFont = False
        Top = 215.433210000000000000
        Width = 759.685530000000000000
        DataSet = frxDBDataset
        DataSetName = #36827#38144#31649#29702
        RowCount = 0
        object Memo6: TfrxMemoView
          Align = baLeft
          Width = 37.795300000000000000
          Height = 20.787401570000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 4.000000000000000000
          GapY = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '[Line#]')
          ParentFont = False
        end
        object Memo9: TfrxMemoView
          Align = baLeft
          Left = 559.370440000000000000
          Width = 154.960671420000000000
          Height = 20.787401570000000000
          DataField = 'SumMonery'
          DataSet = frxDBDataset
          DataSetName = #36827#38144#31649#29702
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 4.000000000000000000
          GapY = 2.000000000000000000
          Memo.UTF8W = (
            '['#36827#38144#31649#29702'."SumMonery"]')
          ParentFont = False
        end
        object Memo21: TfrxMemoView
          Align = baLeft
          Left = 37.795300000000000000
          Width = 151.181200000000000000
          Height = 20.787401570000000000
          DataField = 'GoodsName'
          DataSet = frxDBDataset
          DataSetName = #36827#38144#31649#29702
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 4.000000000000000000
          GapY = 2.000000000000000000
          Memo.UTF8W = (
            '['#36827#38144#31649#29702'."GoodsName"]')
          ParentFont = False
        end
        object Memo22: TfrxMemoView
          Align = baLeft
          Left = 188.976500000000000000
          Width = 139.842610000000000000
          Height = 20.787401570000000000
          DataField = 'Model'
          DataSet = frxDBDataset
          DataSetName = #36827#38144#31649#29702
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 4.000000000000000000
          GapY = 2.000000000000000000
          Memo.UTF8W = (
            '['#36827#38144#31649#29702'."Model"]')
          ParentFont = False
        end
        object Memo27: TfrxMemoView
          Align = baLeft
          Left = 328.819110000000000000
          Width = 56.692950000000000000
          Height = 20.787401570000000000
          DataField = 'Spec'
          DataSet = frxDBDataset
          DataSetName = #36827#38144#31649#29702
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 4.000000000000000000
          GapY = 2.000000000000000000
          Memo.UTF8W = (
            '['#36827#38144#31649#29702'."Spec"]')
          ParentFont = False
        end
        object Memo28: TfrxMemoView
          Align = baLeft
          Left = 385.512060000000000000
          Width = 37.795300000000000000
          Height = 20.787401570000000000
          DataField = 'Company'
          DataSet = frxDBDataset
          DataSetName = #36827#38144#31649#29702
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 4.000000000000000000
          GapY = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '['#36827#38144#31649#29702'."Company"]')
          ParentFont = False
        end
        object Memo29: TfrxMemoView
          Align = baLeft
          Left = 423.307360000000000000
          Width = 68.031540000000000000
          Height = 20.787401570000000000
          DataField = 'CountTotal'
          DataSet = frxDBDataset
          DataSetName = #36827#38144#31649#29702
          DisplayFormat.FormatStr = '%2.2f'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 4.000000000000000000
          GapY = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '['#36827#38144#31649#29702'."CountTotal"]')
          ParentFont = False
        end
        object Memo30: TfrxMemoView
          Align = baLeft
          Left = 491.338900000000000000
          Width = 68.031540000000000000
          Height = 20.787401570000000000
          DataField = 'UnitPrice'
          DataSet = frxDBDataset
          DataSetName = #36827#38144#31649#29702
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 4.000000000000000000
          GapY = 2.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            '['#36827#38144#31649#29702'."UnitPrice"]')
          ParentFont = False
        end
      end
      object PageFooter1: TfrxPageFooter
        FillType = ftBrush
        Frame.Typ = []
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        Height = 18.897650000000000000
        ParentFont = False
        Top = 389.291590000000000000
        Width = 759.685530000000000000
        object Memo2: TfrxMemoView
          Align = baBottom
          Left = 257.008040000000000000
          Width = 245.669450000000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            #31532' [Page#] '#39029'  / '#20849' [TotalPages#] '#39029)
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object Header: TfrxPageHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 90.708705350000000000
        Top = 18.897650000000000000
        Width = 759.685530000000000000
        object Title: TfrxMemoView
          Left = 295.653680000000000000
          Top = 26.456692910000000000
          Width = 173.008040000000000000
          Height = 22.677180000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -20
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '[Title]')
          ParentFont = False
        end
        object Line1: TfrxLineView
          Align = baRight
          Left = 94.488250000000000000
          Top = 15.118112680000000000
          Width = 188.976500000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line2: TfrxLineView
          Align = baLeft
          Left = 476.220780000000000000
          Top = 15.118110236220500000
          Width = 188.976500000000000000
          Color = clBlack
          Frame.Typ = [ftTop]
        end
        object Line3: TfrxLineView
          Left = 275.905690000000000000
          Top = 52.913420000000000000
          Width = 207.874150000000000000
          Color = clBlack
          ArrowLength = 30
          Frame.Style = fsDouble
          Frame.Typ = [ftTop]
        end
        object Memo10: TfrxMemoView
          Left = 3.779530000000000000
          Top = 68.031496060000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #21457#36135#21333#20301#65306'['#21457#36135#21333#20301']')
          ParentFont = False
        end
        object Memo24: TfrxMemoView
          Left = 245.669450000000000000
          Top = 68.031496060000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #25910#36135#21333#20301#65306'['#25910#36135#21333#20301']')
          ParentFont = False
        end
        object Memo14: TfrxMemoView
          Left = 498.897960000000000000
          Top = 68.031496060000000000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #24320#31080#26085#26399#65306'['#24320#31080#26085#26399']')
          ParentFont = False
        end
        object Barcode2D1: TfrxBarcode2DView
          Left = 7.559060000000000000
          Top = 11.338590000000000000
          Width = 42.000000000000000000
          Height = 42.000000000000000000
          StretchMode = smActualHeight
          Expression = '<'#21333#25454#32534#21495'>'
          BarType = bcCodeQR
          BarProperties.Encoding = qrUTF8NoBOM
          BarProperties.QuietZone = 0
          BarProperties.ErrorLevels = ecQ
          BarProperties.PixelSize = 2
          BarProperties.CodePage = 0
          Frame.Typ = []
          Rotation = 0
          ShowText = False
          Text = '12345678'
          Zoom = 1.000000000000000000
          FontScaled = True
          QuietZone = 0
        end
        object Memo3: TfrxMemoView
          Left = 283.464750000000000000
          Top = 3.779530000000001000
          Width = 192.756030000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          HAlign = haCenter
          Memo.UTF8W = (
            '['#21517#31216']')
          ParentFont = False
        end
        object Memo11: TfrxMemoView
          Left = 514.016080000000000000
          Top = 41.574830000000000000
          Width = 177.637910000000000000
          Height = 18.897650000000000000
          AutoWidth = True
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            'No.['#21333#25454#32534#21495'] '#65293' [Page#]')
          ParentFont = False
          Formats = <
            item
            end
            item
            end>
        end
      end
      object ColumnFooter1: TfrxColumnFooter
        FillType = ftBrush
        Frame.Typ = []
        Height = 68.031540000000000000
        Top = 298.582870000000000000
        Width = 759.685530000000000000
        object Memo1: TfrxMemoView
          Align = baLeft
          Width = 445.984540000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 4.000000000000000000
          GapY = 4.000000000000000000
          Memo.UTF8W = (
            #22823#20889#65306'[MoneyConvert( SUM( <'#36827#38144#31649#29702'."SumMonery">) )] ')
          ParentFont = False
        end
        object Memo36: TfrxMemoView
          Align = baLeft
          Left = 445.984540000000000000
          Width = 268.346630000000000000
          Height = 22.677165350000000000
          DisplayFormat.FormatStr = '%2.2m'
          DisplayFormat.Kind = fkNumeric
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          Frame.Width = 0.500000000000000000
          GapX = 4.000000000000000000
          GapY = 4.000000000000000000
          Memo.UTF8W = (
            #21512#35745#37329#39069#65306'  [SUM( <'#36827#38144#31649#29702'."SumMonery">)]  '#20803)
          ParentFont = False
        end
        object Memo5: TfrxMemoView
          Left = 245.669450000000000000
          Top = 26.456709999999990000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            #32463#25163#20154#65306'['#32463#25163#20154']')
          ParentFont = False
        end
        object Memo31: TfrxMemoView
          Left = 245.669450000000000000
          Top = 45.354347800000030000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            #31614#25910#20154#65306'['#31614#25910#20154']')
          ParentFont = False
        end
        object Memo32: TfrxMemoView
          Left = 3.779530000000000000
          Top = 45.354347800000030000
          Width = 185.196847950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            #21457#36135#20154#65306'['#21457#36135#20154']')
          ParentFont = False
        end
        object Memo33: TfrxMemoView
          Left = 498.897960000000000000
          Top = 26.456709999999990000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            #20132#26131#26041#24335#65306'['#20132#26131#26041#24335']')
          ParentFont = False
        end
        object Memo23: TfrxMemoView
          Left = 498.897960000000000000
          Top = 45.354347800000030000
          Width = 188.976377950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Frame.Width = 0.500000000000000000
          Memo.UTF8W = (
            #31614#25910#26085#26399#65306'['#31614#25910#26085#26399']')
          ParentFont = False
        end
        object Memo4: TfrxMemoView
          Left = 3.779530000000000000
          Top = 26.456709999999990000
          Width = 185.196847950000000000
          Height = 18.897650000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #31080#25454#29366#24577#65306'['#31080#25454#29366#24577']')
          ParentFont = False
        end
      end
      object ColumnHeader1: TfrxColumnHeader
        FillType = ftBrush
        Frame.Typ = []
        Height = 22.677180000000000000
        Top = 132.283550000000000000
        Width = 759.685530000000000000
        object Memo19: TfrxMemoView
          Left = 721.889766220000000000
          Top = 22.677179999999990000
          Width = 18.897650000000000000
          Height = 260.787570000000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          Frame.Typ = []
          Memo.UTF8W = (
            #19968#32852#23384#26681#12288#12288#12288#20108#32852#23458#25143#12288#12288#12288#19977#32852#36130#21153)
          ParentFont = False
        end
        object Memo7: TfrxMemoView
          Align = baLeft
          Width = 37.795300000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapY = 4.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #24207#21495)
          ParentFont = False
        end
        object Memo8: TfrxMemoView
          Align = baLeft
          Left = 37.795300000000000000
          Width = 151.181200000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapY = 4.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #21830#21697#21517#31216)
          ParentFont = False
        end
        object Memo12: TfrxMemoView
          Align = baLeft
          Left = 328.819110000000000000
          Width = 56.692950000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapY = 4.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #35268#26684)
          ParentFont = False
        end
        object Memo13: TfrxMemoView
          Align = baLeft
          Left = 423.307360000000000000
          Width = 68.031496060000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapY = 4.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #25968#37327)
          ParentFont = False
        end
        object Memo16: TfrxMemoView
          Align = baLeft
          Left = 559.370396060000000000
          Width = 154.960671420000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapY = 4.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #37329#39069)
          ParentFont = False
        end
        object Memo20: TfrxMemoView
          Align = baLeft
          Left = 385.512060000000000000
          Width = 37.795300000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapY = 4.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #21333#20301)
          ParentFont = False
        end
        object Memo26: TfrxMemoView
          Align = baLeft
          Left = 188.976500000000000000
          Width = 139.842610000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapY = 4.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #22411#21495)
          ParentFont = False
        end
        object Memo17: TfrxMemoView
          Align = baLeft
          Left = 491.338856060000000000
          Width = 68.031540000000000000
          Height = 22.677165350000000000
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clBlack
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          Frame.Typ = [ftLeft, ftRight, ftTop, ftBottom]
          GapY = 4.000000000000000000
          HAlign = haCenter
          Memo.UTF8W = (
            #21333#20215)
          ParentFont = False
        end
      end
    end
  end
  object frxBarCode: TfrxBarCodeObject
    Left = 188
    Top = 128
  end
  object frxChart: TfrxChartObject
    Left = 104
    Top = 192
  end
  object ds: TDataSource
    DataSet = dsReport
    Left = 24
    Top = 128
  end
  object dsReport: TClientDataSet
    Aggregates = <>
    Params = <>
    Left = 24
    Top = 72
  end
  object frxRichObject1: TfrxRichObject
    Left = 305
    Top = 214
  end
  object frxOLEObject1: TfrxOLEObject
    Left = 305
    Top = 262
  end
  object frxMapObject1: TfrxMapObject
    Left = 209
    Top = 222
  end
  object frxReportCellularTextObject1: TfrxReportCellularTextObject
    Left = 105
    Top = 254
  end
  object frxCrossObject1: TfrxCrossObject
    Left = 97
    Top = 310
  end
  object frxCheckBoxObject1: TfrxCheckBoxObject
    Left = 97
    Top = 358
  end
  object frxGradientObject1: TfrxGradientObject
    Left = 217
    Top = 294
  end
  object frxGZipCompressor1: TfrxGZipCompressor
    Left = 33
    Top = 206
  end
  object frxReportTableObject1: TfrxReportTableObject
    Left = 25
    Top = 310
  end
end
