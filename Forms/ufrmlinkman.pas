unit ufrmlinkman;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls,TreeUtils, DB, ADODB, ExtCtrls, RzButton, RzPanel;

type
  Tfrmlinkman = class(TForm)
    TreeView1: TTreeView;
    Qry: TADOQuery;
    RzPanel1: TRzPanel;
    RzBitBtn1: TRzBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure RzBitBtn1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    FTree: TTreeUtils;
  end;

var
  frmlinkman: Tfrmlinkman;

implementation

uses
   uDataModule,global,TreeFillThrd,ufrmMain,ufrmSort;

{$R *.dfm}

procedure Tfrmlinkman.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  FTree.Free;
end;

procedure Tfrmlinkman.FormShow(Sender: TObject);
begin
  Self.TreeView1.Items.Clear;
  FTree := TTreeUtils.Create(Self.TreeView1,
                                      DM.ADOconn,
                                      'pay_tree',
                                      'parent',
                                      'PID',
                                      'Name',
                                      m_Code,
                                      m_money ,
                                      m_dwPhoto ,
                                      m_Remarks,
                                      'Price',
                                      'num',
                                      'Input_time',
                                      'End_time',
                                      'PatType',
                                      '��ϵ��'
                                      );
  FTree.FillTree(0,g_pacttype);
end;

procedure Tfrmlinkman.RzBitBtn1Click(Sender: TObject);
var
  Child : TfrmSort;
begin
  FTree.Free;
  Child := TfrmSort.Create(Application);
  try
    Child.ShowModal;
  finally
  end;
  FormShow(Sender);
end;

procedure QueryCall(Qry : TADOQuery ; ATabName , ACode : string);
var
  s : string;
  Caption : string;
  
begin
  s := 'select * from ' + ATabName +' where '+ m_Code +'="' + ACode + '"';
  with qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    Open;
    {
    frmPayInput.cxComboBox1.Clear;
    while not Eof do
    begin
      Caption := FieldByName('Caption').AsString;
      frmPayInput.cxComboBox1.Properties.Items.Add(Caption);
      Next;
    end;
    frmPayInput.cxComboBox1.DroppedDown:=True;
    }
  end;
end;

procedure Tfrmlinkman.TreeView1Change(Sender: TObject; Node: TTreeNode);
var
  Nod: TNodeData;
  szNode : PNodeData;
  szCode : string;
  s : string;

begin
  if Assigned(Node) then
  begin
    szNode := PNodeData(Node.Data);
    szCode := szNode.Code;

    if (Node.Level > 0) and (Node.Count = 0) then
    begin
      try

        if Length(szCode) <> 0 then
        begin
          QueryCall(Self.Qry,m_pay_contact,szCode);
        end;

      finally

      end;
    end;
  end;

end;

end.
