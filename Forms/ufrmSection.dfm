object frmSection: TfrmSection
  Left = 0
  Top = 0
  BorderStyle = bsDialog
  Caption = #37096#38376#31649#29702
  ClientHeight = 462
  ClientWidth = 293
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnActivate = FormActivate
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Grid: TcxGrid
    Left = 0
    Top = 58
    Width = 293
    Height = 404
    Align = alClient
    TabOrder = 0
    object tvSection: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      OnEditing = tvSectionEditing
      OnEditKeyDown = tvSectionEditKeyDown
      DataController.DataSource = DataSource1
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      OptionsData.CancelOnExit = False
      OptionsData.Deleting = False
      OptionsData.DeletingConfirmation = False
      OptionsSelection.InvertSelect = False
      OptionsSelection.MultiSelect = True
      OptionsSelection.CellMultiSelect = True
      OptionsView.DataRowHeight = 26
      OptionsView.GroupByBox = False
      OptionsView.HeaderHeight = 23
      OptionsView.Indicator = True
      OptionsView.IndicatorWidth = 20
      object tvSectionColumn1: TcxGridDBColumn
        Caption = #24207#21495
        PropertiesClassName = 'TcxTextEditProperties'
        OnGetDisplayText = tvSectionColumn1GetDisplayText
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Sorting = False
        Width = 50
      end
      object tvSectionColumn2: TcxGridDBColumn
        Caption = #37096#38376#31867#22411
        DataBinding.FieldName = 'section'
        HeaderAlignmentHorz = taCenter
        Options.Filtering = False
        Options.Sorting = False
        Width = 212
      end
    end
    object Lv: TcxGridLevel
      GridView = tvSection
    end
  end
  object RzToolbar1: TRzToolbar
    Left = 0
    Top = 29
    Width = 293
    Height = 29
    Images = DM.cxImageList1
    BorderInner = fsNone
    BorderOuter = fsGroove
    BorderSides = [sdTop]
    BorderWidth = 0
    GradientColorStyle = gcsCustom
    TabOrder = 1
    VisualStyle = vsGradient
    ToolbarControls = (
      RzSpacer1
      RzToolButton1
      RzSpacer2
      RzToolButton2
      RzSpacer3
      RzToolButton3
      RzSpacer4
      RzToolButton4)
    object RzSpacer1: TRzSpacer
      Left = 4
      Top = 2
    end
    object RzToolButton1: TRzToolButton
      Left = 12
      Top = 2
      ImageIndex = 37
      OnClick = RzToolButton1Click
    end
    object RzSpacer2: TRzSpacer
      Left = 37
      Top = 2
    end
    object RzToolButton2: TRzToolButton
      Left = 45
      Top = 2
      ImageIndex = 3
      OnClick = RzToolButton1Click
    end
    object RzSpacer3: TRzSpacer
      Left = 70
      Top = 2
    end
    object RzToolButton3: TRzToolButton
      Left = 78
      Top = 2
      ImageIndex = 2
      OnClick = RzToolButton1Click
    end
    object RzSpacer4: TRzSpacer
      Left = 103
      Top = 2
    end
    object RzToolButton4: TRzToolButton
      Left = 111
      Top = 2
      ImageIndex = 8
      OnClick = RzToolButton4Click
    end
  end
  object RzPanel7: TRzPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 287
    Height = 23
    Align = alTop
    Alignment = taLeftJustify
    BorderOuter = fsNone
    Caption = #37096#38376#31649#29702
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clHotLight
    Font.Height = -15
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
  end
  object ADOQuery1: TADOQuery
    Connection = DM.ADOconn
    CursorType = ctStatic
    Parameters = <>
    Left = 160
    Top = 121
  end
  object DataSource1: TDataSource
    DataSet = ADOQuery1
    Left = 160
    Top = 168
  end
end
