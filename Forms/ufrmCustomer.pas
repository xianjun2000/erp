unit ufrmCustomer;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, cxStyles, dxSkinscxPCPainter,
  cxCustomData, cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData,
  cxTextEdit, cxCalendar, cxDropDownEdit, Vcl.ComCtrls, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,UtilsTree,UnitADO,
  cxDBLookupComboBox, cxLabel, RzButton, RzPanel, Vcl.ExtCtrls, Vcl.StdCtrls,
  Datasnap.DBClient, Vcl.Menus,ufrmBaseController, cxButtonEdit;

type
  TfrmCustomer = class(TfrmBaseController)
    StatusBar1: TStatusBar;
    RzPanel10: TRzPanel;
    tv: TTreeView;
    RzToolbar6: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton20: TRzToolButton;
    RzSpacer21: TRzSpacer;
    RzSpacer26: TRzSpacer;
    RzToolButton19: TRzToolButton;
    pnl1: TPanel;
    bvl1: TBevel;
    Panel1: TPanel;
    Grid: TcxGrid;
    tvView: TcxGridDBTableView;
    tvViewCol1: TcxGridDBColumn;
    tvViewCol6: TcxGridDBColumn;
    tvViewCol2: TcxGridDBColumn;
    tvViewCol3: TcxGridDBColumn;
    tvViewCol4: TcxGridDBColumn;
    tvViewCol5: TcxGridDBColumn;
    tvViewCol7: TcxGridDBColumn;
    Lv: TcxGridLevel;
    RzToolbar10: TRzToolbar;
    RzSpacer77: TRzSpacer;
    RzBut2: TRzToolButton;
    RzSpacer78: TRzSpacer;
    RzBut4: TRzToolButton;
    RzSpacer79: TRzSpacer;
    RzBut5: TRzToolButton;
    RzSpacer80: TRzSpacer;
    RzBut8: TRzToolButton;
    RzSpacer1: TRzSpacer;
    RzBut1: TRzToolButton;
    RzBut7: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzSpacer4: TRzSpacer;
    RzSpacer5: TRzSpacer;
    RzBut3: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzSpacer7: TRzSpacer;
    RzSpacer8: TRzSpacer;
    RzBut6: TRzToolButton;
    cxLabel2: TcxLabel;
    cxLookupComboBox1: TcxLookupComboBox;
    dsMaster: TClientDataSet;
    dsMasterSource: TDataSource;
    pm: TPopupMenu;
    N7: TMenuItem;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    Old: TPopupMenu;
    N6: TMenuItem;
    N8: TMenuItem;
    N5: TMenuItem;
    N4: TMenuItem;
    N3: TMenuItem;
    MenuItem1: TMenuItem;
    N1: TMenuItem;
    N2: TMenuItem;
    MenuItem2: TMenuItem;
    tvViewCol8: TcxGridDBColumn;
    procedure RzToolButton18Click(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure RzBut8Click(Sender: TObject);
    procedure RzToolButton20Click(Sender: TObject);
    procedure tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure tvDblClick(Sender: TObject);
    procedure RzBut2Click(Sender: TObject);
    procedure RzBut3Click(Sender: TObject);
    procedure RzBut5Click(Sender: TObject);
    procedure RzBut4Click(Sender: TObject);
    procedure dsMasterAfterInsert(DataSet: TDataSet);
    procedure tvViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvViewDblClick(Sender: TObject);
    procedure tvViewEditing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure MenuItem1Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure RzBut6Click(Sender: TObject);
    procedure tvClick(Sender: TObject);
    procedure RzBut1Click(Sender: TObject);
    procedure tvKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvViewCol2PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxLookupComboBox1PropertiesEditValueChanged(Sender: TObject);
  private
    { Private declarations }
    g_TreeView : TNewUtilsTree;
    dwADO : TADO;
    dwNodeId : Integer;
    procedure UpDateIndex(lpNode : TTreeNode);
    procedure ReadCustomerData(IsDelete : Boolean);
    function IsDelete(lpValue : Integer):Boolean;
  public
    { Public declarations }
    dwOfferType : Byte;
  end;

var
  frmCustomer: TfrmCustomer;

implementation

uses
   ufrmCustomerPlate,uDataModule,global,ufunctions,FillThrdTree,ufrmCompanyManage;

{$R *.dfm}


function DeteleData(lvNodeIdList : string):Boolean;
var
  lvSQLText : string;
begin
  Result := False;
//  DM.ADOconn.BeginTrans; //开始事务
  try
    if lvNodeIdList <> '' then
    begin
      with DM.Qry do
      begin
        lvSQLText := 'delete * from '+ g_Table_Decorate_OfferBranchTree +' where Parent in('+ lvNodeIdList  +')';
        Close;
        SQL.Text := lvSQLText;
        ExecSQL;
        lvSQLText := 'delete * from '+ g_Table_Decorate_OfferList +' where tages in('+ lvNodeIdList  +')';
        Close;
        SQL.Text := lvSQLText;
        ExecSQL;
        Close;
        lvSQLText := 'delete * from '+ g_Table_Decorate_OfferInfo +' where tages in('+ lvNodeIdList  +')';
        SQL.Text := lvSQLText;
        ExecSQL;
        Close;
        lvSQLText := 'delete * from ' + g_Table_Decorate_OfferSumCheck + ' where tages in('+ lvNodeIdList  +')';
        SQL.Text  := lvSQLText;
        ExecSQL;
      end;
    //  DM.ADOconn.Committrans; //提交事
      ShowMessage('删除成功！');
      Result := True;
    end;

    {
    DM.InDeleteData(g_Table_Decorate_OfferBranchTree,szCodeList);
    Self.MenuItem1.Click;
    }

  except
    on E:Exception do
    begin
    //  DM.ADOconn.RollbackTrans;           // 事务回滚
    //  ShowMessage(e.Message);
    end;
  end;

end;

function GetParentList(lpWhere , lpCodeList : string ; lpIndex : Byte = 0):string;
var
  lvSQLText : string;
  s : string;
begin
  case lpIndex of
    0: s := ' in (' + lpCodeList  +')';
    1: s := ' in (''' + lpCodeList  +''')';
  end;
  with DM.Qry do
  begin

    lvSQLText := 'Select parent from ' + g_Table_Decorate_OfferBranchTree + ' WHERE '+ lpWhere + s;
    Close;
    SQL.Text := lvSQLText;
    Open;
    Result := '';
    while not Eof do
    begin
      if Length(Result) = 0 then
        Result := Fields[0].AsString
      else
        Result := Result + ',' + Fields[0].AsString;
      Next;
    end;

  end;
end;

function TfrmCustomer.IsDelete(lpValue : Integer):Boolean;
var
  i : Integer;
  szCount : Integer;
  szRowsCount : string;
  szIndex : Integer;
  s , szSQLText , str : string;

begin
  szCount := Self.tvView.Controller.SelectedRowCount;
  szRowsCount := IntToStr(szCount) ;
  if szCount >= 0 then
  begin

      with Self.tvView  do
      begin

        for I := szCount -1 downto 0 do
        begin
          s :=  VarToStr(Controller.SelectedRows[i].Values[ Self.tvViewCol7.Index ] );
        //  s :=  IntToStr( DataController.DataSet.FieldByName('parent').AsInteger );
          if Length(str) = 0 then
            str := s
          else
            str := str + ',' + s;
        end;

        szSQLText := 'UPDATE ' + g_Table_Decorate_OfferBranchTree +
                     ' SET IsDelete='+ IntToStr(lpValue) +' where ' +
                     Self.tvViewCol7.DataBinding.FieldName + ' in(' + str + ')';

        DM.ADOconn.BeginTrans; //开始事务
        try
          with DM.Qry do
          begin
            Close;
            SQL.Text := szSQLText;
            ExecSQL;
          end;
          DM.ADOconn.Committrans; //提交事

        except
          on E:Exception do
          begin
            DM.ADOconn.RollbackTrans;           // 事务回滚
          end;
        end;

      end;

  end;

end;


procedure TfrmCustomer.ReadCustomerData(IsDelete : Boolean);
var
  lvSQLText : string;
begin
  lvSQLText := 'Select * from ' + g_Table_Decorate_OfferBranchTree + ' WHERE ' +  'TreeId=' + IntToStr(dwNodeId) +' AND PID=0 AND IsDelete=' + BoolToStr(IsDelete,True);
  dwADO.OpenSQL(Self.dsMaster,lvSQLText);
end;

procedure TfrmCustomer.UpDateIndex(lpNode : TTreeNode);
var
  s : string;
begin
  if lpNode <> nil then
  begin
    S := 'UPDATE ' + g_Table_Decorate_OfferTree + ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''' WHERE SignName="' + lpNode.Text + '"';
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := s;
      ExecSQL;
    end;
  end;
end;

procedure TfrmCustomer.cxLookupComboBox1PropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  RzBut1.Click;
end;

procedure TfrmCustomer.dsMasterAfterInsert(DataSet: TDataSet);
var
  parent : Integer;
  lvNode : TTreeNode;

begin
  inherited;
  lvNode := Self.tv.Selected;
  with DataSet do
  begin
    parent := DM.getLastId( g_Table_Decorate_OfferBranchTree );
    FieldByName('parent').Value := Parent;
    FieldByName('Code').Value   := GetRowCode( g_Table_Decorate_OfferBranchTree ,'Code');
    FieldByName('TreeId').Value := dwNodeId;
    FieldByName(tvViewCol3.DataBinding.FieldName).Value := Date;
  end;
end;

procedure TfrmCustomer.FormActivate(Sender: TObject);
begin
  g_TreeView := TNewUtilsTree.Create(Self.tv,DM.ADOconn,g_Table_Decorate_OfferTree,'分类');
  g_TreeView.GetTreeTable(0);
end;

procedure TfrmCustomer.MenuItem1Click(Sender: TObject);
begin
  inherited;
  //历史工程
  Self.RzBut1.Enabled := False;
  Self.RzBut2.Enabled := False;
  Self.RzBut3.Enabled := False;
  Self.RzBut4.Enabled := False;
  Self.RzBut5.Enabled := False;

  Self.MenuItem2.Enabled := True;
  Self.N1.Enabled := True;

  Self.N9.Enabled := True;
  Self.N11.Enabled:= True;
  ReadCustomerData(True);
end;

procedure TfrmCustomer.MenuItem2Click(Sender: TObject);
var
  szTableList : array[0..3] of TTableList;
  szCodeList  : string;
  szParent : Integer;
  szCode : string;
  lvSQLText : string;
  lvNodeIdList : string;
begin
  inherited;
  //彻底删除
  if Application.MessageBox( '是否需要要彻底删除数据，删除后无法恢复？', '提示:',
     MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin

    if Self.tvView.Controller.SelectedRowCount = 1 then
    begin

      with Self.tvView.DataController.DataSet do
      begin
        if not IsEmpty then
        begin
          szCode := FieldByName('Code').AsString;
          szParent := FieldByName('parent').AsInteger;
        end;

      end;
      DM.getTreeCode(g_Table_Decorate_OfferBranchTree, szParent,szCodeList); //获取要删除的id表

      lvNodeIdList := GetParentList('Code',szCodeList,1);
      if DeteleData(lvNodeIdList) then
        Self.MenuItem1.Click;
    end else
    begin
      if Self.tvView.Controller.SelectedRowCount > 1 then
      begin
        Application.MessageBox( '禁止同时删除多条程，只允许一次一条！', '提示:', MB_OKCANCEL + MB_ICONWARNING) ;
      end;
    end;

  end;

end;

procedure TfrmCustomer.N1Click(Sender: TObject);
begin
  //恢复工程
  if Application.MessageBox( '是否要恢复所有选中的数据？', '恢复数据？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    IsDelete(0);
    Self.MenuItem1.Click;
  end;
end;

procedure TfrmCustomer.RzBut1Click(Sender: TObject);
var
  s : string;
  lvSQLText : string;
begin
  inherited;
  //搜索
  s := Self.cxLookupComboBox1.Text;
  lvSQLText := 'Select * from ' + g_Table_Decorate_OfferBranchTree +
  ' WHERE ' +  'TreeId=' + IntToStr(dwNodeId) +' AND PID=0 AND IsDelete=0 AND ' + tvViewCol2.DataBinding.FieldName + '="' + s +'"';

  dwADO.OpenSQL(Self.dsMaster,lvSQLText);
end;

procedure TfrmCustomer.RzBut2Click(Sender: TObject);
begin
  dwADO.ADOAppend(Self.dsMaster);
end;

procedure TfrmCustomer.RzBut3Click(Sender: TObject);
begin
  dwADO.ADOIsEdit(Self.dsMaster);
end;

procedure TfrmCustomer.RzBut4Click(Sender: TObject);
begin
  if dwADO.UpdateTableData(g_Table_Decorate_OfferBranchTree , 'TreeID',0,Self.dsMaster) then
  begin
    ShowMessage('保存成功');
    ReadCustomerData(False);
  end else
  begin
    ShowMessage('保存失败');
  end;
end;

procedure TfrmCustomer.RzBut5Click(Sender: TObject);
begin
  inherited;

  if  Application.MessageBox( '您要删除所有选中的记录吗？', '删除记录？', MB_OKCANCEL + MB_ICONWARNING) = IDOK then
  begin
    IsDelete(1);
    Self.RzBut6.Click();
  end;
end;

procedure TfrmCustomer.RzBut6Click(Sender: TObject);
begin
  inherited;
  //刷新
  Self.RzBut1.Enabled := True;
  Self.RzBut2.Enabled := True;
  Self.RzBut3.Enabled := True;
  Self.RzBut4.Enabled := True;
  Self.RzBut5.Enabled := True;
  Self.RzBut6.Enabled := True;

  Self.MenuItem2.Enabled := False;
  Self.N1.Enabled := False;

  Self.N9.Enabled := False;
  Self.N11.Enabled:= False;

  GetMaintainInfo;

  tvDblClick(Sender);
end;

procedure TfrmCustomer.RzBut8Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmCustomer.RzToolButton18Click(Sender: TObject);
  procedure TreeAddName(Tv : TTreeView); //树增加名称
  var
    Node , ChildNode : TTreeNode;
    szCode : string;

  begin
    with Tv do
    begin
      Node := Selected;
      if Assigned(Node) then
      begin
        szCode := GetRowCode( g_Table_Decorate_OfferTree ,'Code','901007',170000);
        g_TreeView.AddChildNode(szCode,'',Date,m_OutText);
      end;

    end;

  end;
var
  Node : TTreeNode;
  szCodeList : string;
  szColName  : string;
  szTableList : array[0..1] of TTableList;

begin
  inherited;
//新增
//sk_Decorate_OfferBranchTree
//sk_Decorate_OfferTree  往来单位
//sk_Decorate_OfferList  项目列表
  if sender  = Self.RzToolButton18 then
  begin
    //添加
    TreeAddName(Self.tv);
  end else
  if Sender = Self.RzToolButton19 then
  begin
    //编辑
    Node := Self.tv.Selected;
    if Node.Level <= 1 then
    begin
      MessageBox(handle, PChar('不可以编辑顶级节点?'),'提示',
             MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2)
    end else
    begin
      Node.EditText;
    end;

  end;

end;

procedure TfrmCustomer.RzToolButton20Click(Sender: TObject);
var
  Node : TTreeNode;
  szCodeList : string;
  I: Integer;
  lvSQLText : string;
  lvNodeIdList : string;

begin
  //删除
  if MessageBox(handle, '删除分类节点并同时彻底删除相关下所有数据，是否继续？', '提示', MB_ICONQUESTION + MB_YESNO) = IDYES then
  begin
    Node := Self.Tv.Selected;
    if Assigned(Node) then
    begin
      if Node.Level >= 1 then
      begin
        Self.dsMaster.Close;
        GetIndexList(Node,szCodeList);
       // DELETE u FROM sk_Decorate_OfferBranchTree u INNER JOIN sk_Decorate_OfferList r ON r.tages = u.Parent WHERE u.TreeId in (2,1);
        lvNodeIdList := GetParentList('TreeId',szCodeList);
        DeteleData(lvNodeIdList);
        g_TreeView.DeleteTree(Node);
      end;

    //  dwTreeView.DeleteTree(Node);
    //  ReadData(dwNodeIndex);//读数据
    end;

  end;
end;

procedure TfrmCustomer.tvClick(Sender: TObject);
var
  lvNode : TTreeNode;
  PData: PNodeData;
begin
  inherited;
  lvNode := Self.tv.Selected;
  if Assigned(lvNode) then
  begin
    PData := PNodeData(lvNode.Data);
    if Assigned(PData) then dwNodeId   := pData^.Index;
  end;
end;

procedure TfrmCustomer.tvDblClick(Sender: TObject);
var
  lvNode : TTreeNode;
  PData: PNodeData;
  lvTreeCode : string;
  lvNodeText : string;
begin
  lvNode := Self.tv.Selected;
  if Assigned(lvNode) then
  begin
    PData := PNodeData(lvNode.Data);
    if Assigned(PData) then
    begin
      dwNodeId   := pData^.Index;
      lvTreeCode := pData^.Code;
      lvNodeText := lvNode.Text;
      ReadCustomerData(False);
    //  g_Table_Decorate_OfferBranchTree : string = 'sk_Decorate_OfferBranchTree';//报价分支往来单位
    //  g_Table_sk_Decorate_OfferList    : string = 'sk_Decorate_OfferList'; //报价明细列表
    end;
  end;
end;

procedure TfrmCustomer.tvEdited(Sender: TObject; Node: TTreeNode;
  var S: string);
  function IstreeEdit(S : string ; Node: TTreeNode):Boolean;
  var
    szCodeList : string;
    PrevNode , NextNode : TTreeNode;

  begin
    if s = m_OutText then
    begin
      treeCldnode(Node,szCodeList);
      g_TreeView.DeleteTree(Node);
    end else
    begin
      g_TreeView.ModifyNodeSignName(s,Node);
      UpDateIndex(Node);
      PrevNode   := Tv.Selected.getPrevSibling;//上
      if PrevNode.Index <> -1 then UpDateIndex(PrevNode);
      NextNode   := Tv.Selected.getNextSibling;//下
      if NextNode.Index <> -1 then UpDateIndex(NextNode);
    end;
  end;
begin
  IstreeEdit(s,Node);
end;

procedure TfrmCustomer.tvKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    Self.tv.Selected.Parent.Selected := True;
    Self.RzToolButton18.Click;
  end else
  if Key = 46 then
  begin
    Self.RzToolButton20.Click;
  end;
end;

procedure TfrmCustomer.tvViewCol1GetDisplayText(Sender: TcxCustomGridTableItem;
  ARecord: TcxCustomGridRecord; var AText: string);
begin
  AText := IntToStr(ARecord.Index + 1);
end;

procedure TfrmCustomer.tvViewCol2PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmCompanyManage;
  lvTelephone : Variant;

begin
  inherited;
  Child := TfrmCompanyManage.Create(Application);
  try
    Child.dwIsReadonly := True;
    Child.ShowModal;

    tvViewCol2.EditValue := Child.dwSignName;
    tvViewCol8.EditValue := Child.tvViewColumn1.EditValue;
  finally
    Child.Free;
  end;
end;

procedure TfrmCustomer.tvViewDblClick(Sender: TObject);
var
  szRowCount : Integer;
  lvParentIndex : Integer;

begin
  inherited;
  szRowCount := Self.tvView.Controller.SelectedRecordCount;
  if szRowCount = 1 then
  begin
    with Self.dsMaster do
    begin
      if State <> dsInactive then
      begin
        lvParentIndex := FieldByName('parent').AsInteger;
        Application.CreateForm(TfrmCustomerPlate,frmCustomerPlate);
        frmCustomerPlate.dwParentNodeId := dwNodeId;
        frmCustomerPlate.dwParentId := lvParentIndex;
        frmCustomerPlate.dwOfferType:= dwOfferType;
        frmCustomerPlate.dwCustomerPhoneNumber := tvViewCol8.EditValue;
        frmCustomerPlate.Show;
      end;
    end;
    Close;
  end else
  begin
    if szRowCount > 0 then
    begin
      ShowMessage('选择太多往来单位，无法确定要进入哪个！');
    end;
  end;
end;

procedure TfrmCustomer.tvViewEditing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
begin
  dwADO.ADOIsEditing(Self.dsMaster,AAllow);
end;

end.
