unit ufrmSelectType;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ImgList, RzButton, jpeg, ExtCtrls, RzBmpBtn;


type
  TObj = record
    index : Integer;
  end;
  pObj =^TObj;

type
  TfrmSelectType = class(TForm)
    Image1: TImage;
    RzBmpButton1: TRzBmpButton;
    RzBmpButton2: TRzBmpButton;
    RzBmpButton3: TRzBmpButton;
    RzBmpButton4: TRzBmpButton;
    RzBmpButton5: TRzBmpButton;
    RzBmpButton6: TRzBmpButton;
    RzBmpButton7: TRzBmpButton;
    RzBmpButton8: TRzBmpButton;
    RzBmpButton9: TRzBmpButton;
    procedure RzBmpButton1Click(Sender: TObject);
    procedure RzBmpButton2Click(Sender: TObject);
    procedure RzBmpButton3Click(Sender: TObject);
    procedure RzBmpButton4Click(Sender: TObject);
    procedure RzBmpButton5Click(Sender: TObject);
    procedure RzBmpButton6Click(Sender: TObject);
    procedure RzBmpButton7Click(Sender: TObject);
    procedure RzBmpButton8Click(Sender: TObject);
    procedure RzBmpButton9Click(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
  public
    { Public declarations }
    procedure ShowMain(AIndex : Integer);
  end;

var
  frmSelectType: TfrmSelectType;

implementation

uses
    global,ufrmMain,ufrmBranch;

{$R *.dfm}

procedure TfrmSelectType.ShowMain(AIndex : Integer);
var
  Child : TfrmBranch;
begin
  g_pacttype := AIndex;
  g_IsSelect := True;
  Child := TfrmBranch.Create(Self);
  Child.Show;
  Close;
end;

procedure TfrmSelectType.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
  
end;

procedure TfrmSelectType.RzBmpButton1Click(Sender: TObject);
begin
  ShowMain(1);
end;

procedure TfrmSelectType.RzBmpButton2Click(Sender: TObject);
begin
  ShowMain(2);
end;

procedure TfrmSelectType.RzBmpButton3Click(Sender: TObject);
begin
  ShowMain(3);
end;

procedure TfrmSelectType.RzBmpButton4Click(Sender: TObject);
begin
  ShowMain(0);
end;

procedure TfrmSelectType.RzBmpButton5Click(Sender: TObject);
begin
  ShowMain(5);
end;

procedure TfrmSelectType.RzBmpButton6Click(Sender: TObject);
begin
  ShowMain(6);
end;

procedure TfrmSelectType.RzBmpButton7Click(Sender: TObject);
begin
  ShowMain(8);
end;

procedure TfrmSelectType.RzBmpButton8Click(Sender: TObject);
begin
  ShowMain(4);
end;

procedure TfrmSelectType.RzBmpButton9Click(Sender: TObject);
begin
  ShowMain(9);
end;

end.
