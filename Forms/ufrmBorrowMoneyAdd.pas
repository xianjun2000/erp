unit ufrmBorrowMoneyAdd;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, RzEdit, ComCtrls, Mask, RzButton, ExtCtrls, RzPanel, RzTabs;

type
  TfrmBorrowMoneyAdd = class(TForm)
    procedure RzBitBtn11Click(Sender: TObject);
    procedure RzBitBtn10Click(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzBitBtn5Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBorrowMoneyAdd: TfrmBorrowMoneyAdd;
  {
  g_TabName : string = 'sk_BorrowMoney_Tree';
  g_Code : Integer;
  g_Modify_Code : string;

  g_Borrow_Prefix  : string = '';
  g_Borrow_Suffix  : string = '';
  g_loan_Prefix    : string = '';
  g_loan_Suffix    : string = '';
  }

implementation

uses
   uDataModule,global,ufrmBorrowMoneyAccount,TreeFillThrd;

{$R *.dfm}

procedure TfrmBorrowMoneyAdd.FormCreate(Sender: TObject);
begin
  //for i := 0 to Self.RzPageControl1.PageCount - 1 do Self.RzPageControl1.Pages[i].TabVisible := False;//隐藏
end;

procedure TfrmBorrowMoneyAdd.FormShow(Sender: TObject);
var
  i : Integer;
  s : string;
  t : string;
  Node : PNodeData;

begin
  {
    i := DM.getTableNum(g_TabName);
    if i <> 0 then
    begin
        s := 'SELECT MAX(编号) FROM ' + g_TabName;
        with DM.Qry do
        begin
          Close;
          SQL.Clear;
          SQL.Text := s;
          Open;
          if RecordCount <> 0 then
          begin
            t := FormatDateTime('yyyyMMdd',Now) + IntToStr(fieldvalues['expr1000'] +1 );
          end;
        end;
    end
    else
    begin
      t := FormatDateTime('yyyyMMdd',Now) + IntToStr(1);
    end;
    g_Code := StrToInt64Def( t , 0 ) ;

    case g_tabIndex of
      1:
      begin
          //借款帐目修改
          if frmBorrowMoneyAccount.ListView1.Selected <> nil then
          begin
            i := frmBorrowMoneyAccount.ListView1.ItemIndex;
            Node := PNodeData(frmBorrowMoneyAccount.ListView1.Items[i].Data);
            g_Modify_Code := Node.Code;
            
            Self.RzEdit1.Text := Node.Caption;
            Self.DateTimePicker3.DateTime := Node.Input_time;
            Self.DateTimePicker4.DateTime := Node.End_time;
            Self.RzMemo1.Text := Node.remarks;

          end;
      end;
      2:
      begin
        //贷款帐目修改
          if frmBorrowMoneyAccount.ListView2.Selected <> nil then
          begin
            i := frmBorrowMoneyAccount.ListView2.ItemIndex;
            Node := PNodeData(frmBorrowMoneyAccount.ListView2.Items[i].Data);

            g_Modify_Code := Node.Code;
            
            Self.RzEdit1.Text := Node.Caption;
            Self.DateTimePicker3.DateTime := Node.Input_time;
            Self.DateTimePicker4.DateTime := Node.End_time;
            Self.RzMemo1.Text := Node.remarks;
          end;
      end;
      
    end;
  }
end;

procedure TfrmBorrowMoneyAdd.RzBitBtn10Click(Sender: TObject);
var
  i : Integer;
  szCode   : string;
  szName  : string;
  szPactRemarks:string;
  sqlstr : string;
  parent : Integer;
  Input_time : string;
  End_time   : string;

begin
{
  parent := DM.getLastId(g_TabName);
  if parent > 0 then
  begin
      szCode := g_Borrow_Prefix + IntToStr( g_Code );
      i := DM.SelectCode(g_TabName,szCode);
      if i = 0 then
      begin
        szName := Self.RzEdit16.Text;
        if Length(szName ) = 0 then
        begin
          Application.MessageBox('请输入帐目名称!',m_title,MB_OK + MB_ICONQUESTION)
        end
        else
        begin
          Input_time := FormatDateTime('yyyy-MM-dd HH:mm:ss',Self.DateTimePicker1.DateTime);
          End_time   := FormatDateTime('yyyy-MM-dd HH:mm:ss',Self.DateTimePicker2.DateTime);

          sqlstr := 'Insert into ' + g_TabName
                                   + '(parent,Code,Remarks,Input_time,End_time,PID,Name,PatType) values('
                                   + IntToStr(parent)
                                   + ',"' + szCode
                                   + '","'+ Self.RzMemo3.Text
                                   +'","' + Input_time
                                   +'","' + End_time + '",0,"'+ szName +'",'+ IntToStr( g_pacttype ) +')';
          with DM.Qry do
          begin
            Close;
            SQL.Clear;
            SQL.Text := sqlstr;
            if ExecSQL <> 0 then
            begin
               Application.MessageBox('帐目添加成功!',m_title,MB_OK + MB_ICONQUESTION);
               Close;
            end
            else
               Application.MessageBox('帐目添加失败!',m_title,MB_OK + MB_ICONQUESTION)
              
          end;

          i := StrToIntDef(szCode,0);
          Inc(i);
          g_Code := i;
        end;

      end;

  end;
  }
end;

procedure TfrmBorrowMoneyAdd.RzBitBtn11Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmBorrowMoneyAdd.RzBitBtn1Click(Sender: TObject);
var
  i : Integer;
  szName  : string;
  szPactRemarks:string;
  sqlstr : string;
  parent : Integer;
  Input_time : string;
  End_time   : string;

begin
  {
  //借款帐目修改
  i := DM.SelectCode(g_TabName,g_Modify_Code);
  if i <> 0 then
  begin
    szName := Self.RzEdit1.Text;
    if Length(szName ) = 0 then
    begin
      Application.MessageBox('请输入帐目名称!',m_title,MB_OK + MB_ICONQUESTION)
    end
    else
    begin
      Input_time := FormatDateTime('yyyy-MM-dd HH:mm:ss',Self.DateTimePicker3.DateTime);
      End_time   := FormatDateTime('yyyy-MM-dd HH:mm:ss',Self.DateTimePicker4.DateTime);
      sqlstr := 'Update '+ g_TabName + ' set Name="' + szName +'",Input_time="'+
                                                    Input_time +'",End_time="'+
                                                    End_time +'",Remarks="'+
                                                    Self.RzMemo1.Text +'" where code ="' + g_Modify_Code + '"';

      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := sqlstr;
        if ExecSQL <> 0 then
        begin
           Application.MessageBox('借款帐目修改成功!',m_title,MB_OK + MB_ICONQUESTION);
           Close;
        end
        else
           Application.MessageBox('借款帐目修改失败!',m_title,MB_OK + MB_ICONQUESTION)

      end;
    end;

  end;
  }
end;

procedure TfrmBorrowMoneyAdd.RzBitBtn3Click(Sender: TObject);
var
  i : Integer;
  szCode   : string;
  szName  : string;
  szPactRemarks:string;
  sqlstr : string;
  parent : Integer;
  Input_time : string;
  End_time   : string;

begin
  {
  parent := DM.getLastId(g_TabName);
  if parent > 0 then
  begin
      szCode := g_loan_Prefix + IntToStr( g_Code );
      i := DM.SelectCode(g_TabName,szCode);
      if i = 0 then
      begin
        szName := Self.RzEdit2.Text;
        if Length(szName ) = 0 then
        begin
          Application.MessageBox('请输入帐目名称!',m_title,MB_OK + MB_ICONQUESTION)
        end
        else
        begin
          Input_time := FormatDateTime('yyyy-MM-dd HH:mm:ss',Self.DateTimePicker5.DateTime);
          End_time   := FormatDateTime('yyyy-MM-dd HH:mm:ss',Self.DateTimePicker6.DateTime);

          sqlstr := 'Insert into ' + g_TabName
                                   + '(parent,Code,Remarks,Input_time,End_time,PID,Name,PatType) values('
                                   + IntToStr(parent)
                                   + ',"' + szCode
                                   + '","'+ Self.RzMemo2.Text
                                   +'","' + Input_time
                                   +'","' + End_time + '",0,"'+ szName +'",''1'')';
          with DM.Qry do
          begin
            Close;
            SQL.Clear;
            SQL.Text := sqlstr;
            if ExecSQL <> 0 then
            begin
               Application.MessageBox('帐目添加成功!',m_title,MB_OK + MB_ICONQUESTION);
               Close;
            end
            else
               Application.MessageBox('帐目添加失败!',m_title,MB_OK + MB_ICONQUESTION)
              
          end;

          i := StrToIntDef(szCode,0);
          Inc(i);
          g_Code := i;
        end;

      end;
      
  end;  
  }
end;

procedure TfrmBorrowMoneyAdd.RzBitBtn5Click(Sender: TObject);
var
  i : Integer;
  szName  : string;
  szPactRemarks:string;
  sqlstr : string;
  parent : Integer;
  Input_time : string;
  End_time   : string;

begin
  {
  //贷款帐目修改
  i := DM.SelectCode(g_TabName,g_Modify_Code);
  if i <> 0 then
  begin
    szName := Self.RzEdit3.Text;
    if Length(szName ) = 0 then
    begin
      Application.MessageBox('请输入帐目名称!',m_title,MB_OK + MB_ICONQUESTION)
    end
    else
    begin
      Input_time := FormatDateTime('yyyy-MM-dd HH:mm:ss',Self.DateTimePicker7.DateTime);
      End_time   := FormatDateTime('yyyy-MM-dd HH:mm:ss',Self.DateTimePicker8.DateTime);
      sqlstr := 'Update '+ g_TabName + ' set Name="' + szName +'",Input_time="'+
                                                    Input_time +'",End_time="'+
                                                    End_time +'",Remarks="'+
                                                    Self.RzMemo4.Text +'" where code ="' + g_Modify_Code + '"';

      with DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := sqlstr;
        if ExecSQL <> 0 then
        begin
           Application.MessageBox('贷款帐目修改成功!',m_title,MB_OK + MB_ICONQUESTION);
           Close;
        end
        else
           Application.MessageBox('贷款帐目修改失败!',m_title,MB_OK + MB_ICONQUESTION)

      end;
    end;

  end;
  }
end;

end.
