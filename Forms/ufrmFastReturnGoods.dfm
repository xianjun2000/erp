inherited frmFastReturnGoods: TfrmFastReturnGoods
  Caption = #24555#25463#21333'-'#36864#36135
  PixelsPerInch = 96
  TextHeight = 13
  inherited RzToolbar4: TRzToolbar
    ToolbarControls = (
      RzSpacer26
      RzToolButton1
      RzSpacer1
      RzToolButton7
      RzSpacer6
      RzToolButton6
      RzSpacer2
      RzToolButton2
      RzSpacer4
      RzToolButton23
      RzSpacer31
      RzToolButton8
      RzSpacer7
      RzToolButton24
      RzSpacer32
      RzToolButton4
      RzSpacer33
      cxDBNavigator1
      RzSpacer5
      RzToolButton5
      RzSpacer3
      RzToolButton9
      RzSpacer8
      RzToolButton3)
  end
  inherited cxGroupBox1: TcxGroupBox
    inherited cxLabel7: TcxLabel
      Caption = #25910#36135#21333#20301#65306
    end
    inherited cxLabel1: TcxLabel
      Caption = #21457#36135#20179#24211#65306
    end
    inherited cxLabel3: TcxLabel
      Caption = #21457#36135#21333#20301#65306
    end
    inherited cxDBComboBox1: TcxDBComboBox
      Properties.Items.Strings = (
        #24080#30446#36864#36135#21333
        #36827#36135#36864#36135#21333
        #31199#20837#36864#36135#21333
        #20511#20837#36864#36135#21333
        #35843#20837#36864#36135#21333)
    end
    inherited cxDBButtonEdit1: TcxDBButtonEdit
      Left = 421
      Top = 21
      Hint = #21457#36135
      DataBinding.DataField = 'DeliverCompany'
      ExplicitLeft = 421
      ExplicitTop = 21
    end
    inherited cxDBButtonEdit2: TcxDBButtonEdit
      Left = 759
      Top = 22
      Hint = #25910#36135
      DataBinding.DataField = 'ReceiveCompany'
      ExplicitLeft = 759
      ExplicitTop = 22
    end
  end
  inherited cxGroupBox3: TcxGroupBox
    inherited cxDBComboBox7: TcxDBComboBox
      Properties.Items.Strings = (
        #36827#36135#36864#36135#32479#35745
        #36827#36135#36864#36135#32479#35745
        #31199#20837#36864#36135#32479#35745
        #20511#20837#36864#36135#32479#35745
        #35843#20837#36864#36135#32479#35745)
    end
  end
  inherited RzPanel5: TRzPanel
    inherited RzPanel1: TRzPanel
      inherited cxTextEdit1: TcxTextEdit
        Style.IsFontAssigned = True
      end
    end
  end
  inherited Panel2: TPanel
    inherited cxGroupBox2: TcxGroupBox
      inherited Navigator: TDBNavigator
        Hints.Strings = ()
      end
    end
  end
end
