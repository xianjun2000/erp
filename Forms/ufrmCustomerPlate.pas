unit ufrmCustomerPlate;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, RzButton, RzPanel,
  Vcl.ComCtrls, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, dxSkinsCore, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxLabel, cxStyles, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxNavigator, Data.DB, cxDBData, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, RzTabs, Vcl.Menus, cxGridCustomPopupMenu,
  cxGridPopupMenu,UtilsTree,UnitADO,FillThrdTree, Datasnap.DBClient, cxImage,
  cxMemo, cxCurrencyEdit, Vcl.StdCtrls, cxGridBandedTableView,
  cxGridDBBandedTableView, cxTL, cxTLdxBarBuiltInMenu, cxInplaceContainer,
  cxTLData, cxDBTL, dxmdaset, cxSpinEdit,ufrmBaseController, cxButtonEdit,
  dxBarBuiltInMenu, cxPC, cxButtons, cxGroupBox, dxCore, cxDateUtils, cxCalendar, Generics.Collections, {Delphi 2009 新增的泛型容器单元}
  cxDBEdit, Data.Win.ADODB, cxImageComboBox, dxGDIPlusClasses,activex, frxClass,
  frxDBSet, cxSplitter;

type
  TViewList = record
    dwFieldName : string;
    dwCellValues : Variant;
    dwCaption : TCaption;
  end;
type
  TCostList = record
    dwProjectName : string;
    dwAutoCalc    : Boolean;
    dwItemtext    : string;
    dwItemCurrent : Currency;
  end;
type
  TMyCxGrid = class(TObject)
    class procedure DrawIndicatorCell( Sender: TcxGridTableView; ACanvas: TcxCanvas;
      AViewInfo: TcxCustomGridIndicatorItemViewInfo; var ADone: Boolean);
  end;
  TfrmCustomerPlate = class(TfrmBaseController)
    pnl: TPanel;
    pnl2: TPanel;
    tv: TTreeView;
    RzToolbar6: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton20: TRzToolButton;
    RzSpacer21: TRzSpacer;
    RzSpacer26: TRzSpacer;
    RzToolButton19: TRzToolButton;
    Bevel1: TBevel;
    pnl3: TPanel;
    pnl1: TPanel;
    pnl5: TPanel;
    RzToolbar10: TRzToolbar;
    RzSpacer77: TRzSpacer;
    RzBut2: TRzToolButton;
    RzSpacer78: TRzSpacer;
    RzBut4: TRzToolButton;
    RzSpacer79: TRzSpacer;
    RzBut5: TRzToolButton;
    RzBut8: TRzToolButton;
    RzSpacer5: TRzSpacer;
    RzBut3: TRzToolButton;
    RzSpacer6: TRzSpacer;
    RzSpacer7: TRzSpacer;
    RzBut6: TRzToolButton;
    Grid1: TcxGrid;
    tvView1: TcxGridDBTableView;
    Lv1: TcxGridLevel;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    pmPrint: TPopupMenu;
    N1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    PopupMenu: TcxGridPopupMenu;
    dsMaster: TClientDataSet;
    dsSource: TDataSource;
    RzSpacer2: TRzSpacer;
    RzToolButton3: TRzToolButton;
    pm1: TPopupMenu;
    Lv2: TcxGridLevel;
    Grid2: TcxGrid;
    tvView2: TcxGridDBBandedTableView;
    tvGroupName: TcxGridDBBandedColumn;
    tvSpaceName: TcxGridDBBandedColumn;
    tvProjectName: TcxGridDBBandedColumn;
    tvView2Col1: TcxGridDBBandedColumn;
    tvView2Col2: TcxGridDBBandedColumn;
    tvView2Col3: TcxGridDBBandedColumn;
    tvView2Col4: TcxGridDBBandedColumn;
    tvView2Col5: TcxGridDBBandedColumn;
    tvView2Col6: TcxGridDBBandedColumn;
    tvView2Col7: TcxGridDBBandedColumn;
    tvView2Col8: TcxGridDBBandedColumn;
    tvView2Col9: TcxGridDBBandedColumn;
    tvView2Col10: TcxGridDBBandedColumn;
    tvView2Col11: TcxGridDBBandedColumn;
    tvView2Col12: TcxGridDBBandedColumn;
    tvView2Col13: TcxGridDBBandedColumn;
    tvView2Col14: TcxGridDBBandedColumn;
    tvView2Col15: TcxGridDBBandedColumn;
    tvView2Col16: TcxGridDBBandedColumn;
    tvView2Col17: TcxGridDBBandedColumn;
    tvView2Col18: TcxGridDBBandedColumn;
    tvView2Col19: TcxGridDBBandedColumn;
    tvView2Col20: TcxGridDBBandedColumn;
    tvView2Col21: TcxGridDBBandedColumn;
    tvView2Col22: TcxGridDBBandedColumn;
    tvView2Col23: TcxGridDBBandedColumn;
    tvView2Col24: TcxGridDBBandedColumn;
    tvView2Col25: TcxGridDBBandedColumn;
    tvView2Col26: TcxGridDBBandedColumn;
    tvView2Col27: TcxGridDBBandedColumn;
    N6: TMenuItem;
    N7: TMenuItem;
    N8: TMenuItem;
    tvGroupIndex: TcxGridDBBandedColumn;
    tvView2Col28: TcxGridDBBandedColumn;
    tvView2Col29: TcxGridDBBandedColumn;
    tvView2Col30: TcxGridDBBandedColumn;
    tvView2Col31: TcxGridDBBandedColumn;
    tvView2Col32: TcxGridDBBandedColumn;
    tvView1Col1: TcxGridDBColumn;
    dxGroupName: TdxMemData;
    dsGroupNameSource: TDataSource;
    dxGroupNameGroupName: TStringField;
    pm2: TPopupMenu;
    N9: TMenuItem;
    N10: TMenuItem;
    N11: TMenuItem;
    N12: TMenuItem;
    N13: TMenuItem;
    N15: TMenuItem;
    N16: TMenuItem;
    dslib: TClientDataSet;
    dslibSource: TDataSource;
    btn3: TButton;
    N17: TMenuItem;
    N18: TMenuItem;
    N19: TMenuItem;
    N14: TMenuItem;
    N20: TMenuItem;
    tmrClock: TTimer;
    pm3: TPopupMenu;
    N21: TMenuItem;
    N22: TMenuItem;
    N23: TMenuItem;
    N4: TMenuItem;
    N5: TMenuItem;
    N24: TMenuItem;
    tvView2Column1: TcxGridDBBandedColumn;
    tvView2Column2: TcxGridDBBandedColumn;
    N25: TMenuItem;
    cxPageControl1: TcxPageControl;
    cxTabSheet1: TcxTabSheet;
    Label1: TLabel;
    tvView2Column3: TcxGridDBBandedColumn;
    RzClient: TRzPageControl;
    RzInfo: TRzTabSheet;
    RzCustomer: TRzTabSheet;
    RzToolButton2: TRzToolButton;
    cxButton1: TcxButton;
    RzSpacer3: TRzSpacer;
    N26: TMenuItem;
    cxGroupBox2: TcxGroupBox;
    cxLabel13: TcxLabel;
    cxLabel14: TcxLabel;
    cxLabel15: TcxLabel;
    cxLabel16: TcxLabel;
    cxLabel17: TcxLabel;
    cxLabel18: TcxLabel;
    cxLabel19: TcxLabel;
    cxLabel20: TcxLabel;
    cxGroupBox1: TcxGroupBox;
    cxLabel4: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    cxLabel21: TcxLabel;
    cxLabel22: TcxLabel;
    cxLabel23: TcxLabel;
    cxLabel24: TcxLabel;
    cxLabel25: TcxLabel;
    cxLabel26: TcxLabel;
    cxLabel27: TcxLabel;
    cxLabel28: TcxLabel;
    cxLabel29: TcxLabel;
    cxLabel30: TcxLabel;
    cxLabel31: TcxLabel;
    cxLabel32: TcxLabel;
    cxLabel33: TcxLabel;
    tv1: TTreeView;
    Grid3: TcxGrid;
    tvView3: TcxGridDBTableView;
    tvView3Column1: TcxGridDBColumn;
    tvView3Column2: TcxGridDBColumn;
    Lv3: TcxGridLevel;
    Bevel2: TBevel;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxDBTextEdit4: TcxDBTextEdit;
    cxDBSpinEdit2: TcxDBSpinEdit;
    cxDBSpinEdit3: TcxDBSpinEdit;
    cxDBSpinEdit4: TcxDBSpinEdit;
    cxDBComboBox1: TcxDBComboBox;
    cxDBComboBox2: TcxDBComboBox;
    cxDBTextEdit6: TcxDBTextEdit;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxDBMemo1: TcxDBMemo;
    cxDBDateEdit1: TcxDBDateEdit;
    cxDBDateEdit2: TcxDBDateEdit;
    cxDBDateEdit3: TcxDBDateEdit;
    cxDBDateEdit4: TcxDBDateEdit;
    cxDBDateEdit5: TcxDBDateEdit;
    cxDBDateEdit6: TcxDBDateEdit;
    cxDBSpinEdit5: TcxDBSpinEdit;
    cxDBSpinEdit6: TcxDBSpinEdit;
    cxDBDateEdit8: TcxDBDateEdit;
    cxDBSpinEdit7: TcxDBSpinEdit;
    cxDBDateEdit9: TcxDBDateEdit;
    cxDBDateEdit10: TcxDBDateEdit;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    cxDBCurrencyEdit3: TcxDBCurrencyEdit;
    cxDBCurrencyEdit4: TcxDBCurrencyEdit;
    cxDBCurrencyEdit5: TcxDBCurrencyEdit;
    cxDBMemo2: TcxDBMemo;
    dsSounceInfo: TDataSource;
    dsDetailed: TClientDataSet;
    dsDetailed1: TDataSource;
    N27: TMenuItem;
    N28: TMenuItem;
    N29: TMenuItem;
    icCustomIconList: TcxImageCollection;
    icCustomIcon1: TcxImageCollectionItem;
    icCustomIconListItem1: TcxImageCollectionItem;
    tvAutoCalc: TcxGridDBBandedColumn;
    RzSpacer4: TRzSpacer;
    RzBut: TRzToolButton;
    tvView2Column4: TcxGridDBBandedColumn;
    N30: TMenuItem;
    N31: TMenuItem;
    cxDBButtonEdit1: TcxDBButtonEdit;
    cxDBButtonEdit2: TcxDBButtonEdit;
    RzToolbar1: TRzToolbar;
    RzToolButton4: TRzToolButton;
    RzSpacer8: TRzSpacer;
    cxLookupComboBox1: TcxLookupComboBox;
    cxDBButtonEdit3: TcxDBButtonEdit;
    cxDBButtonEdit4: TcxDBButtonEdit;
    cxLabel34: TcxLabel;
    cxDBComboBox3: TcxDBComboBox;
    dsClientInfo: TClientDataSet;
    N32: TMenuItem;
    N33: TMenuItem;
    N35: TMenuItem;
    N36: TMenuItem;
    N37: TMenuItem;
    frxDataset1: TfrxDBDataset;
    frxDataset2: TfrxDBDataset;
    frxDataset3: TfrxDBDataset;
    cxLabel35: TcxLabel;
    cxLabel36: TcxLabel;
    cxLabel37: TcxLabel;
    cxLabel38: TcxLabel;
    cxDBButtonEdit5: TcxDBButtonEdit;
    cxDBSpinEdit8: TcxDBSpinEdit;
    cxDBSpinEdit9: TcxDBSpinEdit;
    cxDBComboBox4: TcxDBComboBox;
    qry1: TADOQuery;
    dsReportArtert: TADOQuery;
    dsReportCost: TADOQuery;
    N34: TMenuItem;
    N38: TMenuItem;
    dsSearch: TClientDataSet;
    dsDataSearch: TDataSource;
    N39: TMenuItem;
    N40: TMenuItem;
    N41: TMenuItem;
    N45: TMenuItem;
    N44: TMenuItem;
    N46: TMenuItem;
    N47: TMenuItem;
    cxLabel40: TcxLabel;
    cxLookupComboBox2: TcxLookupComboBox;
    RzSpacer9: TRzSpacer;
    RzToolButton5: TRzToolButton;
    N42: TMenuItem;
    N43: TMenuItem;
    N48: TMenuItem;
    N49: TMenuItem;
    cxSplitter1: TcxSplitter;
    cxSplitter2: TcxSplitter;
    Panel1: TPanel;
    tvView3Column3: TcxGridDBColumn;
    cxLookupComboBox3: TcxLookupComboBox;
    cxLabel39: TcxLabel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormActivate(Sender: TObject);
    procedure RzBut8Click(Sender: TObject);
    procedure RzBut2Click(Sender: TObject);
    procedure tvView4CustomDrawGroupCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableCellViewInfo;
      var ADone: Boolean);
    procedure tvView4GetCellHeight(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      ACellViewInfo: TcxGridTableDataCellViewInfo; var AHeight: Integer);
    procedure tvView4CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure dsMasterAfterInsert(DataSet: TDataSet);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvView2GetCellHeight(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      ACellViewInfo: TcxGridTableDataCellViewInfo; var AHeight: Integer);
    procedure btn3Click(Sender: TObject);
    procedure tvGroupIndexGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure tvView2MouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure dsMasterAfterDelete(DataSet: TDataSet);
    procedure tvView1DblClick(Sender: TObject);
    procedure tvView2CustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure RzBut4Click(Sender: TObject);
    procedure dsMasterAfterClose(DataSet: TDataSet);
    procedure N16Click(Sender: TObject);
    procedure N15Click(Sender: TObject);
    procedure tvView2Col6PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure RzToolButton18Click(Sender: TObject);
    procedure tvEdited(Sender: TObject; Node: TTreeNode; var S: string);
    procedure tvView3DblClick(Sender: TObject);
    procedure RzBut5Click(Sender: TObject);
    procedure tvView2MouseEnter(Sender: TObject);
    procedure N18Click(Sender: TObject);
    procedure N20Click(Sender: TObject);
    procedure tvView2Editing(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; var AAllow: Boolean);
    procedure RzBut3Click(Sender: TObject);
    procedure tvSpaceNameCompareRowValuesForCellMerging(Sender: TcxGridColumn;
      ARow1: TcxGridDataRow; AProperties1: TcxCustomEditProperties;
      const AValue1: Variant; ARow2: TcxGridDataRow;
      AProperties2: TcxCustomEditProperties; const AValue2: Variant;
      var AAreEqual: Boolean);
    procedure tvGroupNameCompareRowValuesForCellMerging(Sender: TcxGridColumn;
      ARow1: TcxGridDataRow; AProperties1: TcxCustomEditProperties;
      const AValue1: Variant; ARow2: TcxGridDataRow;
      AProperties2: TcxCustomEditProperties; const AValue2: Variant;
      var AAreEqual: Boolean);
    procedure tvView2KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure tv1Click(Sender: TObject);
    procedure tvKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure tvView2DblClick(Sender: TObject);
    procedure tvView2EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure RzBut6Click(Sender: TObject);
    procedure N21Click(Sender: TObject);
    procedure N22Click(Sender: TObject);
    procedure N23Click(Sender: TObject);
    procedure N4Click(Sender: TObject);
    procedure N5Click(Sender: TObject);
    procedure N6Click(Sender: TObject);
    procedure N7Click(Sender: TObject);
    procedure N8Click(Sender: TObject);
    procedure N24Click(Sender: TObject);
    procedure tvView2Col22PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure N25Click(Sender: TObject);
    procedure tvDblClick(Sender: TObject);
    procedure tvView2Col22StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure tvView2Col1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure tvView2Col3PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure tvView2Col4PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure cxSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvView2DataControllerSummaryAfterSummary(ASender: TcxDataSummary);
    procedure tmrClockTimer(Sender: TObject);
    procedure RzInfoResize(Sender: TObject);
    procedure RzToolButton2Click(Sender: TObject);
    procedure N26Click(Sender: TObject);
    procedure cxDBCurrencyEdit2PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure dsClientInfoAfterInsert(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure cxDBSpinEdit7PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvView2Col6PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure N27Click(Sender: TObject);
    procedure cxDBSpinEdit1PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvView2Col24ValidateDrawValue(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; const AValue: Variant;
      AData: TcxEditValidateInfo);
    procedure N29Click(Sender: TObject);
    procedure tvView2Col24PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvGroupNameCustomDrawCell(Sender: TcxCustomGridTableView;
      ACanvas: TcxCanvas; AViewInfo: TcxGridTableDataCellViewInfo;
      var ADone: Boolean);
    procedure tvGroupNameGetDataText(Sender: TcxCustomGridTableItem;
      ARecordIndex: Integer; var AText: string);
    procedure tvGroupNameGetDisplayText(Sender: TcxCustomGridTableItem;
      ARecord: TcxCustomGridRecord; var AText: string);
    procedure RzButClick(Sender: TObject);
    procedure N31Click(Sender: TObject);
    procedure cxDBButtonEdit1PropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure RzToolButton4Click(Sender: TObject);
    procedure N1Click(Sender: TObject);
    procedure cxDBDateEdit8PropertiesEditValueChanged(Sender: TObject);
    procedure N34Click(Sender: TObject);
    procedure cxLookupComboBox2PropertiesEditValueChanged(Sender: TObject);
    procedure cxLookupComboBox2PropertiesChange(Sender: TObject);
    procedure RzToolButton5Click(Sender: TObject);
    procedure N43Click(Sender: TObject);
    procedure tvView2StylesGetContentStyle(Sender: TcxCustomGridTableView;
      ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
      var AStyle: TcxStyle);
    procedure cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
    procedure cxLookupComboBox3PropertiesCloseUp(Sender: TObject);
    procedure tvView2Col16PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure tvView2Col8PropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
  private
    { Private declarations }
    dwNoId : Integer;
    dwIsExcel : Boolean;
    dwSingleSummary: Currency;
    dwWholeListdiscount:Integer;
    dwTreeView : TNewUtilsTree;
    dwTreeOffer: TNewUtilsTree;
    dwADO : TADO;
    dwRowIndex : Integer;
    dwNodeIndex: Integer;
    dwQuantities : array[0..1] of string;
    dwSummary           : Currency; //合计金额
    dwMainMaterialTotal : Currency; //主材合计金额
    dwManpowerSummary   : Currency; //人工合计金额
    dwCostSummary       : Currency; //成本
    dwProfit            : Currency; //利润
    dwloseAfterProfit   : Currency; //折后利润
    dwAfterDismantSummary:Currency; //折后金额
    dwDirectFee : Currency;
    dwExtrasSummary : Currency;
    dwAcreage : Double; //面积
    dwSleepTimer : DWORD;
    procedure SetGridRow(lpGrid : TcxGridDBBandedTableView ; lpFileName : string);
    procedure CalculatData(Sender: TObject);
    function GridBanbsLocateRecord(View : TcxGridDBBandedTableView; FieldName, LocateValue : String) : Boolean;
    function GetGroupName(lpIsActive : Boolean):Boolean;
    function IstreeEdit(S : string ; Node: TTreeNode):Boolean;
    procedure UpDateIndex(lpNode : TTreeNode);
    procedure TreeAddName(Tv : TTreeView); //树增加名称
    procedure ReadData(lpNodeId : Integer);
    function GetSumMoney( lpIndex : Integer = 0 ):Boolean;
    procedure SetFieldName(lpIndex : Byte);
    function DoAutoCalcValidate(const AValue: Variant;var AErrorText: TCaption): Boolean;
    function GetPersonFullName(ARecord: TcxCustomGridRecord): string;
    procedure InitializeEditors(Sender: TObject);
    procedure DoAutoCalcValues();
    function SQLTotal( lpSQLText : string):Currency;
    function GroupSQLText(lpFieldName : string; lpOfferType : Byte):string;
    procedure WndProc(var message:TMessage);override; //重载窗口消息过程
    procedure IsAlwaysExpanded(lpExpanded : Boolean);
    function GetGridSummary(lpColIndex , lpIndex : Integer):Currency;
    procedure SaveInfo();
    procedure RePinert(Sender: TObject;lpReportName : string ; lpIndex : Byte = 0);
  public
    { Public declarations }
    dwParentId : Integer;
    dwParentNodeId : Integer;
    dwOfferType : Byte;
    dwExpensesName  : string;
    dwNucleusCount  : Boolean;
    dwIsTotal : Integer;
    dwTopLevelName : string;
    dwFormAutoCalc : Boolean;
    dwGroupName : string;
    dwCustomerPhoneNumber : Variant;

  end;

var
  frmCustomerPlate: TfrmCustomerPlate;

implementation

uses
   ufrmReport,
   uGUID,
   ComObj,
   System.Math,
   ufrmCustomerSummaryU,
   cxRegExpr,
   ufrmCustomerCostCalc,
   ufrmCustomerAmount,
   ufrmCustomerSummary,
   ufrmCompanyManage,
   ufrmWorkType,
   uDataModule,global,
   cxGridDBDataDefinitions,
   ufrmCustomerPost,
   ufunctions,
   ufrmCustomerProject; //引用这个是必须嘀~

{$R *.dfm}

{
var
  lvGroupCount : Integer;
  I: Integer;
  lvRowIndex : Integer;
  lvChildCount: Integer;
  GRowIndex : Integer;
  GRowID    : Integer;
  j: Integer;

begin
}
  {
  with Self.tvView2.DataController.Groups , Self.tvView2.Controller do
  begin
    lvRowIndex := DataGroupIndexByRowIndex[ FocusedRowIndex ]; //当前选中小组号
    lvChildCount := ChildCount[lvRowIndex]; //小组成员数

  end;
  Self.tvView2.DataController.Groups.ChildDataGroupIndex;
  }
  {
  ShowMessage('DataRecordCount:' + IntToStr(Self.dsMaster.RecordCount));

  for i := 0 to Self.tvView2.Controller.SelectedRowCount   -   1   do
  begin
    //判断选择的是否为组的行
    if Self.tvView2.Controller.SelectedRows[i] is TcxGridGroupRow   then
    begin

    end;

    //取得组的行的index;
    GRowIndex   :=   Self.tvView2.Controller.SelectedRows[i].Index;
    //取得组的行的ID；
  //  GRowID   :=   Self.tvView2.DataController.Groups.DataGroupIndexByRowIndex[GRowIndex];
    //这里得到的是组的行的那个标题
    ShowMessage(Self.tvView2.DataController.Groups.GroupValues[GRowID]);
    //这里是组的行数
    ShowMessage(IntToStr( Self.tvView2.DataController.Groups.ChildCount[GRowID]));
  //  Self.tvView2.DataController.Groups.ChildRecordIndex
    ShowMessage(IntToStr( Self.tvView2.DataController.Groups.Level[GRowIndex] ));

  end;
  }
//end;


procedure TfrmCustomerPlate.RePinert(Sender: TObject;lpReportName : string ; lpIndex : Byte = 0);
var
  lvReport : TfrmReport;
  s : string;
begin
  inherited;
  s := Self.cxDBComboBox1.Text;
  lvReport := TfrmReport.Create(Application);
  try

    with lvReport.dwDictionary do
    begin
      Clear;
      Add('甲方', cxDBTextEdit1.Text);
      Add('甲方电话', cxDBSpinEdit2.Text);
      Add('设计师',cxDBButtonEdit1.Text);
      Add('工程地址',Self.cxDBTextEdit6.Text);
      Add('工程面积',cxDBSpinEdit1.Text);
      Add('户型',cxDBTextEdit4.Text);
      Add('合同备注',cxDBMemo2.Text);
      Add('客户要求',cxDBMemo1.Text);
      Add('公司名称',cxDBButtonEdit5.Text);
      Add('公司电话',cxDBSpinEdit8.Text);
      Add('工程类型',cxDBComboBox1.Text);
      Add('Title',lpReportName);
      {
      Add('开票日期', Self.cxDBDateEdit1.Text);
      Add('审核签字', Self.cxDBTextEdit9.Text);
      Add('签收日期', Self.cxDBDateEdit4.Text);

      }
    end;

    lvReport.frxDBDataset.DataSource := dsSource;
    lvReport.frxDBDataset.UserName   := '报价管理';

    if Sender = N1 then
    begin
      lvReport.dwReportName := '报价预算表';
    end else
    if Sender = N2 then
    begin
      lvReport.dwReportName := '四项报价';  //四项报价
    end else
    if Sender = N3 then
    begin
      lvReport.dwReportName := '物料项目清单';  //物料项目清单
    end else
    if Sender = N32 then
    begin
      lvReport.dwReportName := '人工报价';  //人工报价
    end else
    if Sender = N33 then
    begin
      lvReport.dwReportName := '主材报价(无折扣)';  //主材报价
    end else
    if Sender = N35 then
    begin
      lvReport.dwReportName := '成本核算清单';  //成本核算清单
    end else
    if Sender = N36 then
    begin
      lvReport.dwReportName := '主材核算清单';  //主材核算清单
    end else
    if Sender = N37 then
    begin
      lvReport.dwReportName := '人工核算清单';  //人工核算清单
    end else
    if Sender = N38 then
    begin
      lvReport.dwReportName := '综合报价(无人工组)';
    end else
    if Sender = N39 then
    begin
      lvReport.dwReportName := '综合报价(折扣报价)';
    end else
    if Sender = N40 then
    begin
      lvReport.dwReportName := '报价预算表(查询)';
    end else
    if Sender = N49 then
    begin
      lvReport.dwReportName := '主材报价(带折扣)';  //主材报价
    end;


  //  lvReport.dwReportFile  := lpReportName; //报表文件
    lvReport.dwReportTitle := lpReportName; //报表标题

    if lpIndex = 1 then
    begin
      lvReport.Show;
      lvReport.Hide;
      lvReport.dwIsReportTitle := False;
      lvReport.cxButton1.Click;
      lvReport.Close;
    end else
    begin
      lvReport.ShowModal;
    end;
  finally
  end;
end;


procedure TfrmCustomerPlate.IsAlwaysExpanded(lpExpanded : Boolean);
begin
  with tvView2.DataController do
  begin
    if lpExpanded then
    begin
      Options := [dcoGroupsAlwaysExpanded];
    end else
    begin
      Options := [dcoAnsiSort,
        dcoAssignGroupingValues,
        dcoAssignMasterDetailKeys,
        dcoSaveExpanding ];
    end;
  end;
end;

procedure TfrmCustomerPlate.WndProc(var Message: TMessage);
begin
//  SendMessage()
  case Message.Msg of
    WM_LisView : DoAutoCalcValues();
  end;
  // 一定要加上这句，否则编译通不过。因为绝大部分消息没人处理了
  inherited WndProc(Message); // 会一路向上调用，直到TControl.WndProc调用Dispatch来寻找消息处理函数
end;

function TfrmCustomerPlate.SQLTotal( lpSQLText : string):Currency;
begin
  Result := 0;
  with DM.Qry do
  begin
    Close;
    SQL.Text := lpSQLText;
    Open;
    if RecordCount <> 0 then
    begin
      Result := Fields[0].AsCurrency;
    end;
  //  Self.mmo1.Lines.Add('SQLTotal:' + CurrToStr(Result));
  end;
end;

function TfrmCustomerPlate.GroupSQLText(lpFieldName : string; lpOfferType : Byte):string;
var
  lvOfferType : string;
begin
  lvOfferType := 'OfferType';
  Result := 'Select SUM( '+ lpFieldName +' ) AS t from '+ g_Table_Decorate_OfferList +
             ' WHERE tages=' + IntToStr(dwNodeIndex) + ' AND '+ lvOfferType +'=' + IntToStr(lpOfferType);
end;

procedure TfrmCustomerPlate.DoAutoCalcValues();
var
  lvMainMaterialTotal : Currency;
  lvManpowerSummary : Currency;
  lvAfterDismantSummary : Currency;
  t:Integer;
  I:Integer;
  lvItemtext , s : string;
  lvTmpSummary : Currency;
  lvItemCurrent: Double;
  bm:TBookmark;
  lvIndex : Integer;
  lvAcreage : Double;
  RocusdRow : TcxCustomGridRow;
begin
  lvManpowerSummary := 0;
  lvItemtext:='';
  lvItemCurrent:=0;
  lvAfterDismantSummary:=0;
  s := VarToStr(Self.cxDBSpinEdit1.Value);
  lvAcreage := StrToFloatDef(s,0);
  if (dwOfferType = 1) and (dwFormAutoCalc = False) then
  begin
    t:=GetTickCount;
  //  lvMainMaterialTotal   := SQLTotal( GroupSQLText( tvView2Col25.DataBinding.FieldName, 2 ));
    lvManpowerSummary     := SQLTotal( GroupSQLText( tvView2Column2.DataBinding.FieldName,3));

    with tvView2.ViewData,tvView2.Controller,tvView2.DataController do
    begin
      lvIndex := FocusedRowIndex;
      for I := 0 to RowCount-1 do
      begin
        if Rows[i] is TcxGridGroupRow then
        begin
        //  Self.mmo1.Lines.Add('组行:' + IntToStr(i));
        end else
        begin

          if StrToBoolDef( VarToStr(Rows[i].Values[tvAutoCalc.Index]) , False ) then
          begin
            lvItemtext := VarToStr( Rows[i].Values[tvView2Col17.Index] );
            lvItemCurrent := StrToFloatDef( VarToStr( Rows[i].Values[tvView2Col18.Index]   ) , 0 ) ;
            lvAfterDismantSummary := StrToCurrDef( VarToStr( Rows[i].Values[tvView2Col25.Index]  ) , 0 ) ;
            {
            Self.mmo1.Lines.Add(
              'A ItemCurrent'  + lvItemtext +
              ' Calc:' + g_AutoCalcParameter[0] +
              ' lvItemCurrent:' + CurrToStr(lvItemCurrent) +
              ' lvtmpSumary:' + CurrToStr(lvTmpSummary) +
              ' lvAfterDismantSummary:' + CurrToStr(lvAfterDismantSummary)
              );
            }
            if Trim( lvItemtext ) = Trim( g_AutoCalcParameter[0] ) then  // '税金';
            begin
              //税金(人工费 + 直接费 + 费用)
              lvTmpSummary  := ( dwDirectFee + lvManpowerSummary + ( dwExtrasSummary - lvAfterDismantSummary ))  / 100 *  lvItemCurrent; //税金
            //  Self.mmo1.Lines.Add('税金:'+ FloatToStr(dwAcreage) + ' ' + lvItemtext);
            end;

            if Trim( lvItemtext ) = Trim( g_AutoCalcParameter[1] ) then  // '工程直接费的总额';
            begin
              //(不含费用组)
              lvTmpSummary  := ( dwDirectFee + lvManpowerSummary) / 100 *  lvItemCurrent; //直接费总额
            //  Self.mmo1.Lines.Add('直接费总额:' + FloatToStr(dwAcreage) + ' ' + lvItemtext);
            end;

            if Trim( lvItemtext ) = Trim( g_AutoCalcParameter[2] ) then // '装修面积';
            begin
              //(面积*金额)
              lvTmpSummary  := dwAcreage * lvItemCurrent;
            //  Self.mmo1.Lines.Add('面积:' + FloatToStr(dwAcreage) + ' ' + lvItemtext);
            end;
            {
            Self.mmo1.Lines.Add(
              'B ItemCurrent'  + lvItemtext +
              ' Calc:' + g_AutoCalcParameter[0] +
              ' lvItemCurrent:' + CurrToStr(lvItemCurrent) +
              ' lvtmpSumary:' + CurrToStr(lvTmpSummary) +
              ' lvAfterDismantSummary:' + CurrToStr(lvAfterDismantSummary)
              );
            }
            if lvTmpSummary <> lvAfterDismantSummary then
            begin
              FocusedRowIndex := I;
              DataSet.DisableControls;
              Edit;
                DataSet.FieldByName(tvView2Col24.DataBinding.FieldName).Value := lvTmpSummary;
                DataSet.FieldByName(tvView2Col25.DataBinding.FieldName).Value := lvTmpSummary;
              Post();
              DataSet.EnableControls;
           //   Self.mmo1.Lines.Add(lvItemtext + ' - ' + DataSet.FieldByName('ItemCurrent').AsString);
            end;

          end;

        end;

      end;
      if lvIndex <> -1 then
      begin
        FocusedRowIndex := lvIndex;
      //  Self.mmo1.Lines.Add('lvIndex:' + IntToStr(lvIndex));
      end;

    end;
  //  Self.mmo1.Lines.Add('t:' + IntToStr(GetTickCount - t));


    {
  //  lvAfterDismantSummary := StrToCurrDef( VarToStr( tvView2Col25.EditValue ) , 0);
  //  dwDirectFee; //直接费
  //  Self.mmo1.Lines.Add('lvMainMaterialTotal:' + CurrToStr(lvMainMaterialTotal));
  //  Self.mmo1.Lines.Add('lvManpowerSummary:'+CurrToStr(lvManpowerSummary));
  //  Self.mmo1.Lines.Add('dwDirectFee:' + CurrToStr(dwDirectFee));

    lvIndex := Self.tvView2.Controller.FocusedRow.Index;
    Self.tvView2.Controller.FocusedRowIndex := -1;
    Self.mmo1.Lines.Add('Start:' + IntToStr( Self.tvView2.Controller.FocusedRecordIndex ) + ' - ' +
IntToStr( Self.tvView2.Controller.FocusedRow.Index));

    with Self.dsMaster do
    begin
    //  DisableControls;
    //  bm := GetBookmark;
      while not Eof do
      begin
        Application.ProcessMessages;
      //  First;
        if FieldByName('AutoCalc').AsBoolean then
        begin

          lvItemtext := FieldByName('Itemtext').AsString;
          lvItemCurrent := FieldByName('ItemCurrent').AsFloat;
          lvAfterDismantSummary := FieldByName(tvView2Col25.DataBinding.FieldName).AsCurrency;

          //  g_AutoCalcParameter[0]   := '税金';
          //  g_AutoCalcParameter[1] := '工程直接费的总额';
          //  g_AutoCalcParameter[2] := '装修面积';

          if Trim( lvItemtext ) = g_AutoCalcParameter[0] then  // '税金';
          begin
            //税金(人工费 + 直接费 + 费用)
            lvTmpSummary  := ( dwDirectFee + lvManpowerSummary + dwExtrasSummary)  / 100 *  lvItemCurrent; //税金
            Self.mmo1.Lines.Add('税金:' + CurrToStr(lvTmpSummary) );
          end else
          if Trim( lvItemtext ) = g_AutoCalcParameter[1] then  // '工程直接费的总额';
          begin
            //(不含费用组)
            lvTmpSummary  := ( dwDirectFee + lvManpowerSummary) / 100 *  lvItemCurrent; //直接费总额
            Self.mmo1.Lines.Add('直接费:' + CurrToStr(lvTmpSummary) );
          end else
          if Trim( lvItemtext ) = g_AutoCalcParameter[2] then // '装修面积';
          begin
            //(面积*金额)
            lvTmpSummary  := dwAcreage * lvItemCurrent;
            Self.mmo1.Lines.Add('面积:' + CurrToStr(lvTmpSummary) );
          end;

          if lvTmpSummary <> lvAfterDismantSummary then
          begin
            Edit;
            FieldByName(tvView2Col25.DataBinding.FieldName).Value := lvTmpSummary;
            Self.mmo1.Lines.Add(FieldByName('Itemtext').AsString + ' - ' + FieldByName('ItemCurrent').AsString);
          end;

        end;

        Next;
      end;

    //  GotoBookmark(bm);
    //  EnableControls;
    //  FreeBookmark(bm);
    end;

    Self.mmo1.Lines.Add('End:' + IntToStr( Self.tvView2.Controller.FocusedRecordIndex ) + ' - ' +
      IntToStr( Self.tvView2.Controller.FocusedRow.Index));
    }

  end;

end;

procedure TfrmCustomerPlate.InitializeEditors(Sender: TObject);
var
  AValidationOptions: TcxEditValidationOptions;
begin
  AValidationOptions := [];
//  if GetMenuItemChecked(miValidationRaiseException) then
    Include(AValidationOptions, evoRaiseException);
//  if GetMenuItemChecked(miValidationShowErrorIcon) then
    Include(AValidationOptions, evoShowErrorIcon);
//  if GetMenuItemChecked(miValidationAllowLoseFocus) then
    Include(AValidationOptions, evoAllowLoseFocus);

//  tvView2Col1.Properties.ValidationOptions := AValidationOptions;
  tvView2Col25.Properties.ValidationOptions:= AValidationOptions;

  {
  cxGridTableViewColumnFirstName.Properties.ValidationOptions := AValidationOptions;
  cxGridTableViewColumnLastName.Properties.ValidationOptions := AValidationOptions;
  cxGridTableViewColumnAddress.Properties.ValidationOptions := AValidationOptions;
  cxGridTableViewColumnPhoneNumber.Properties.ValidationOptions := AValidationOptions;
  cxGridTableViewColumnEmail.Properties.ValidationOptions := AValidationOptions;
  cxGridTableViewColumn1.Properties.ValidationOptions := AValidationOptions;
  }

end;


procedure TfrmCustomerPlate.SetFieldName(lpIndex : Byte);
var
  s : string;
  lvFieldName : string;
  lvTages : string;
  lvSQLText : string;
  s0: string;
begin
  s := '工程量';
  case lpIndex of
    0: Self.tvView2Col6.Caption := s ;
    1: Self.tvView2Col6.Caption := '实收' + s ;
  end;

  Self.tvView2Col6.DataBinding.FieldName := dwQuantities[lpIndex];
  Self.tvView2Col22.Options.Editing  := lpIndex = 0;
  Self.RzBut3.Enabled := True;
  Self.RzBut4.Enabled := True;
  Self.RzBut5.Enabled := True;
  if dwOfferType = 1 then
  begin
    N31.Enabled := True;
    if dwIsTotal = 0 then
      N27.Enabled := True
    else
      N27.Enabled := False;
  end else begin
    N27.Enabled := False;
    N31.Enabled := False;
  end;

  if dwIsTotal <> 0 then
  begin
    cxLabel40.Visible:= True;
    cxLookupComboBox2.Visible := True;
    RzSpacer4.Visible := True;
    RzToolButton5.Visible := True;
    RzSpacer9.Visible := True;

    N35.Visible := False; //成本核算清单
    N36.Visible := False; //主材核算清单
    N37.Visible := False; //人工核算清单
    N38.Visible := False; //综合报价(无人工组)
    N39.Visible := False; //综合报价(折扣率)
    lvFieldName := 'HeadOfWork';
    if (dwIsTotal = 1) or (dwIsTotal =3 ) then
      s0 := '工长'
    else
      s0 := '供应商';

    cxLabel40.Caption := s0 + '：';
    case dwIsTotal of
      1: N35.Visible := True; //成本核算清单
      2:
      begin
        N36.Visible := True; //主材核算清单
        lvFieldName := 'Supplier';
      end;
      3: N37.Visible := True; //人工核算清单
    end;

    with Self.cxLookupComboBox2.Properties do
    begin
      KeyFieldNames := lvFieldName;
      ListFieldNames:= lvFieldName;
      ListColumns[0].FieldName := lvFieldName;
      ListColumns[0].Caption := s0;
    end;

    tvView2.OptionsView.Footer := True;
  end else
  begin
    cxLabel40.Visible:= False;
    cxLookupComboBox2.Visible := False;
    RzSpacer4.Visible := False;
    RzToolButton5.Visible := False;
    RzSpacer9.Visible := False;

    tvView2.OptionsView.Footer := False;
    //打印菜单
    case dwOfferType of
      0:
      begin
      end;
      1:
      begin
        N1.Visible  := True;
        N2.Visible  := True;
        N3.Visible  := True;
        N32.Visible := True;

        N33.Visible := False; //主材报价
        N35.Visible := False; //成本核算清单
        N36.Visible := False; //主材核算清单
        N37.Visible := False; //人工核算清单
        N38.Visible := True;  //综合报价(无人工组)
        N39.Visible := True; //综合报价(折扣率)
        N49.Visible := False; //主材报价(无折扣)
      end;
      2:
      begin
        N1.Visible  := False;
        N2.Visible  := False;
        N3.Visible  := False;
        N32.Visible := False;

        N33.Visible := True; //主材报价
        N35.Visible := False; //成本核算清单
        N36.Visible := False; //主材核算清单
        N37.Visible := False; //人工核算清单
        N38.Visible := False; //综合报价(无人工组)
        N39.Visible := False; //综合报价(折扣率)
        N49.Visible := True; //主材报价(无折扣)
      end;
      3:
      begin
        N1.Visible  := False;
        N2.Visible  := False;
        N3.Visible  := False;
        N32.Visible := True;

        N33.Visible := False; //主材报价
        N35.Visible := False; //成本核算清单
        N36.Visible := False; //主材核算清单
        N37.Visible := False; //人工核算清单
        N38.Visible := False; //综合报价(无人工组)
        N39.Visible := False; //综合报价(折扣率)
        N49.Visible := False; //主材报价(无折扣)
      end;
    end;


  end;
end;

function TfrmCustomerPlate.GetSumMoney(lpIndex : Integer = 0):Boolean;

  function GetSingleSummary( lpIndex : Integer ):Currency;
  var
     i : Integer;
     lvIndex : Integer;
     s : string;

  begin
    Result := 0;
    {
    with Self.dsMaster do
    begin
      if State <> dsInactive then
      begin

        while not Eof do
        begin
          if (FieldByName('OfferType').AsInteger = lpIndex) and
             (FieldByName(tvGroupName.DataBinding.FieldName).AsString <> dwExpensesName ) then
          begin
            i := FieldByName(tvView2Col25.DataBinding.FieldName).AsCurrency;
            Result := Result + i;
          end;
          Next;
        end;

      end;

    end;
    }

    with Self.tvView2.ViewData do
    begin
      for I := 0 to RowCount-1 do
      begin
        s :=  VarToStr(Rows[i].Values[tvView2Col17.Index]);
        lvIndex := StrToIntDef(s,-1);
        if (lpIndex = lvIndex) and (VarToStr( Rows[i].Values[tvGroupName.Index] ) = dwExpensesName) then
        begin
          s := VarToStr( Rows[i].Values[tvView2Col25.Index] );
          Result := Result + StrToCurrDef(s,0);
        end;
      end;

    end;

  end;

  function GetGroupSummary(ADataGroupIndex: TcxDataGroupIndex;lpSystemIndex : Byte = 2):Variant;
  begin
    //获取合计总小计
    with tvView2.DataController.Summary do
    begin
      Result := GroupSummaryValues[ADataGroupIndex, lpSystemIndex ];
    end;
  end;
  function AGetGroupSummaryInfo(ADataGroupIndex: TcxDataGroupIndex): Boolean;
  var
    ALevel: Integer;
  begin
    with TcxDataSummary(tvView2.DataController.Summary) do
    begin
      ALevel := tvView2.DataController.DataControllerInfo.DataGroups[ADataGroupIndex].Level;
      GroupSummaryItems[ALevel].Items[0].Kind := skNone;
    end;   
  end;

  function ToCurr(lpValue : Variant):Currency;
  begin
    Result := StrToCurrDef( VarToStr( lpValue ) , 0)
  end;
  
var
  AChildDataGroupsCount: Integer;
  AChildDataGroupIndex, AParentDataGroupIndex: TcxDataGroupIndex;
  AChildPosition: Integer;
  i: Integer;
  s , A , B , C , s1: string;
  lvGroupName : string;
  lvSummary : Currency;
  lvMainMaterialTotal : Currency;
  lvManpowerSummary   : Currency;
  lvCostSummary : Currency;
  lvProfit : Currency;
  lvloseAfterProfit : Currency;
  lvAfterDismantSummary: Currency;
  t : Integer;
begin
  {
    TcxDataControllerGroups(tvView2.DataController.Summary.GroupSummaryItems)
    TcxCustomDataSummaryItem = class(TCollectionItem)
  }
  with TcxDataControllerGroups(tvView2.DataController.Groups) do
  begin
    t:= GetTickCount;
    lvSummary := 0;
    s:='';
    lvCostSummary := 0;
    lvManpowerSummary := 0;
    lvMainMaterialTotal := 0;
    lvSummary := 0;
    lvProfit := 0;
    lvloseAfterProfit := 0;
    lvAfterDismantSummary := 0;

    dwSummary           := 0; //合计金额
    dwMainMaterialTotal := 0; //主材合计金额
    dwManpowerSummary   := 0; //人工合计金额
    dwCostSummary       := 0; //成本
    dwProfit            := 0; //利润
    dwloseAfterProfit   := 0;//折后利润
    dwAfterDismantSummary:=0;//折后金额
    dwDirectFee := 0; //直接费
    dwExtrasSummary := 0; //费用
    for i := 1 to GroupingItemCount do
    begin
      AChildDataGroupsCount := ChildCount[i -2];
      for AChildPosition := 0 to AChildDataGroupsCount - 1 do
      begin
        AChildDataGroupIndex := ChildDataGroupIndex[i -2, AChildPosition]; //组Index
        lvGroupName := VarToStr( GroupValues[AChildDataGroupIndex] );
        case lpIndex of
          0:
          begin
            //基本核算

            if lvGroupName <> Trim( dwExpensesName ) then
            begin

              if dwNucleusCount then
              begin
                s := VarToStr( GetGroupSummary(AChildDataGroupIndex , 5) ); //成本合计
              end else
              begin
                case dwOfferType of
                  0:
                  begin
                    lvSummary := lvSummary + StrToCurrDef(VarToStr( GetGroupSummary(AChildDataGroupIndex , 8) ) , 0) ; //折后金额
                  //  lvMainMaterialTotal := lvMainMaterialTotal + StrToCurrDef(VarToStr( GetGroupSummary(AChildDataGroupIndex , 3) ) , 0) ;  //主材合计金额
                    lvManpowerSummary   := lvManpowerSummary + StrToCurrDef(VarToStr( GetGroupSummary(AChildDataGroupIndex , 4) ) , 0); //人工合计金额
                  end;
                  1: s := VarToStr( GetGroupSummary(AChildDataGroupIndex , 8) ); //折后金额
                  2: s := VarToStr( GetGroupSummary(AChildDataGroupIndex , 8) ); //主材合计金额
                  3: s := VarToStr( GetGroupSummary(AChildDataGroupIndex , 4) ); //人工合计金额
                end;
              end;

              if dwOfferType <> 0 then lvSummary := lvSummary +  StrToCurrDef(s , 0);

            end else
            begin
              lvCostSummary   := lvCostSummary + ToCurr( GetGroupSummary(AChildDataGroupIndex , 8) ); //费用
              dwExtrasSummary := lvCostSummary;

              {
              with tvView2.DataController.Summary do
              begin
                GroupSummaryValues[AChildDataGroupIndex , 0] := Null;
                GroupSummaryValues[AChildDataGroupIndex , 1] := Null;
                GroupSummaryValues[AChildDataGroupIndex , 3] := Null;
                GroupSummaryValues[AChildDataGroupIndex , 4] := Null;
              end;
              }

            end;

          end;
          1:
          begin
          //统一核算

          // 总营收
          // 折后总营收
          // 汇总成本
          // 利润
          // 折后利润

          // 利润比
          // 折后利润比

          //小写大写
            dwSummary           := dwSummary + ToCurr( GetGroupSummary(AChildDataGroupIndex , 2) ) ; //合计金额

            dwMainMaterialTotal := dwMainMaterialTotal + ToCurr( GetGroupSummary(AChildDataGroupIndex , 3)  ); //主材合计金额
            dwManpowerSummary   := dwManpowerSummary   + ToCurr( GetGroupSummary(AChildDataGroupIndex , 4) ); //人工合计金额

            dwCostSummary       := dwCostSummary + ToCurr( GetGroupSummary(AChildDataGroupIndex , 5)  ); //成本

            dwProfit            := dwProfit + ToCurr( GetGroupSummary(AChildDataGroupIndex , 6) ); //利润
            dwloseAfterProfit   := dwloseAfterProfit + ToCurr( GetGroupSummary(AChildDataGroupIndex , 7) );//折后利润
            dwAfterDismantSummary:=dwAfterDismantSummary + ToCurr( GetGroupSummary(AChildDataGroupIndex , 8)  );//折后金额

          end;

        end;

      end;

    end;
    //整体打折不算费用组
    if lpIndex = 0 then
    begin

      if dwNucleusCount then
      begin
        s := '成本汇总:' ;
      //  Self.tvView2.Bands[2].Caption := '成本汇总:' +  Format('%2.2m',[lvSummary]) + ' 大写(' + MoneyConvert(lvSummary) + ')' ;
      end else
      begin
        s := '汇总合计:' ;
        if dwOfferType = 0 then
        begin
          if lvSummary <> 0 then
          begin
            dwSingleSummary := GetSingleSummary(1);
            lvSummary := (lvSummary - dwSingleSummary) + ( dwSingleSummary / 100 * dwWholeListdiscount );
            lvSummary := lvSummary + lvManpowerSummary + lvCostSummary;
          //lvMainMaterialTotal +
          //  Self.tvView2.Bands[2].Caption := '汇总合计:' + Format('%2.2m',[ lvSummary ]) + '　大写(' + MoneyConvert(lvSummary) + ')';
          end;
        end else
        begin
          if dwOfferType = 1 then
          begin
            lvSummary   := (lvSummary / 100 * dwWholeListdiscount );
            dwDirectFee := lvSummary;
            lvManpowerSummary     := SQLTotal( GroupSQLText( tvView2Column2.DataBinding.FieldName,3));
            Self.tvView2.Bands[2].Caption := Format('%s%2.2m 大写(%s)',
            [s,lvSummary + lvCostSummary + lvManpowerSummary ,
               MoneyConvert(lvSummary + lvCostSummary + lvManpowerSummary )]) ; // + ' 大写(' + MoneyConvert(lvSummary) + ')' ;

            Exit;
          end;
          
        end;

      end;
      Self.tvView2.Bands[2].Caption := Format('%s%2.2m 大写(%s)',[s, lvSummary , MoneyConvert(lvSummary) ]);
    end else
    begin

      {
      Self.mmo1.Lines.Add( '主材合计金额:' + CurrToStr( lvMainMaterialTotal ) ) ; //主材合计金额
      Self.mmo1.Lines.Add( CurrToStr( lvManpowerSummary ) )  ; //人工合计金额
      } {
      Self.mmo1.Lines.Add('合计金额:' + CurrToStr( lvSummary ) ); //合计金额
      Self.mmo1.Lines.Add('成本合计'  + CurrToStr( lvCostSummary ) ) ; //成本
      Self.mmo1.Lines.Add('利润:'     + CurrToStr( lvProfit ) ) ; //利润
      Self.mmo1.Lines.Add('折后利润:' + CurrToStr( lvloseAfterProfit ) );//折后利润
      Self.mmo1.Lines.Add('折后金额:' + CurrToStr( lvAfterDismantSummary ) );//折后金额
      }
    end;
  end;

end;


{
var
  lvGroupName : string;
  I: Integer;
  s: string;
  c: Currency;
  t: Integer;

begin
  t := GetTickCount;
  with Self.tvView2 do
  begin
    for I := 0 to ViewData.RowCount-1 do
    begin
      lvGroupName := VarToStr( ViewData.Rows[i].Values[tvGroupName.Index] );
      s := VarToStr( ViewData.Rows[i].Values[tvView2Col24.Index] );
    //  Self.mmo1.Lines.Add(VarToStr( ViewData.Rows[i].Values[Self.tvProjectName.Index] ) + ' s:' + s);
      if ( Length(s) <> 0) and (lvGroupName <> '费用') then
      begin
        c := StrToCurrDef(s,0) + c;
      end;
    end;
    Self.tvView2.Bands[2].Caption := '直接费:' +  Format('%2.2m',[c]) + ' - ' + IntToStr(GetTickCount- t);
  end;
end;
}

procedure TfrmCustomerPlate.ReadData(lpNodeId : Integer);

  function IsDataExist(DataSet : TClientDataSet ; lpFieldName , lpinSpectName : string) : Boolean;
  var
    I: Integer;
  begin
    Result := False;
    with Self.tvView2.ViewData do
    begin
      for I := 0 to RowCount-1 do
      begin
        if VarToStr( Rows[i].Values[tvGroupName.Index] ) = lpinSpectName then
        begin
          Result := True;
          Break;
        end;
      end;
    end;

    {
    With DataSet do
    begin
      First;
      if RecordCount <> 0 then
      begin
        while not Eof do
        begin
          Result := False;
          if FieldByName(  lpFieldName ).AsString = lpinSpectName then
          begin
            Result := True;
            Break;
          end;
          Next;
        end;
      end;
    end;

    }
  end;

  function DeleteData(DataSet : TClientDataSet ; lpFieldName , lpName : string) : Boolean;
  begin
    With DataSet do
    begin
      First;
      while Not Eof do
      begin
        if lpName = FieldByName(lpFieldName).AsString then
        begin
          Delete;
          continue;
        end;
        Next;
      end;
    end;
  end;
  //检查费用数据是否存在
  function InSpectData():Boolean;
  var
    i : Integer;
    s: string;

    lvFieldName : string;

    lvIsExist : Boolean;
    lvCostList : array[0..2] of TCostList;
  begin

    lvCostList[0].dwProjectName := '税金';
    lvCostList[0].dwAutoCalc    := True;
    lvCostList[0].dwItemtext    := g_AutoCalcParameter[0]; //税金
    lvCostList[0].dwItemCurrent := 3;

    lvCostList[1].dwProjectName := '管理费';
    lvCostList[1].dwAutoCalc    := True;
    lvCostList[1].dwItemtext    := g_AutoCalcParameter[2]; //装修面积  //工程直接费的总额
    lvCostList[1].dwItemCurrent := 20;

    lvCostList[2].dwProjectName := '设计费';
    lvCostList[2].dwAutoCalc    := True;
    lvCostList[2].dwItemtext    := g_AutoCalcParameter[2]; //装修面积
    lvCostList[2].dwItemCurrent := 20;

    {
    g_AutoCalcParameter[0] := '税金';
    g_AutoCalcParameter[1] := '工程直接费的总额';
    g_AutoCalcParameter[2] := '装修面积';
    }

    {
      type
      TCostList = record
        dwProjectName : string;
        dwAutoCalc    : Boolean;
        dwItemtext    : string;
        dwItemCurrent : Currency;
      end;
    }

    {
    lvCostList[0] := '管理费';
    lvCostList[1] := '设计费';
    lvCostList[2] := '水电预收费';
    lvCostList[3] := '灯具安装费';
    lvCostList[4] := '其他费用';
    lvCostList[5] := '垃圾清运费';
    lvCostList[6] := '成品保护费';
    lvCostList[7] := '税金';
    }
  //  dwWholeListdiscount := StrToIntDef(VarToStr( Self.cxSpinEdit1.Value ) ,0 ) ;

    Self.tv.Enabled := False;
    Self.Grid1.Enabled := False;
    Self.tv1.Enabled:= False;
    Self.Grid3.Enabled := False;
    Self.Grid2.Enabled := False;
    IsAlwaysExpanded(True);
    with Self.dsMaster do
    begin
      if State <> dsInactive then
      begin
        lvFieldName := Self.tvGroupName.DataBinding.FieldName;
      //  DisableConstraints;
        if (dwOfferType = 2) or (dwOfferType = 3)  then //主材报价
        begin

          case dwOfferType of
            2: s := '主材';
            3: s := '人工';
          end;

          if not IsDataExist(Self.dsMaster,lvFieldName,s) then
          begin
            DisableControls;
            Insert;
            FieldByName(lvFieldName).Value := s;
            Post;
            EnableControls;
          end;

          if IsDataExist(Self.dsMaster,lvFieldName,dwExpensesName) then
             DeleteData(Self.dsMaster,lvFieldName,dwExpensesName);

        end else
        begin
           if dwOfferType = 1 then
           begin

              if not IsDataExist(Self.dsMaster,lvFieldName,dwExpensesName) then
              begin
                lvIsExist := False;
                for I := 0 to 2 do
                begin
                  Application.ProcessMessages;
                  DisableControls;
                  Append;
                  lvIsExist := True;
                  FieldByName(lvFieldName).Value := dwExpensesName;
                  FieldByName(tvSpaceName.DataBinding.FieldName).Value  := dwExpensesName;
                  FieldByName(tvProjectName.DataBinding.FieldName).Value:= lvCostList[i].dwProjectName;
                  FieldByName(tvAutoCalc.DataBinding.FieldName).Value   := lvCostList[i].dwAutoCalc;
                  FieldByName(tvView2Col17.DataBinding.FieldName).Value := lvCostList[i].dwItemtext;
                  FieldByName(tvView2Col18.DataBinding.FieldName).Value := lvCostList[i].dwItemCurrent;

                  FieldByName(tvView2Col5.DataBinding.FieldName).Value  := '项';
                  FieldByName(tvView2Col6.DataBinding.FieldName).Value  := 1;
                //  Post;
                  EnableControls;
                end;

                if lvIsExist then  Post;

              end;

           end;

        end;   
      //  EnableConstraints;
      end;

    end;
    IsAlwaysExpanded(False);
    Self.tv.Enabled := True;
    Self.Grid1.Enabled := True;
    Self.tv1.Enabled:= True;
    Self.Grid3.Enabled := True;
    Self.Grid2.Enabled := True;
  end;

var
  lvSQLText : string;
  lvTages : string;
  lvFieldName : string;

begin
  lvTages := 'tages=' + IntToStr(lpNodeId);
  {
  if dwOfferType = 0 then
  begin
    lvSQLText := 'Select * from '+ g_Table_Decorate_OfferList +
               ' WHERE ' + lvTages;
  end else
  begin
  end;
  }
  lvSQLText := 'Select * from '+ g_Table_Decorate_OfferList +
               ' WHERE ' + lvTages + ' AND OfferType=' + IntToStr(dwOfferType); // + ' order by iRec'


  dwADO.OpenSQL(Self.dsMaster,lvSQLText);


  lvFieldName := Self.cxLookupComboBox2.Properties.KeyFieldNames;
  lvTages := 'tages=' + IntToStr(dwNodeIndex);
  lvSQLText := 'Select '+ lvFieldName +' from '+ g_Table_Decorate_OfferList +
               ' WHERE ' + lvTages + ' AND OfferType=' + IntToStr(dwOfferType) + ' AND ' + lvFieldName + '<> "" group by ' + lvFieldName; // + ' order by iRec'
  dwADO.OpenSQL(dsSearch,lvSQLText);

  InSpectData;
  N15.Click;
  GetGroupName(True); //整理分组
//  Self.tmrClock.Enabled := True;
end;

procedure TfrmCustomerPlate.UpDateIndex(lpNode : TTreeNode);
var
  s : string;
begin
  S := 'UPDATE ' + g_Table_Decorate_OfferBranchTree +
  ' SET TreeIndex=''' + IntToStr(lpNode.Index) + ''', TreeId=''' + IntToStr(dwParentNodeId) + ''' WHERE SignName="' + lpNode.Text + '"';
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := s;
    ExecSQL;
  end;
end;

procedure TfrmCustomerPlate.TreeAddName(Tv : TTreeView); //树增加名称
var
  Node , ChildNode : TTreeNode;
  lvCode : string;
  lvSQLText : string;

begin
  with Tv do
  begin
    Node := Selected;
    if Assigned(Node) then
    begin
      if Node.Level = 0  then
      begin
         MessageBox(handle, PChar('不可以在顶级节点新建?'),'提示',
               MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2)
      end else
      begin
        lvCode := GetRowCode( g_Table_Decorate_OfferTree ,'Code','901007',170000);
        dwTreeView.AddChildNode(lvCode,'',Date,m_OutText);
        lvSQLText := 'UPDATE ' + g_Table_Decorate_OfferBranchTree +' SET TreeId=''' + IntToStr(dwParentNodeId) + ''' WHERE Code="' + lvCode + '"';
        with DM.Qry do
        begin
          Close;
          SQL.Text := lvSQLText;
          ExecSQL;
        end;
      end;
    end;
  end;

end;

function TfrmCustomerPlate.IstreeEdit(S : string ; Node: TTreeNode):Boolean;
var
  szCodeList : string;
  PrevNode , NextNode : TTreeNode;

begin
  if s = m_OutText then
  begin
    treeCldnode(Node,szCodeList);
    dwTreeView.DeleteTree(Node);
  end else
  begin
    dwTreeView.ModifyNodeSignName(s,Node);
    UpDateIndex(Node);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    if PrevNode.Index <> -1 then UpDateIndex(PrevNode);
    NextNode   := Tv.Selected.getNextSibling;//下
    if NextNode.Index <> -1 then UpDateIndex(NextNode);
  end;
end;

function TfrmCustomerPlate.GridBanbsLocateRecord(View : TcxGridDBBandedTableView; FieldName, LocateValue : String) : Boolean;
begin
  {表格数据查找定位}
  Result := False;
  if (View.GetColumnByFieldName(FieldName) <> nil) then
  begin
    Result := View.DataController.Search.Locate(View.GetColumnByFieldName(FieldName).Index, LocateValue);
  end;
end;

procedure TfrmCustomerPlate.N15Click(Sender: TObject);
begin
  Self.tvView2.DataController.Groups.FullExpand;
end;

procedure TfrmCustomerPlate.N16Click(Sender: TObject);
begin
  Self.tvView2.DataController.Groups.FullCollapse;
end;

procedure TfrmCustomerPlate.N18Click(Sender: TObject);
var
  lvFieldName : string;
  lvRowIndex : Integer;
  I: Integer;
  vGroupname : Variant;
  lvGroupName : Variant;
  lvSpaceName : Variant;
  lvProjectName : Variant;
  lvSupplier  : Variant;
  lvBrand : Variant;
  lvSpec : Variant;
  lvProfession : Variant;
  lvHeadOfWork : Variant;
  lvCompany : Variant;
  lvQuantities:Variant;
  lvMainMaterialPrice : Variant;
  lvArtificialPrice :Variant;
  lvMaterialPrice : Variant;
  lvCostTotal : Variant;
  lvArtifi : Variant;
  lvMainMaterial : Variant;
  lvAuxil : Variant;
  lvloss : Variant;
  lvSyntheticalPrice : Variant;
  lvDismantRate: Variant;
  lvAfterDismantRatePrice : Variant;
  lvSumMoney : Variant;
  lvAfterDismantSummary:Variant;
  lvMainMaterialTotal:Variant;
  lvManpowerSummary:Variant;
  lvMaterialSummary:Variant;
  lvLaborCost:Variant;
  lvCostSummary:Variant;
  lvProfit : Variant;
  lvloseAfterProfit:Variant;
  lvTechnics : Variant;
  lvAutoCalc : Variant;
  lvColIndex : Integer;
  lvCellValues:Variant;
  j : Integer;
  lvInsertGroupName : Variant;
  lvGroupIndex : Integer;
  lvFieldIndex : Integer;
  lvTopGroupName : Variant;
  s : string;
  lvDictionList : TObjectDictionary<string,Variant>;
  lvFieldList : TViewList;
  lvGroupFieldName : string;
begin
  inherited;
  lvGroupFieldName := tvGroupName.DataBinding.FieldName;
  lvDictionList := TObjectDictionary<string,Variant>.Create();
  try
    lvDictionList.Clear;
    vGroupname := tvGroupName.EditValue;
    with dsMaster do
    begin
      for I := 0 to Fields.Count-1 do
      begin
        lvFieldName := Fields[i].FieldName;
        lvCellValues:= Fields[i].Value;
        if ( lvFieldName <> 'Id') and (lvFieldName <> 'NoId') and (lvFieldName <> lvGroupFieldName) then
        begin
          if not lvDictionList.ContainsKey(lvFieldName) then
          begin
            lvDictionList.Add(lvFieldName,lvCellValues);
          //  mmo1.Lines.Add(lvFieldName + ' ' + VarToStr(lvCellValues));
          end;
        end;
      end;
    end;

    with tvView2,ViewData , tvView2.DataController do
    begin
      {
      lvGroupIndex := tvGroupName.Index;
      for I := 0 to Controller.SelectedRecordCount -1 do
      begin
        vGroupname  := Controller.SelectedRecords[i].Values[ lvGroupIndex ]; //当前选中组
        for lvColIndex := 0 to ColumnCount-1 do
        begin
          //取列
          lvFieldName := Columns[ lvColIndex ].DataBinding.FieldName;
          lvFieldIndex:= Columns[ lvColIndex ].Index;
          lvCellValues:= Controller.SelectedRecords[i].Values[ lvFieldIndex ];
          if (lvFieldName <> '') and (tvGroupName.DataBinding.FieldName <> lvFieldName) then  //and (lvFieldName <> 'OfferType')
          begin
            if not lvDictionList.ContainsKey(lvFieldName) then
            begin
              lvDictionList.Add(lvFieldName,lvCellValues);
            end;
          end;

        end;

      end;
      }
      //***********
      {
      for i := 0 to RowCount-1 do
      begin
        //全部行
        Application.ProcessMessages;

        lvInsertGroupName := Rows[i].Values[ lvGroupIndex ];
        if ( VarToStr( lvInsertGroupName) <> VarToStr( vGroupname ) ) and
           ( VarToStr( lvInsertGroupName) <> lvTopGroupName ) and
           ( VarToStr( lvInsertGroupName) <> dwExpensesName)  then
        begin
          lvTopGroupName := lvInsertGroupName;
          FocusedRecordIndex := i;
          dsMaster.Insert;
          dsMaster.DisableControls;
          dsMaster.FieldByName(tvGroupName.DataBinding.FieldName).Value := lvInsertGroupName;
          if lvDictionList.Count <> 0 then
          begin
            for s in lvDictionList.Keys do
            begin

              if lvDictionList.ContainsKey(s) then
              begin
                lvCellValues := lvDictionList.Items[s];

                if ( VarToStr( lvCellValues ) <> '') and (VarToStr(lvCellValues) <> '0') then
                begin
                  if (s <> '')  then
                  begin
                    dsMaster.FieldByName(s).Value := lvCellValues;
                    Application.ProcessMessages;
                  end;
                end;

              end;

            end;

          end;
          dsMaster.EnableControls;

          DataSet.DisableControls;
          DataSet.Insert;
          DataSet.FieldByName(tvGroupName.DataBinding.FieldName).Value := lvInsertGroupName;
          if lvDictionList.Count <> 0 then
          begin
            for s in lvDictionList.Keys do
            begin

              if lvDictionList.ContainsKey(s) then
              begin
                lvCellValues := lvDictionList.Items[s];

                if ( VarToStr( lvCellValues ) <> '') and (VarToStr(lvCellValues) <> '0') then
                begin
                  if (s <> '')  then
                  begin
                    DataSet.FieldByName(s).Value := lvCellValues;
                    Application.ProcessMessages;
                  end;
                end;

              end;

            end;

          end;
          DataSet.Post;
          DataSet.EnableControls;

        end;
        Groups.FullExpand;
      end;
      }
    end;
    IsAlwaysExpanded(True);
    try
      with dxGroupName do
      begin
        First;
        while not Eof do
        begin
          lvInsertGroupName := FieldByName(lvGroupFieldName).Value ;
        //  mmo1.Lines.Add(lvInsertGroupName + ' ---- ' + IntToStr( Length( lvInsertGroupName ) ) + ' - ' + IntToStr( Length(vGroupname) ));
          if ( VarToStr( lvInsertGroupName) <> VarToStr( vGroupname ) ) and
             ( VarToStr( lvInsertGroupName) <> dwExpensesName)  then
          begin
            dsMaster.DisableControls;
            dsMaster.Insert;
            dwGroupName := VarToStr( lvInsertGroupName );
            dsMaster.FieldByName(lvGroupFieldName).Value := lvInsertGroupName;
          //  mmo1.Lines.Add(lvInsertGroupName + ' ++++ Start i:' + IntToStr(Self.tvView2.Controller.FocusedRowIndex));
            for s in lvDictionList.Keys do
            begin

              if lvDictionList.ContainsKey(s) then
              begin
                lvCellValues := lvDictionList.Items[s];

                if ( VarToStr( lvCellValues ) <> '') and (VarToStr(lvCellValues) <> '0') then
                begin
                  if (s <> '')  then
                  begin
                  //  mmo1.Lines.Add(lvInsertGroupName + ' ++++ S = ' + s);
                    dsMaster.FieldByName(s).Value := lvCellValues;
                  //  Application.ProcessMessages;
                  end;
                end;

              end;

            end;

            if ( dsMaster.State = dsInsert ) or (dsMaster.State = dsEdit) then
            begin
            //  mmo1.Lines.Add(lvInsertGroupName + ' ++++ Post = ' + s);
              dsMaster.Post;
            end;
          //  tvView2.DataController.Groups.FullExpand;
            dsMaster.EnableControls;
          end;

          Next;
        end;
      end;
    finally
      IsAlwaysExpanded(False);
    end;

    {
    with Self.dsMaster do
    begin
      if State <> dsInactive then
      begin
        First;
        while not Eof do
        begin
          lvInsertGroupName := FieldByName(tvGroupName.DataBinding.FieldName).AsString ;

          if ( VarToStr( lvInsertGroupName) <> VarToStr( vGroupname ) ) then
          begin
            Self.mmo1.Lines.Add( lvInsertGroupName + ' [] ' +
             vGroupname + ' [] ' + lvTopGroupName + ' [] ' + dwExpensesName);
          end;
          

          if ( VarToStr( lvInsertGroupName) <> VarToStr( vGroupname ) ) and
             ( VarToStr( lvInsertGroupName) <> lvTopGroupName ) and
             ( VarToStr( lvInsertGroupName) <> dwExpensesName)  then
          begin
            Self.mmo1.Lines.Add( lvInsertGroupName + '  []' + vGroupname + ' [-]' + lvTopGroupName + '  [-] ' + dwExpensesName);

            lvTopGroupName := lvInsertGroupName;
            Insert;
            FieldByName(tvGroupName.DataBinding.FieldName).Value := lvInsertGroupName;
            for s in lvDictionList.Keys do
            begin

              if lvDictionList.ContainsKey(s) then
              begin
                lvCellValues := lvDictionList.Items[s];

                if ( VarToStr( lvCellValues ) <> '') and (VarToStr(lvCellValues) <> '0') then
                begin
                  if (s <> '')  then
                  begin
                    FieldByName(s).Value := lvCellValues;
                    Application.ProcessMessages;
                  end;
                end;

              end;

            end;
            Post;
            tvView2.DataController.Groups.FullExpand;
          end;

          Next;
        end;
      end;
    end;
    }
    //***********************全部汇总***************************************
    {
    if lvDictionList.Count <> 0 then
    begin
      for s in lvDictionList.Keys do
      begin
        if lvDictionList.ContainsKey(s) then
        begin
          Self.mmo1.Lines.Add(s + ' Values:' + VarToStr( lvDictionList.Items[s] ));
        end;
      end;
    end;
    }
  //  for t := 0 to 17 do lvDictionList.Add(ProjectName[t],0);
  finally
    lvDictionList.Free;
  end;
  Exit;
  {
      with tvView2,ViewData , tvView2.DataController do
      begin
        lvGroupIndex := tvGroupName.Index;
        for I := 0 to Controller.SelectedRecordCount -1 do
        begin
          vGroupname  := Controller.SelectedRecords[i].Values[ lvGroupIndex ]; //当前选中组

          for j := 0 to RowCount-1 do
          begin
            //全部行
            lvInsertGroupName := Rows[j].Values[ lvGroupIndex ];
            if ( VarToStr( lvInsertGroupName) <> VarToStr( vGroupname ) ) and
               ( VarToStr( lvInsertGroupName) <> lvTopGroupName ) and
               ( VarToStr( lvInsertGroupName) <> dwExpensesName)  then
            begin

              lvTopGroupName := lvInsertGroupName;
              DataSet.Insert;
              DataSet.FieldByName(tvGroupName.DataBinding.FieldName).Value := lvInsertGroupName;
              Groups.FullExpand;

              Application.ProcessMessages;


              for lvColIndex := 1 to ColumnCount-1 do
              begin
                //取列
              //  Self.mmo1.Lines.Add(IntToStr(i) + '----------' + IntToStr( lvColIndex ) + ':' + IntToStr( Controller.SelectedRecordCount ) );


                lvFieldName := Columns[ lvColIndex ].DataBinding.FieldName;
                lvFieldIndex:= Columns[ lvColIndex ].Index;

              //  lvCellValues:= Controller.SelectedRecords[i].Values[ lvFieldIndex ];
                {
                if lvFieldName <> '' then
                begin
                  DataSet.FieldByName(lvFieldName).Value := lvCellValues;
                end;

                Self.mmo1.Lines.Add('Values:' + VarToStr( lvCellValues ) +
                 ' FieldName:' + lvFieldName + ' lvFieldIndex:' + IntToStr(lvFieldIndex)+
                 ' lvColIndex:'+ IntToStr(lvColIndex));
              end;


            end;

          end;
          if (DataSet.State = dsInsert) or (DataSet.State = dsEdit)  then Post();

        end;

      end;

  Exit;
 }
  lvRowIndex    := Self.tvView2.Controller.FocusedRowIndex;
  lvGroupName   := Self.tvGroupName.EditValue;
  lvSpaceName   := Self.tvSpaceName.EditValue;
  lvProjectName := Self.tvProjectName.EditValue;
  lvSupplier    := self.tvView2Col1.EditValue; //供应商
  lvSpec        := Self.tvView2Col7.EditValue;  //规格
  lvBrand       := Self.tvView2Col2.EditValue;  //品牌
  lvProfession  := Self.tvView2Col3.EditValue; //工种
  lvHeadOfWork  := Self.tvView2Col4.EditValue; //工长
  lvCompany     := Self.tvView2Col5.EditValue; //单位
  lvQuantities  := Self.tvView2Col6.EditValue; //工程量
  lvMainMaterialPrice:= Self.tvView2Col8.EditValue ;//主材报价
  lvArtificialPrice  := Self.tvView2Col9.EditValue;//人工单价
  lvMaterialPrice    := Self.tvView2Col10.EditValue; //材料成本
  lvCostTotal        := Self.tvView2Col11.EditValue; //成本合计
  lvArtifi           := Self.tvView2Col12.EditValue;//人工
  lvMainMaterial     := Self.tvView2Col13.EditValue;//材料
  lvAuxil            := Self.tvView2Col14.EditValue; //辅料
  lvloss             := Self.tvView2Col15.EditValue; //损耗
  lvSyntheticalPrice := Self.tvView2Col16.EditValue; //综合单价
  lvDismantRate      := Self.tvView2Col22.EditValue; //折扣率
  lvAfterDismantRatePrice := Self.tvView2Col23.EditValue; //折后单价
  lvSumMoney := tvView2Col24.EditValue; //合计金额
  lvAfterDismantSummary := tvView2Col25.EditValue;//折后金额
  lvMainMaterialTotal:=tvView2Col26.EditValue;//主材合计
  lvMaterialSummary:= tvView2Col27.EditValue;//材料合计
  lvLaborCost:=tvView2Col28.EditValue;//人工合计
  lvCostSummary:= tvView2col29.EditValue;//成本合计
  lvProfit := tvView2Col30.EditValue;//利润
  lvloseAfterProfit:= tvView2Col31.EditValue;//折后利润
  lvTechnics := tvView2Col32.EditValue;//工艺
  lvAutoCalc := tvAutoCalc.EditValue;//自动计算

  with Self.tvView2.DataController.DataSet do
  begin
    if State <> dsInactive then
    begin
      First;
      while not Eof do
      begin
        vGroupname := FieldByName(tvGroupName.DataBinding.FieldName).Value ;
        ShowMessage(VarToStr(vGroupname) + ' - ' + VarToStr(lvGroupName));

        if VarToStr( vGroupname ) <> lvGroupName then
        begin
          Insert;

          FieldByName(tvGroupName.DataBinding.FieldName).Value   := vGroupname;
          FieldByName(tvSpaceName.DataBinding.FieldName).Value   := lvSpaceName;
          FieldByName(tvProjectName.DataBinding.FieldName).Value := lvProjectName;
          FieldByName(tvView2Col1.DataBinding.FieldName).Value   := lvSupplier;
          FieldByName(tvView2Col2.DataBinding.FieldName).Value   := lvBrand;
          FieldByName(tvView2Col3.DataBinding.FieldName).Value   := lvProfession;
          FieldByName(tvView2Col4.DataBinding.FieldName).Value   := lvHeadOfWork;
          FieldByName(tvView2Col5.DataBinding.FieldName).Value   := lvCompany;
          FieldByName(tvView2Col6.DataBinding.FieldName).Value   := lvQuantities;
          FieldByName(tvView2Col7.DataBinding.FieldName).Value   := lvSpec;
          FieldByName(tvView2Col8.DataBinding.FieldName).Value   := lvMainMaterialPrice;
          FieldByName(tvView2Col9.DataBinding.FieldName).Value   := lvArtificialPrice;
          FieldByName(tvView2Col10.DataBinding.FieldName).Value  := lvMaterialPrice;
          FieldByName(tvView2Col11.DataBinding.FieldName).Value  := lvCostTotal;
          FieldByName(tvView2Col12.DataBinding.FieldName).Value  := lvArtifi;
          FieldByName(tvView2Col13.DataBinding.FieldName).Value  := lvMainMaterial;
          FieldByName(tvView2Col14.DataBinding.FieldName).Value  := lvAuxil;
          FieldByName(tvView2Col15.DataBinding.FieldName).Value  := lvloss;
          FieldByName(tvview2col16.DataBinding.FieldName).Value  := lvSyntheticalPrice;
          FieldByName(tvView2Col22.DataBinding.FieldName).Value  := lvDismantRate;
          FieldByName(tvView2Col23.DataBinding.FieldName).Value  := lvAfterDismantRatePrice;
          FieldByName(tvView2Col24.DataBinding.FieldName).Value  := lvSumMoney;
          FieldByName(tvView2Col25.DataBinding.FieldName).Value  := lvAfterDismantSummary;
          FieldByName(tvView2Col26.DataBinding.FieldName).Value  := lvMainMaterialTotal;
          FieldByName(tvView2Col27.DataBinding.FieldName).Value  := lvMaterialSummary;
          FieldByName(tvView2Col28.DataBinding.FieldName).Value  := lvLaborCost;
          FieldByName(tvView2Col29.DataBinding.FieldName).Value  := lvCostSummary;
          FieldByName(tvView2Col30.DataBinding.FieldName).Value  := lvProfit;
          FieldByName(tvView2Col31.DataBinding.FieldName).Value  := lvloseAfterProfit;
          FieldByName(tvView2Col32.DataBinding.FieldName).Value  := lvTechnics;
          FieldByName(tvAutoCalc.DataBinding.FieldName).Value    := lvAutoCalc;
          Post;
        end;
        Next;
      end;
    end;
  end;
end;

procedure TfrmCustomerPlate.N1Click(Sender: TObject);
var
  lvTMPSQLText : string;
  lvSQLText : string;
  lvOfferType : string;
  lvFieldName : string;

begin
  inherited;
  //报表的名称：销售报价单、销货出库单
  //主材的
  //综合报价
  lvFieldName := tvGroupName.DataBinding.FieldName;
  lvTMPSQLText:= 'Select * from '+ g_Table_Decorate_OfferList +' WHERE tages=' + IntToStr(dwNodeIndex);
  lvOfferType := 'OfferType';
  lvSQLText := lvTMPSQLText + ' AND ' + lvOfferType +'=' + IntToStr(3);

  with Self.dsReportArtert do
  begin    //取人工报价
    Close;
    SQL.Text := lvSQLText;
    Open;
    Sort := lvFieldName + ' ASC,' + tvSpaceName.DataBinding.FieldName + ' ASC';
  end;
  lvSQLText := lvTMPSQLText + ' AND ' + lvOfferType +'=' + IntToStr(1) +
                              ' AND ' + lvFieldName + ' <> "' + dwExpensesName + '"';

  with Self.qry1 do
  begin
    Close;
    SQL.Text := lvSQLText;
    Open;
    Sort := lvFieldName + ' ASC,' + tvSpaceName.DataBinding.FieldName + ' ASC';
  end;
  lvSQLText := lvTMPSQLText + ' AND ' + lvOfferType +'=' + IntToStr(1) +
                              ' AND ' + lvFieldName + ' = "' + dwExpensesName + '"';

  with Self.dsReportCost do
  begin
    Close;
    SQL.Text := lvSQLText;
    Open;
    Sort := lvFieldName + ' ASC,' + tvSpaceName.DataBinding.FieldName + ' ASC';
  end;
  //'报价预算表'
  RePinert( Sender , cxDBComboBox4.Text,0);
//  报价决算表
end;

procedure TfrmCustomerPlate.N20Click(Sender: TObject);
var
  lvFieldName : string;
begin
  inherited;
//删除空行
//  dwADO.DeleteRowEmpty(Detailed,tViewCol4.DataBinding.FieldName);
  lvFieldName := Self.tvSpaceName.DataBinding.FieldName;
  with Self.dsMaster do
  begin
    First;
    while Not Eof do
    begin
      if null = FieldByName(lvFieldName).Value then
      begin
        Delete;
        continue;
      end;
      Next;
    end;
  end;
end;

procedure TfrmCustomerPlate.N21Click(Sender: TObject);
var
  PrevNode , NextNode , SourceNode , TargetNode : TTreeNode;
begin
  inherited;
  PrevNode   := Tv.Selected.getPrevSibling;
  NextNode   := Tv.Selected.getNextSibling;
  SourceNode := Tv.Selected;
  if (PrevNode.Index <> -1) then
  begin
    if NextNode <> nil then
      PrevNode.MoveTo(NextNode, naInsert)
    else
      PrevNode.MoveTo(SourceNode, naAdd);
  end;

  SourceNode := Tv.Selected; //中
  UpDateIndex(SourceNode);
//  PrevNode   := Tv.Selected.getPrevSibling;//上
//  UpDateIndex(PrevNode);
  NextNode   := Tv.Selected.getNextSibling;//下
  UpDateIndex(NextNode);
end;

procedure TfrmCustomerPlate.N22Click(Sender: TObject);
var
  OldNode , Node , SourceNode ,PrevNode: TTreeNode;
begin
  inherited;
  Node := Self.Tv.Selected;
  OldNode := Node.getNextSibling;
  if OldNode.Index <> -1 then
  begin
  //  ShowMessage('Index:' + IntToStr(OldNode.Index));
    OldNode.MoveTo(Node, naInsert);

    SourceNode := Tv.Selected; //中
    UpDateIndex(SourceNode);
    PrevNode   := Tv.Selected.getPrevSibling;//上
    UpDateIndex(PrevNode);
  //  NextNode   := Tv.Selected.getNextSibling;//下
  //  UpDateIndex(NextNode);
  end;
end;

procedure TfrmCustomerPlate.N23Click(Sender: TObject);
var
  Child : TfrmCustomerProject;
begin
  inherited;
  Child := TfrmCustomerProject.Create(nil);
  try
    Child.ShowModal;
  finally
    Child.Free;
  end;
end;

procedure TfrmCustomerPlate.N24Click(Sender: TObject);
var
  i: Integer;
begin
  inherited;
  dwOfferType := 0;
  dwIsTotal := 0;
  for i := 0 to Self.tvView2.Bands.Count-1 do
    Self.tvView2.Bands[i].Visible := True;

  dwNucleusCount := False;
  tvDblClick(Sender);
  Self.RzBut3.Enabled := False;
  Self.RzBut4.Enabled := False;
  Self.RzBut5.Enabled := False;
  Self.RzBut.Visible := False ;
end;

procedure TfrmCustomerPlate.N25Click(Sender: TObject);
begin
  inherited;
  //数据只显示人工报价小组
  dwOfferType := 3;
  dwIsTotal := 0;
  SetFieldName(0);
  Self.RzBut.Visible := False ;
  Self.tvView2.Bands[1].Visible := True;  //项目名称
  Self.tvView2.Bands[3].Visible := false; //供应商
  Self.tvView2.Bands[4].Visible := False; //品牌
  Self.tvView2.Bands[5].Visible := False; //规格
  Self.tvView2.Bands[6].Visible := False; //工种
  Self.tvView2.Bands[7].Visible := False; //工长
  Self.tvView2.Bands[8].Visible := True;  //单位
  Self.tvView2.Bands[9].Visible := True;  //工程量

  Self.tvView2.Bands[10].Visible:= False; //成本报价 (人工单价 、 材料成本 、 成本单价)
  Self.tvView2.Bands[11].Visible:= False; //主材报价

  Self.tvView2.Bands[14].Visible:= False;  //综合报价 ( 人工 、主料 、 辅料 、 损耗 、 综合单价  )
  Self.tvView2.Bands[21].Visible:= False;  //折扣率
  Self.tvView2.Bands[22].Visible:= False;  //折后单价
  Self.tvView2.Bands[23].Visible:= False;  //合计金额
  Self.tvView2.Bands[24].Visible:= False;  //折后金额
  Self.tvView2.Bands[25].Visible:= False;  //主材合计
  Self.tvView2.Bands[26].Visible:= False;  //材料合计
  Self.tvView2.Bands[27].Visible:= False;  //人工合计
  Self.tvView2.Bands[28].Visible:= False;  //成本合计
  Self.tvView2.Bands[29].Visible:= False;  //利润
  Self.tvView2.Bands[30].Visible:= False;  //折后利润
  Self.tvView2.Bands[31].Visible:= True;  //工艺

  Self.tvView2.Bands[33].Visible:= True;
  Self.tvView2.Bands[32].Visible:= True;
  tvDblClick(Sender);
  dwNucleusCount := False;
end;

procedure TfrmCustomerPlate.N26Click(Sender: TObject);
begin
  inherited;
  RzClient.ActivePage := RzInfo;
//  dxGroupName.Active := False;
end;

procedure TfrmCustomerPlate.N27Click(Sender: TObject);
  function SQLTotal( lpSQLText : string):Currency;
  begin
    with DM.Qry do
    begin
      Close;
      SQL.Text := lpSQLText;
      Open;
      if RecordCount <> 0 then
      begin
        Result := Fields[0].AsCurrency;
      end;
    end;
  end;

  function GroupSQLText(lpFieldName : string; lpOfferType : Byte):string;
  var
    lvOfferType : string;
  begin
    lvOfferType := 'OfferType';
    Result := 'Select SUM( '+ lpFieldName +' ) AS t from '+ g_Table_Decorate_OfferList +
               ' WHERE tages=' + IntToStr(dwNodeIndex) + ' AND '+ lvOfferType +'=' + IntToStr(lpOfferType);
  end;

var
  i : Integer;
  s : Variant;
  Child : TfrmCustomerCostCalc ;
  lvMainMaterialTotal : Currency; //主材合计
  lvManpowerSummary   : Currency; //人工合计
  lvAfterDismantSummary : Currency; //折后金额
  lvGroupName : string;
begin
  inherited;

  with Self.tvView2.Controller do
  begin
    for I := 0 to SelectedRowCount-1 do
    begin
      if SelectedRows[i] is TcxGridGroupRow then
      begin

      end else begin

        if  (VarToStr( SelectedRows[i].Values[tvGroupName.Index] ) = dwExpensesName )  then
        begin
        //tvView2Col25    折扣金额
        //tvView2Col26    主材合计
        //tvView2Column2  人工  合计
        //tvView2Col24    合计金额
          tvAutoCalc.EditValue  := True;

          lvMainMaterialTotal   := SQLTotal( GroupSQLText( tvView2Col25.DataBinding.FieldName, 2 ));
          lvManpowerSummary     := SQLTotal( GroupSQLText( tvView2Column2.DataBinding.FieldName,3));
          lvAfterDismantSummary := StrToCurrDef( VarToStr( tvView2Col25.EditValue ) , 0);

          lvGroupName := VarToStr(tvProjectName.EditValue);
          Child := TfrmCustomerCostCalc.Create(Application);
          try
            Child.dwAcreage   := dwAcreage;
            Child.dwGroupName := lvGroupName;
            Child.dwMainMaterialTotal := lvMainMaterialTotal;
            Child.dwManpowerSummary   := lvManpowerSummary;

            Child.dwDirectFee     := dwDirectFee;
            Child.dwExtrasSummary := dwExtrasSummary;
            Child.dwAfterDismantSummary := lvAfterDismantSummary;
            //************************
            Child.cxDBComboBox1.DataBinding.DataSource := Self.dsSource;
            Child.cxDBSpinEdit1.DataBinding.DataSource := Self.dsSource;
            Child.cxDBCurrencyEdit1.DataBinding.DataSource := Self.dsSource;
            Child.cxDBCurrencyEdit2.DataBinding.DataSource := Self.dsSource;
            dwFormAutoCalc := True;
            Child.ShowModal;
            dwFormAutoCalc := False;
          finally
            Child.Free;
          end;
        end;
      end;

      Break;

    end;

  end;

end;

procedure TfrmCustomerPlate.N29Click(Sender: TObject);
begin
  inherited;
  tvAutoCalc.EditValue := False;
end;

procedure TfrmCustomerPlate.N31Click(Sender: TObject);
begin
  inherited;
  Self.RzBut.Click;
end;

procedure TfrmCustomerPlate.N34Click(Sender: TObject);
begin
  inherited;
  dwIsExcel := True;
  CxGridToExcel(Grid2,'明细表');
  dwIsExcel := False;
end;

procedure TfrmCustomerPlate.N43Click(Sender: TObject);
var
  lvRowRecoud : Integer;
  lvStatus : Boolean;
  I: Integer;
  lvIndex : Integer;

begin
  inherited;
  lvIndex := tvView2Col20.Index;
  with tvView2.Controller do
  begin
   lvRowRecoud := SelectedRowCount ;
    if lvRowRecoud <> 0 then
    begin
      for I := 0 to lvRowRecoud-1 do
      begin
        if Sender = N43 then
          SelectedRows[i].Values[lvIndex] := False  ;

        if Sender = N48 then
          SelectedRows[i].Values[lvIndex] := True;
      end;

    end;
  end;

end;

procedure TfrmCustomerPlate.N4Click(Sender: TObject);
begin
  inherited;
  //数据只显示主材报价小组
  dwOfferType := 2;
  dwIsTotal := 0;
  SetFieldName(0);
  Self.RzBut.Visible := False ;
  Self.tvView2.Bands[1].Visible := True;  //项目名称
  Self.tvView2.Bands[3].Visible := False; //供应商
  Self.tvView2.Bands[4].Visible := True; //品牌
  Self.tvView2.Bands[5].Visible := True; //规格
  Self.tvView2.Bands[6].Visible := False; //工种
  Self.tvView2.Bands[7].Visible := False; //工长
  Self.tvView2.Bands[8].Visible := True;  //单位
  Self.tvView2.Bands[9].Visible := True;  //工程量

  Self.tvView2.Bands[10].Visible:= False; //成本报价 (人工单价 、 材料成本 、 成本单价)
  Self.tvView2.Bands[11].Visible:= true; //主材报价

  Self.tvView2.Bands[14].Visible:= False;  //综合报价 ( 人工 、主料 、 辅料 、 损耗 、 综合单价  )
  Self.tvView2.Bands[21].Visible:= true;  //折扣率
  Self.tvView2.Bands[22].Visible:= True;  //折后单价
  Self.tvView2.Bands[23].Visible:= False;  //合计金额
  Self.tvView2.Bands[24].Visible:= True;  //折后金额
  Self.tvView2.Bands[25].Visible:= True;  //主材合计
  Self.tvView2.Bands[26].Visible:= False;  //材料合计
  Self.tvView2.Bands[27].Visible:= False;  //人工合计
  Self.tvView2.Bands[28].Visible:= False;  //成本合计
  Self.tvView2.Bands[29].Visible:= False;  //利润
  Self.tvView2.Bands[30].Visible:= False;  //折后利润
  Self.tvView2.Bands[31].Visible:= True;  //工艺
  Self.tvView2.Bands[33].Visible:= False;
  Self.tvView2.Bands[32].Visible:= False;
  
  tvDblClick(Sender);
  dwNucleusCount := False;
end;

procedure TfrmCustomerPlate.N5Click(Sender: TObject);
begin
  inherited;
  dwOfferType := 1;
  dwIsTotal := 0;
  SetFieldName(0);
  Self.RzBut.Visible := True ;
  Self.tvView2.Bands[1].Visible := True;  //项目名称
  Self.tvView2.Bands[3].Visible := False; //供应商
  Self.tvView2.Bands[4].Visible := False; //品牌
  Self.tvView2.Bands[5].Visible := False; //规格
  Self.tvView2.Bands[6].Visible := False; //工种
  Self.tvView2.Bands[7].Visible := False; //工长
  Self.tvView2.Bands[8].Visible := True;  //单位
  Self.tvView2.Bands[9].Visible := True;  //工程量
  
  Self.tvView2.Bands[10].Visible:= False; //成本报价 (人工单价 、 材料成本 、 成本单价)
  Self.tvView2.Bands[11].Visible:= false; //主材报价
  
  Self.tvView2.Bands[14].Visible:= true;  //综合报价 ( 人工 、主料 、 辅料 、 损耗 、 综合单价  )

  Self.tvView2.Bands[16].Visible:= False;
  Self.tvView2.Bands[17].Visible:= False;
  Self.tvView2.Bands[18].Visible:= False;
  Self.tvView2.Bands[19].Visible:= False;

  Self.tvView2.Bands[21].Visible:= true;  //折扣率
  Self.tvView2.Bands[22].Visible:= True;  //折后单价
  Self.tvView2.Bands[23].Visible:= True;  //合计金额
  Self.tvView2.Bands[24].Visible:= True;  //折后金额
  Self.tvView2.Bands[25].Visible:= False;  //主材合计
  Self.tvView2.Bands[26].Visible:= False;  //材料合计
  Self.tvView2.Bands[27].Visible:= False;  //人工合计
  Self.tvView2.Bands[28].Visible:= False;  //成本合计
  Self.tvView2.Bands[29].Visible:= False;  //利润
  Self.tvView2.Bands[30].Visible:= False;  //折后利润
  Self.tvView2.Bands[31].Visible:= True;  //工艺
  Self.tvView2.Bands[33].Visible:= False;
  Self.tvView2.Bands[32].Visible:= False;
  tvDblClick(Sender);
  dwNucleusCount := False;
end;

procedure TfrmCustomerPlate.N6Click(Sender: TObject);
begin
  inherited;
  dwOfferType := 1;
  dwIsTotal := 1;
  SetFieldName(1);
  Self.RzBut.Visible := False ;
  Self.tvView2.Bands[1].Visible := True;  //项目名称
  Self.tvView2.Bands[3].Visible := False; //供应商
  Self.tvView2.Bands[4].Visible := False; //品牌
  Self.tvView2.Bands[5].Visible := False; //规格
  Self.tvView2.Bands[6].Visible := True; //工种
  Self.tvView2.Bands[7].Visible := True; //工长
  Self.tvView2.Bands[8].Visible := True;  //单位
  Self.tvView2.Bands[9].Visible := True;  //工程量
  
  Self.tvView2.Bands[10].Visible:= True; //成本报价 (人工单价 、 材料成本 、 成本单价)
  Self.tvView2.Bands[11].Visible:= False; //主材报价
  Self.tvView2.Bands[12].Visible:= True;
  Self.tvView2.Bands[13].Visible:= True;

  Self.tvView2.Bands[14].Visible:= False;  //综合报价 ( 人工 、主料 、 辅料 、 损耗 、 综合单价  )
  {
  Self.tvView2.Bands[16].Visible:= False; //人工
  Self.tvView2.Bands[17].Visible:= False; //主料
  Self.tvView2.Bands[18].Visible:= False; //辅料
  Self.tvView2.Bands[19].Visible:= False; //损耗
  }
  Self.tvView2.Bands[20].Visible:= True;  //综合单价

  Self.tvView2.Bands[21].Visible:= False;  //折扣率
  Self.tvView2.Bands[22].Visible:= False;  //折后单价
  Self.tvView2.Bands[23].Visible:= True;  //合计金额
  Self.tvView2.Bands[24].Visible:= True;  //折后金额
  Self.tvView2.Bands[25].Visible:= False;  //主材合计
  Self.tvView2.Bands[26].Visible:= True;  //材料合计
  Self.tvView2.Bands[27].Visible:= True;  //人工合计
  Self.tvView2.Bands[28].Visible:= True;  //成本合计
  Self.tvView2.Bands[29].Visible:= True;  //利润
  Self.tvView2.Bands[30].Visible:= True;  //折后利润
  Self.tvView2.Bands[31].Visible:= True;  //工艺
  Self.tvView2.Bands[33].Visible:= False;
  Self.tvView2.Bands[32].Visible:= False;
  dwNucleusCount := True;
  tvDblClick(Sender);
  Self.RzBut5.Enabled := False;
end;

procedure TfrmCustomerPlate.N7Click(Sender: TObject);
begin
  inherited;
  dwOfferType := 3;
  dwIsTotal:=3;
  SetFieldName(1);
  Self.RzBut.Visible := False ;
  Self.tvView2.Bands[1].Visible := True;  //项目名称
  Self.tvView2.Bands[3].Visible := False; //供应商
  Self.tvView2.Bands[4].Visible := False; //品牌
  Self.tvView2.Bands[5].Visible := False; //规格
  Self.tvView2.Bands[6].Visible := True; //工种
  Self.tvView2.Bands[7].Visible := True; //工长
  Self.tvView2.Bands[8].Visible := True;  //单位
  Self.tvView2.Bands[9].Visible := True;  //工程量
  
  Self.tvView2.Bands[10].Visible:= True; //成本报价 (人工单价 、 材料成本 、 成本单价)
  Self.tvView2.Bands[12].Visible:= False;
  Self.tvView2.Bands[13].Visible:= False;
  Self.tvView2.Bands[15].Visible:= True;

  Self.tvView2.Bands[11].Visible:= False; //主材报价
  
  Self.tvView2.Bands[14].Visible:= False;  //综合报价 ( 人工 、主料 、 辅料 、 损耗 、 综合单价  )

  Self.tvView2.Bands[21].Visible:= False;  //折扣率
  Self.tvView2.Bands[22].Visible:= False;  //折后单价
  Self.tvView2.Bands[23].Visible:= False;  //合计金额
  Self.tvView2.Bands[24].Visible:= False;  //折后金额
  Self.tvView2.Bands[25].Visible:= False;  //主材合计
  Self.tvView2.Bands[26].Visible:= False;  //材料合计
  Self.tvView2.Bands[27].Visible:= False;   //人工合计
  Self.tvView2.Bands[28].Visible:= True;  //成本合计
  Self.tvView2.Bands[29].Visible:= True;  //利润
  Self.tvView2.Bands[30].Visible:= False;  //折后利润
  Self.tvView2.Bands[31].Visible:= True;   //工艺
  Self.tvView2.Bands[33].Visible:= True;  //人工合计
  Self.tvView2.Bands[32].Visible:= False;  //人工报价
  dwNucleusCount := True;
  tvDblClick(Sender);
  Self.RzBut5.Enabled := False;
end;

procedure TfrmCustomerPlate.N8Click(Sender: TObject);
begin
  inherited;
  dwOfferType := 2;
  dwIsTotal := 2;
  SetFieldName(1);
  Self.RzBut.Visible := False ;
  Self.tvView2.Bands[1].Visible := True;  //项目名称
  Self.tvView2.Bands[3].Visible := True; //供应商
  Self.tvView2.Bands[4].Visible := True; //品牌
  Self.tvView2.Bands[5].Visible := True; //规格
  Self.tvView2.Bands[6].Visible := False; //工种
  Self.tvView2.Bands[7].Visible := False; //工长
  Self.tvView2.Bands[8].Visible := True;  //单位
  Self.tvView2.Bands[9].Visible := True;  //工程量
  
  Self.tvView2.Bands[10].Visible:= True; //成本报价 (人工单价 、 材料成本 、 成本单价)
  Self.tvView2.Bands[12].Visible:= False;
  Self.tvView2.Bands[13].Visible:= False;
  Self.tvView2.Bands[15].Visible:= True;
  Self.tvView2.Bands[11].Visible:= False; //主材报价

  Self.tvView2.Bands[14].Visible:= False;  //综合报价 ( 人工 、主料 、 辅料 、 损耗 、 综合单价  )
  Self.tvView2.Bands[21].Visible:= False;  //折扣率
  Self.tvView2.Bands[22].Visible:= False;  //折后单价
  Self.tvView2.Bands[23].Visible:= False;  //合计金额
  Self.tvView2.Bands[24].Visible:= True;  //折后金额
  Self.tvView2.Bands[25].Visible:= True;  //主材合计
  Self.tvView2.Bands[26].Visible:= False;  //材料合计
  Self.tvView2.Bands[27].Visible:= False;  //人工合计
  Self.tvView2.Bands[28].Visible:= True;  //成本合计
  Self.tvView2.Bands[29].Visible:= True;  //利润
  Self.tvView2.Bands[30].Visible:= True;  //折后利润
  Self.tvView2.Bands[31].Visible:= True;  //工艺
  Self.tvView2.Bands[33].Visible:= False;
  Self.tvView2.Bands[32].Visible:= False;
  dwNucleusCount := True;
  tvDblClick(Sender);
  Self.RzBut5.Enabled := True;
end;

function TfrmCustomerPlate.GetGroupName(lpIsActive : Boolean):Boolean;
var
  s : string;
  lvFieldName : string;
  lvGroupName : Variant;
  i : Integer;
begin
  dxGroupName.Active := lpIsActive;
  if not dxGroupName.Active then dxGroupName.Active := True;
  if dxGroupName.Active then
  begin
    lvFieldName := tvGroupName.DataBinding.FieldName;
    with Self.tvView2.ViewData do
    begin
      for I := 0 to RowCount-1 do
      begin
        if Rows[i] is TcxGridGroupRow then
        begin
          lvGroupName := Rows[i].Values[tvGroupName.Index];
          dxGroupName.Append();
          dxGroupName.FieldByName(lvFieldName).Value := lvGroupName;
          dxGroupName.Post;
        end;
      end;
    end;
  end;
  {
  for i := 0  to RecordCount-1 do
  begin
    Application.ProcessMessages;
    lvIsExist := False;
    lvGroupName := Self.tvView2.ViewData.Records[i].Values[tvGroupName.Index];

    with dxGroupName do
    begin

      First;

      while not Eof  do
      begin
        s := FieldByName(tvView1Col1.DataBinding.FieldName).AsString;
        lvIsExist := False;;
        if s = VarToStr(lvGroupName) then
        begin
          lvIsExist := True;
          Break;
        end;
        Next;
      end;

      if lvIsExist = False then
      begin
        Append;
          FieldByName(tvGroupName.DataBinding.FieldName).Value := lvGroupName;
        Post;
      end;
    end;

  end;
  }
end;
  
class procedure TMyCxGrid.DrawIndicatorCell(Sender: TcxGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxCustomGridIndicatorItemViewInfo; var ADone: Boolean);
var
  FValue: string;
  FBounds: TRect;
  //method 2
  AIndicatorViewInfo: TcxGridIndicatorRowItemViewInfo;
  ATextRect: TRect;
  AFont: TFont;
  AFontTextColor, AColor: TColor;
  iRec,i: Integer;
begin
  AFont  := ACanvas.Font;
  AColor := clBtnFace;
  AFontTextColor := clWindowText;
  if (AViewInfo is TcxGridIndicatorHeaderItemViewInfo) then
  begin
    ATextRect := AViewInfo.Bounds;
    InflateRect(ATextRect, -1, -1);

    Sender.LookAndFeelPainter.DrawHeader(ACanvas, AViewInfo.Bounds,
      ATextRect, [], cxBordersAll, cxbsNormal, taCenter, TcxAlignmentVert.vaCenter,
      False, False, 'No.',AFont, AFontTextColor, AColor);
    ADone := True;
  end;

  if not (AViewInfo is TcxGridIndicatorRowItemViewInfo) then Exit;

  ATextRect := AViewInfo.ContentBounds;
  AIndicatorViewInfo := AViewInfo as TcxGridIndicatorRowItemViewInfo;
  InflateRect(ATextRect, -1, -1);

  if AIndicatorViewInfo.GridRecord.Selected then
    AFont.Style := ACanvas.Font.Style + [fsBold]
  else
    AFont.Style := ACanvas.Font.Style - [fsBold];

  Sender.LookAndFeelPainter.DrawHeader(ACanvas, AViewInfo.ContentBounds,
    ATextRect, [], [bBottom, bLeft, bRight], cxbsNormal, taCenter, TcxAlignmentVert.vaCenter,
    False, False, IntToStr(AIndicatorViewInfo.GridRecord.Index + 1),
    AFont, AFontTextColor, AColor);

  ADone := True;

end;

procedure TfrmCustomerPlate.CalculatData(Sender: TObject);

  function IsUnitPrice(lpDisMantPrice , lpCostUnitPrice : Currency) : Boolean;
  begin
    Result := False;
    if lpDisMantPrice < lpCostUnitPrice then
    begin
      Result := True;
      Application.MessageBox('折后单价不能低于成本单价','提示:',MB_OK + MB_ICONEXCLAMATION);
      Exit;
    end;
  end;

var
  st: integer;
  lvArtificialPrice : Variant; //人工单价
  lvMaterialPrice   : Variant; //材料成本
  lvCostUnitPrice   : Variant; //成本单价
  lvDismantRate     : Variant; //折扣率
  lvDismantPrice    : Currency;
  lvManpowerPrice   : Variant; //人工报价
  lvQuantities : Variant; //工程量
  lvMainMaterialPrice : Variant;  //主材单价
  t : DWORD;
  szMoney : Double;

begin
  lvQuantities      := tvView2Col6.EditValue ;  //工程量
  lvDismantRate     := tvView2Col22.EditValue;  //折扣率
  lvArtificialPrice := tvView2Col9.EditValue;   //人工单价
  lvMaterialPrice   := tvView2Col10.EditValue;  //材料成本
  lvManpowerPrice   := tvView2Column1.EditValue;//人工报价

  //***********************
  //成本合计
  if (dwOfferType = 1) and (dwIsTotal = 1) then //(dwOfferType <> 3) and (dwOfferType <> 2) then
  begin
    tvView2Col11.EditValue  := StrToCurrDef(VarToStr(tvView2Col9.EditValue) ,0) +
                               StrToCurrDef(VarToStr(tvView2Col10.EditValue),0);
  end;
  lvCostUnitPrice := tvView2Col11.EditValue; //成本单价
//  tvView2Col29.EditValue  := StrToFloatDef(VarToStr( lvQuantities ) , 0) * StrToCurrDef(VarToStr( lvCostUnitPrice ) , 0 );
  //***********************
{
//  tvView2Col16.EditValue ;  分成四项
    tvView2Col12.EditValue; //人工
    tvView2Col13.EditValue; //主料
    tvView2Col14.EditValue; //辅料
    tvView2Col15.EditValue; //损耗
}

  szMoney := StrToCurrDef( VarToStr(tvView2Col16.EditValue )  ,0);
  if Self.tvView2.Controller.FocusedRow.Values[tvGroupName.Index] <> dwExpensesName then
  begin
    Self.tvView2Col12.EditValue := szMoney * 45 / 100; //人工
    Self.tvView2Col14.EditValue := szMoney * 10 / 100; //辅料

    Self.tvView2Col13.EditValue := szMoney * 40 / 100; //主料
    Self.tvView2Col15.EditValue := szMoney * 5  / 100; //损耗
  end;


{
1.客户报价
 （1）主材报价
	工程量 * 主材报价   = 主材合计
	工程量 * 主材报价 * 折扣率 = 折后金额
 （2）快捷报价
	工程量 * 综合单价   = 合计金额
	工程量 * 综合单价 * 折扣率 = 折后金额

**********************************
2.成本核算
  成本核算  后加的工程量 *  成本单价
  工程量 * (人工单价 + 材料单价 = 成本单价） = 成本合计
 （人工单价、材料单价，从报价库取）
  
3.人工核算
  工程量 * 人工单价 = 人工合计
  人工核算  后加的工程量 *  成本单价


4.材料核算
  工程量 * 材料单价 = 材料合计
  主材核算  后加的工程量 *  成本单价

**********************************
  
  工程量 x 主材单价 x 折扣率 ＝ 主材合计
  工程量 x 人工单价 ＝ 人工合计
  工程量 x 材料单价 ＝ 材料合计

  人工 ＋ 材料  = 成本单价

  工程量 x 成本单价           ＝ 成本合计
  工程量 x 综合单价(分四项比）＝ 合计金额
  工程量 x 综合单价折后       ＝ 折后金额合计
                                            n
  合计金额 - 成本 = 利润
  折后金额合计 － 成本 ＝ 折后利润合计

}
  if dwNucleusCount then
  begin
    //核算部分
    //***********************
    case dwIsTotal of
      1:
      begin
        //成本合计
        tvView2Col29.EditValue := StrToCurrDef(VarToStr( lvQuantities ) , 0) * StrToCurrDef(VarToStr( lvCostUnitPrice ) , 0);
        //利润
        tvView2Col30.EditValue := StrToCurrDef(VarToStr( tvView2Col24.EditValue ) , 0) - StrToCurrDef(VarToStr( tvView2Col29.EditValue ) , 0);
        //折后利润
        tvView2Col31.EditValue := StrToCurrDef(VarToStr( tvView2Col25.EditValue)  , 0) - StrToCurrDef(VarToStr( tvView2Col29.EditValue ) , 0);
        //人工成本
        tvView2Col28.EditValue := StrToCurrDef(VarToStr( lvQuantities ) , 0) * StrToCurrDef(VarToStr( lvArtificialPrice ) , 0);
        //材料合计
        tvView2Col27.EditValue := StrToCurrDef(VarToStr( lvQuantities ) , 0) * StrToCurrDef(VarToStr( lvMaterialPrice ) , 0);
        //材料成本 * 工程量 = 材料合计
        //人工单价 * 工程量 = 人工成本
      end;
      2:
      begin
        //材料合计
        tvView2Col29.EditValue := StrToCurrDef(VarToStr( lvCostUnitPrice ) , 0)  *  StrToCurrDef(VarToStr( lvQuantities ) , 0) ;
        //利润
        tvView2Col30.EditValue := StrToCurrDef(VarToStr( tvView2Col26.EditValue ) , 0) - StrToCurrDef(VarToStr( tvView2Col29.EditValue ) , 0);
        //折后利润
        tvView2Col31.EditValue := StrToCurrDef(VarToStr( tvView2Col25.EditValue)  , 0) - StrToCurrDef(VarToStr( tvView2Col29.EditValue ) , 0);
      end;
      3:
      begin
        //成本合计
        tvView2Col29.EditValue := StrToCurrDef(VarToStr( lvQuantities ) ,0) * StrToCurrDef(VarToStr( lvCostUnitPrice ) , 0);
        //人工合计
        //成本合计
        tvView2Col30.EditValue := StrToCurrDef(VarToStr( tvView2Column2.EditValue ) , 0) - StrToCurrDef(VarToStr( tvView2Col29.EditValue ) , 0)
      end;
    end;

  end else
  begin
    //正常计算
    if dwOfferType = 2 then
    begin
      //主材报价计算
      lvMainMaterialPrice := Self.tvView2Col8.EditValue ;  //主材报价
      if (lvQuantities <> Null) and (lvMainMaterialPrice <> null) then
      begin
        //***********************
        //折后单价
        lvDismantPrice := StrToCurrDef(VarToStr( lvMainMaterialPrice ),0) / 100 * StrToCurrDef( VarToStr(lvDismantRate) ,0) ;
        //折后单价 对比 成本单价
        if IsUnitPrice(lvDismantPrice,  StrToCurrDef( VarToStr( lvCostUnitPrice ) ,0 ) ) then  Exit;

        Self.tvView2Col23.EditValue := lvDismantPrice;
        //***********************
        Self.tvView2Col25.EditValue := StrToFloatDef(VarToStr(lvQuantities) , 0) * lvDismantPrice ; //主材合计 * 折扣率
        Self.tvView2Col26.EditValue := StrToFloatDef(VarToStr(lvQuantities) , 0) *
        StrToCurrDef(VarToStr(lvMainMaterialPrice) ,0 ); //主材合计


      end;
    end;

    if dwOfferType = 1 then
    begin
      //客户报价
      //***********************
      //折后单价
      lvDismantPrice := szMoney / 100 * StrToCurrDef( VarToStr(lvDismantRate) ,0) ;
      //折后单价 对比 成本单价
      if IsUnitPrice(lvDismantPrice,  StrToCurrDef( VarToStr( lvCostUnitPrice ) ,0 ) ) then  Exit;

      Self.tvView2Col23.EditValue := lvDismantPrice;
      //***********************
      tvView2Col24.EditValue := StrToFloatDef(VarToStr(lvQuantities) , 0) * szMoney;
      tvView2Col25.EditValue := StrToFloatDef(VarToStr(lvQuantities) , 0) * lvDismantPrice ; //主材合计 * 折扣率

    end;

    if dwOfferType = 3 then
    begin
      tvView2Column2.EditValue := StrToCurrDef( VarToStr( lvManpowerPrice ) , 0) * StrToFloatDef(VarToStr(lvQuantities) , 0);
    end;

  end;

end;

procedure TfrmCustomerPlate.cxDBButtonEdit1PropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
var
  Child : TfrmCompanyManage;
  lvSignName : Variant;
  lvTelePhoneNumber: Variant;

begin
  Child := TfrmCompanyManage.Create(Application);
  try
    Child.dwIsReadonly := True;
    Child.ShowModal;
    TcxDBButtonEdit(Sender).EditValue :=  Child.tvViewColumn2.EditValue;// Child.dwSignName;
    TcxDBButtonEdit(Sender).PostEditValue;
    lvTelePhoneNumber := Child.tvViewColumn1.EditValue;
    if Sender = cxDBButtonEdit1 then
    begin
      Self.cxDBSpinEdit3.EditValue := lvTelePhoneNumber;
      Self.cxDBSpinEdit3.PostEditValue;
    end else
    if Sender = cxDBButtonEdit2 then
    begin
      Self.cxDBSpinEdit4.EditValue := lvTelePhoneNumber;
      Self.cxDBSpinEdit4.PostEditValue;
    end else
    if Sender = cxDBButtonEdit3 then
    begin
      Self.cxDBSpinEdit5.EditValue := lvTelePhoneNumber;
      Self.cxDBSpinEdit5.PostEditValue;
    end else
    if Sender = cxDBButtonEdit4 then
    begin
      Self.cxDBSpinEdit6.EditValue := lvTelePhoneNumber;
      Self.cxDBSpinEdit6.PostEditValue;
    end else
    if Sender = cxDBButtonEdit5 then
    begin
      TcxDBButtonEdit(Sender).EditValue :=  Child.dwSignName;
      TcxDBButtonEdit(Sender).PostEditValue;

      Self.cxDBSpinEdit8.EditValue := lvTelePhoneNumber;
      Self.cxDBSpinEdit8.PostEditValue;
    end;
  finally
    Child.Free;
  end;
end;

procedure TfrmCustomerPlate.cxDBCurrencyEdit2PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  lvMoney : Currency;
  
begin
  inherited;
  lvMoney := StrToCurrDef(VarToStr(DisplayValue) , 0); 
  Self.cxDBCurrencyEdit2.EditValue := lvMoney;
  cxDBCurrencyEdit3.EditValue := (lvMoney / 100) * 45;
  cxDBCurrencyEdit3.PostEditValue;
  cxDBCurrencyEdit4.EditValue := (lvMoney / 100) * 50;
  cxDBCurrencyEdit4.PostEditValue;
  cxDBCurrencyEdit5.EditValue := (lvMoney / 100) * 5;
  cxDBCurrencyEdit5.PostEditValue;
end;

procedure TfrmCustomerPlate.cxDBDateEdit8PropertiesEditValueChanged(
  Sender: TObject);
var
  lvEndDate : TDateTime;
  lvStartDate : TDateTime;
  nDays : Integer;

begin
  inherited;
  lvStartDate := cxDBDateEdit2.Date;
  lvEndDate := cxDBDateEdit8.Date;
  nDays := DateTimeToTimeStamp(lvEndDate).Date - DateTimeToTimeStamp(lvStartDate).Date;
  cxDBSpinEdit9.Value := nDays;
  cxDBSpinEdit9.PostEditValue;
end;

procedure TfrmCustomerPlate.cxDBSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  s : Variant;

begin
  inherited;
  if DisplayValue <> null then
  begin
    s := VarToStr(DisplayValue);
    dwAcreage := StrToFloatDef(s,0);
  end;

end;

procedure TfrmCustomerPlate.cxDBSpinEdit7PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwWholeListdiscount := StrToIntDef(VarToStr(DisplayValue) , 0);
end;

procedure TfrmCustomerPlate.cxLookupComboBox1PropertiesCloseUp(Sender: TObject);
var
  lvProjectName : string;
begin
  inherited;
  lvProjectName := Self.cxLookupComboBox1.Text;
  with Self.dslib do
  begin
    if State <> dsInactive then
    begin
      Filtered := False;
      Filter := 'ProjectName Like '''+ lvProjectName +'%''';
      Filtered := true;
    end;
  end;
end;

procedure TfrmCustomerPlate.cxLookupComboBox2PropertiesChange(Sender: TObject);
begin
  inherited;
  if cxLookupComboBox2.Text = '' then
    N40.Visible := False
  else
    N40.Visible := True;
end;

procedure TfrmCustomerPlate.cxLookupComboBox2PropertiesEditValueChanged(
  Sender: TObject);
begin
  inherited;
  Self.RzToolButton5.Click;
end;

procedure TfrmCustomerPlate.cxLookupComboBox3PropertiesCloseUp(Sender: TObject);
var
  lvSpec : string;
begin
  inherited;
  lvSpec := Self.cxLookupComboBox3.Text;
  with Self.dslib do
  begin
    if State <> dsInactive then
    begin
      Filtered := False;
      Filter := 'Spec Like '''+ lvSpec +'%''';
      Filtered := true;
    end;
  end;

end;

procedure TfrmCustomerPlate.cxSpinEdit1PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  dwWholeListdiscount := StrToIntDef(VarToStr( DisplayValue ) , 0);
  tvDblClick(Sender);
end;

//修改表现值
procedure TfrmCustomerPlate.SetGridRow(lpGrid : TcxGridDBBandedTableView ; lpFileName : string);
var
  i:Integer;
  iRec : Integer;
  lvIndex : Integer;
  lvIsUpdate : Boolean;
  t:Integer;
  // TcxGridBandedTableView
begin
  with lpGrid do
  begin
  //  BeginUpdate();
    iRec := 0;
    lvIndex := tvGroupIndex.Index;
    lvIsUpdate := False;
    t:=GetCurrentTime;

  //  DataController.Edit;
    for i := 0 to ViewData.RowCount-1 do
    begin
      if ViewData.Rows[i].Values[tvGroupName.Index] = lpFileName then
      begin
        Controller.FocusedRowIndex := i;
      //  ViewData.Rows[i].Values[lvIndex] := iRec;
      //  DataController.Values[i,lvIndex] := iRec;
      //  DataController.DataSet.FieldByName('iRec').Value := iRec;

        Inc(iRec);
        lvIsUpdate := True;
      end;
    end;
  //  DataController.UpdateData;
  //  if lvIsUpdate then DataController.DataSet.Post;
  //  EndUpdate;

  end;

end;

procedure TfrmCustomerPlate.tmrClockTimer(Sender: TObject);
begin
  inherited;
  tmrClock.Enabled:=False;
  GetSumMoney(0);
  //更新之后这个是对的
  if GetTickCount - dwSleepTimer >= 100 then
  begin
    PostMessage(Handle,WM_LisView,0,0);
    dwSleepTimer := GetTickCount;
  end;
end;

procedure TfrmCustomerPlate.btn3Click(Sender: TObject);

  function GetFirstChild(ARecord: TcxCustomGridRecord): TcxCustomGridRecord;
  begin
    if ARecord is TcxGridMasterDataRow then
      Result := ARecord.GetFirstFocusableChild
    else
      if ARecord is TcxGridGroupRow then
        Result := ARecord.ViewData.Records[ARecord.Index + 1]
      else
        Result := nil;

  end;

var
  ARecord: TcxCustomGridRecord;
begin
  with TcxCustomGridTableView(tvView2) do
  begin
    ARecord := Controller.FocusedRecord;
    if Assigned(ARecord) then
    begin
      ARecord.Expand(False);
      ARecord := Controller.FocusedRecord;
      ARecord := GetFirstChild(ARecord);
      if Assigned(ARecord) then
        ARecord.Focused := True;
    end;

  end;
end;

procedure TfrmCustomerPlate.dsClientInfoAfterInsert(DataSet: TDataSet);
begin
  inherited;
  with DataSet do
  begin
    FieldByName('tages').Value := dwNodeIndex;
    FieldByName('status').Value:= True;
    FieldByName('MarketingCheckStatus').Value := True;
    FieldByName('DesignCheckStatus').Value := True;
    FieldByName(cxDBComboBox4.DataBinding.DataField).Value := '报价预算表';
    FieldByName(cxDBSpinEdit7.DataBinding.DataField).Value := 100;
    FieldByName(cxDBDateEdit9.DataBinding.DataField).Value := Date;
    FieldByName(cxDBDateEdit10.DataBinding.DataField).Value:= Date;
    FieldByName(cxDBDateEdit1.DataBinding.DataField).Value := Date;
    FieldByName(cxDBDateEdit2.DataBinding.DataField).Value := Date;
    FieldByName(cxDBDateEdit3.DataBinding.DataField).Value := Date;
    FieldByName(cxDBDateEdit4.DataBinding.DataField).Value := Date;
    FieldByName(cxDBDateEdit5.DataBinding.DataField).Value := Date;
    FieldByName(cxDBDateEdit6.DataBinding.DataField).Value := Date;
    FieldByName(cxDBDateEdit8.DataBinding.DataField).Value := Date;
    FieldByName(cxDBDateEdit8.DataBinding.DataField).Value := Date;
    FieldByName(cxDBTextEdit1.DataBinding.DataField).Value := dwTopLevelName;
  end; 
end;

procedure TfrmCustomerPlate.dsMasterAfterClose(DataSet: TDataSet);
begin
  Self.dxGroupName.Active := False;
end;

procedure TfrmCustomerPlate.dsMasterAfterInsert(DataSet: TDataSet);

  function GetViewNoId(lpNoId : string):Boolean;
  var
    I: Integer;
    lvIndex : Integer;
  begin
    Result := False;
    with tvView2.ViewData do
    begin
      for I := 0 to RowCount-1 do
      begin
        if VarToStr(Rows[i].Values[tvView2Col19.Index]) = lpNoId then
        begin
          Result := True;
          Break;
        end;

      end;

    end;

  end;

  function IsRowIndex(lpTableName,lpFieldName : string; lpCode , lpNumber : string ; lpIndex , lpRowCount : Integer ) : string;
  var
    Id : ShortString;

  begin
    Id := Format('%s',[ FormatdateTime('yymmdd',now) ]) ;
    with DM.Qry do
    begin
      Close;
      SQL.Clear;
      SQL.Text := 'Select * from ' + lpTableName + ' Where ' + lpFieldName + '="' + lpNumber + '"';
      Open;
      if RecordCount <> 0 then
      begin
        lpNumber := id + lpCode +  IntToStr(lpIndex + lpRowCount + 1);
        IsRowIndex(lpTableName,lpFieldName,lpCode,lpNumber,lpIndex,lpRowCount);
      end;

    end;
    Result := lpNumber;
  end;

  function GetRowMaxValue(lpTableName , lpFieldName : string):string;
  var
    s : string;
    i:Integer;
  begin

    if dwNoId = 0 then
    begin

    end;


    Result := '';
    i := Self.dsMaster.ChangeCount;
    s := 'SELECT MAX(' + lpFieldName + ') FROM ' + lpTableName;
    with DM.Qry do
    begin

      Close;
      SQL.Clear;
      SQL.Text := s;
      Open;
      if RecordCount <> 0 then
      begin
        s := Fields[0].AsString;
        if s <> '' then
        begin
          Result := IntToStr( StrToIntDef(s,0) + 1 + i);
        end;
      end;

    end;

  end;

  function GetRowCode(lpTableName,lpFieldName : string; lpCode : string = '0000' ; lpIndex : Integer = 10000):string;
  var
    Id : ShortString;
    i  :integer;
    Count:Integer;

  begin
    Result := GetRowMaxValue(lpTableName,lpFieldName);
    if Result = '' then
    begin
      Randomize;
      Count := Random(lpIndex);

      Id := Format('%s',[ FormatdateTime('yymmdd',now) ]) ;
      i  := DM.GetRowCount(lpTableName); //取出行数
      Result := id + lpCode +  IntToStr(lpIndex +  Count + i);
      IsRowIndex(lpTableName,lpFieldName,lpCode,Result,lpIndex +  Count,i);
    end;
  end;

var
  lvCode : string;
  lvNoIdColName : string;
  lvChangeCount : Int64;

  aGUID: string;

begin
//  Self.dsMaster.Delta
  lvNoIdColName := 'NoId';

  with DataSet do
  begin
  //  lvChangeCount := dsMaster.ChangeCount;

    lvCode := GetULID(Now) ; // GetRowCode( g_Table_sk_Decorate_OfferList ,lvNoIdColName);
    FieldByName(lvNoIdColName).Value := lvCode;
    FieldByName(tvGroupName.DataBinding.FieldName).Value := dwGroupName;
    FieldByName('tages').Value := dwNodeIndex;
    FieldByName('status').Value:= True;
    FieldByName('OfferType').Value := dwOfferType;
    FieldByName(tvView2Col22.DataBinding.FieldName).Value := 100;

  end;
  tvProjectName.FocusWithSelection;
end;

procedure TfrmCustomerPlate.dsMasterAfterDelete(DataSet: TDataSet);
function GetChildRowCount():Integer;
  var
    i : Integer;
    GRowIndex , GRowID: Integer;
  begin
    with Self.tvView2.Controller,Self.tvView2.DataController do
    begin
      if SelectedRowCount = 0 then
      begin
        FocusedRowIndex := 0;
      end;

      for i := 0 to SelectedRowCount - 1 do
      begin
        //判断选择的是否为组的行
        if Self.tvView2.Controller.SelectedRows[i] is TcxGridGroupRow   then
        begin
                                      //
        end;
      //  ShowMessage(IntToStr( TcxGridGroupRow(SelectedRows[i]).Index ));

        //取得组的行的index;
        GRowIndex := SelectedRows[i].Index;
        //取得组的行的ID；
        GRowID :=  Groups.DataGroupIndexByRowIndex[GRowIndex];
        //这里得到的是组的行的那个标题
      //  ShowMessage(Groups.GroupValues[GRowID]);
        //这里是组的行数
        Result := Groups.ChildCount[GRowID];
      //  ShowMessage(IntToStr( Groups.ChildCount[GRowID]));
      //  Self.tvView2.DataController.Groups.ChildRecordIndex
        Break;
      end;
    end;
  end;
begin
//  Self.mmo1.Lines.Add('delete');
//  GetSumMoney(0);
end;

procedure TfrmCustomerPlate.FormActivate(Sender: TObject);
var
  i : Integer;

begin
  Caption := '报价';
  N1.Visible  := False;
  N2.Visible  := False;
  N3.Visible  := False;
  N32.Visible := False;

  N33.Visible := False; //主材报价
  N35.Visible := False; //成本核算清单
  N36.Visible := False; //主材核算清单
  N37.Visible := False; //人工核算清单

  dwQuantities[0] := 'Quantities';
  dwQuantities[1] := 'AQuantities';
  dwExpensesName  := '费用';
  Self.tvView2.OptionsView.Header     := False;
  Self.tvView2.OptionsView.GroupByBox := False;

//  tvGroupName.SortOrder := soNone;
  tvView2Column4.SortOrder := soAscending;

  dwTreeView := TNewUtilsTree.Create(Self.tv,DM.ADOconn,g_Table_Decorate_OfferBranchTree,'往来单位');
  dwtreeview.GetTreeTable( dwParentId  );

  Self.tvView2.OptionsView.Indicator:=True;
  Self.tvView2.OptionsView.IndicatorWidth:=30;
  Self.tvView2.OnCustomDrawIndicatorCell:= TMyCxGrid.DrawIndicatorCell;

  {
  for i:=1 to Self.tvView2.ColumnCount-1 do
  begin
    tvView2.Columns[i].ApplyBestFit();
  end;
  }
  case dwOfferType  of
   0: ;
   1: N5.Click;
   2: N4.Click;
   3: N7.Click; //人工报价
  end;

  if dwOfferType = 2 then
  begin
    RzToolButton3.Visible := False;
    RzBut.Visible := False;
    RzClient.ActivePage := RzCustomer;
  end;

  if dwOfferType = 0 then
  begin
     N4.Visible := False;
     N6.Visible := True;
     N7.Visible := True;
     N8.Visible := True;

     N24.Visible:= False;
     N25.Visible:= False;
     N5.Visible := False;
     N6.Click;
  end else
  begin
     N4.Visible := False;
     N6.Visible := False;
     N7.Visible := False;
     N8.Visible := False;
     N24.Visible:= False;
  end;
  Self.RzBut6.Click;
  InitializeEditors(Sender);
end;

procedure TfrmCustomerPlate.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
  
end;

procedure TfrmCustomerPlate.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
  SaveInfo();
  if dwADO.ADOIsSaveData(Handle,Self.dsMaster) then
    CanClose := False
  else
    CanClose := True;
end;

procedure TfrmCustomerPlate.FormCreate(Sender: TObject);
begin
  inherited;
  Self.RzClient.ActivePage := RzInfo;
end;

procedure TfrmCustomerPlate.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  lvRebateSummary : Currency;
  Child : TfrmCustomerSummary;
  lvAcreage : Double;
  lvNodeIndex : Integer;
begin
//83 = s
//90 = z
  if Self.RzClient.ActivePage = RzCustomer then
  begin
    if Key = 45 then
       RzBut2.Click;
    if (ssCtrl in Shift) and (Key = 83) then
      Self.RzBut4.Click;

    if (ssCtrl in Shift) and (Key = 90) then
      Self.dsMaster.UndoLastChange(true);

    if (ssCtrl in Shift) and (Key = 116) then
    begin
    //  if (dwOfferType = 1) or (dwIsTotal = 1) then
    //  begin
        lvAcreage   := dwAcreage;
        lvNodeIndex := dwNodeIndex;
        Child := TfrmCustomerSummary.Create(Application);
        try
          with Child do
          begin
            dwNodeIndex  := lvNodeIndex;
            dwManpowerCost := SQLTotal( GroupSQLText(tvView2Col29.DataBinding.FieldName,3)); //取出人工-成本合计
          //数据库OfferType 1 = 快捷报价   2 = 主材报价 3 = 人工报价
          //  dwManpowerSummary := GetGridSummary(tvView2Column2.Index,3);//人工合计

            dwManpowerSummary := SQLTotal( GroupSQLText(tvView2Column2.DataBinding.FieldName,3));
            dwDesignCost := GetGridSummary(tvView2Col25.Index,4); //设计费

            dwTaxGold    := GetGridSummary(tvView2Col25.Index,1); //税金

            dwSumMoney   := GetGridSummary(tvView2Col24.Index,2); //不含主材折前
            dwSumMoney   := dwSumMoney - dwTaxGold + dwManpowerSummary;//不含税折前总额

            dwAfterDismantSummary := GetGridSummary(tvView2Col25.Index,2); //不含主材折后
            dwAfterDismantSummary := (dwAfterDismantSummary - dwTaxGold) / 100 * dwWholeListdiscount +
            dwManpowerSummary;    //不含税折后总额

            dwLaborCost       := GetGridSummary(tvView2Col28.Index,0);  //人工成本
            dwMaterialSummary := GetGridSummary(tvView2Col27.Index,0);  //材料合计
            dwCostSummary     := GetGridSummary(tvView2Col29.Index,0);  //成本合计
            dwAcreage := lvAcreage;
          end;

          Child.ShowModal;
        finally
          Child.Free;
        end;

    end;

  end;
  
  if Key = 13 then
  begin
  
    if Self.RzClient.ActivePage = RzInfo then
    begin
      with Self.dsClientInfo do
      begin
        if State in [dsInsert , dsEdit] then
        begin
          Post;
        end;  
      end;  
    end; 
     
  end;   
end;

procedure TfrmCustomerPlate.RzBut2Click(Sender: TObject);
var
  lvCode : string;
  lvExIst : Boolean;
  bm:TBookmark;
  Child : TfrmCustomerPost;
begin
  Child := TfrmCustomerPost.Create(nil);
  try
    Child.ShowModal;
    dwGroupName := Child.cxTextEdit1.Text;
  finally
    Child.Free;
  end;

  if dwGroupName <> '' then
  begin
    IsAlwaysExpanded(True);
    with Self.dsMaster do
    begin

      if State <> dsInactive then
      begin
        DisableConstraints;
        First;
        lvExIst := False;
        while not Eof do
        begin
          if FieldByName(tvGroupName.DataBinding.FieldName).AsString = Trim( dwGroupName ) then
          begin
            ShowMessage('相同房间已存在');
            lvExIst := True;
            Break;
          end;
          Next;
        end;

        if not lvExIst then
        begin
          Insert;
          Post;
        end;
        EnableConstraints;
        {
        if Trim(s) <> '' then
        begin
          GetGroupName(True);
          N15.Click;
        end;
        }
      end;

    end;
    IsAlwaysExpanded(False);
  end;
//  Self.Grid2.SetFocus;
//  GridLocateRecord(Self.tvView2,Self.tvGroupName.DataBinding.FieldName,VarToStr( Self.Edit1.Text ) ) 
end;

procedure TfrmCustomerPlate.RzBut3Click(Sender: TObject);
  function ModifyData(lpOriginalGroupName , lpPresentGroupName : string):Boolean;
  var
    i : Integer;
    lvFieldName : string;
  begin

    with Self.dsMaster do
    begin
      if State <> dsInactive then
      begin
        First;
        while not Eof do
        begin
          lvFieldName := Self.tvGroupName.DataBinding.FieldName;
          if FieldByName(lvFieldName).AsString =  lpOriginalGroupName then
          begin
            Edit;
            FieldByName(lvFieldName).Value := lpPresentGroupName;
          end;
          Next;
        end;

      end;

    end;
  end;
var
  s : string;
  
  lvRowCount : Integer;
  I: Integer;
  vValue , lvModifyName : Variant;
  Child : TfrmCustomerPost;
  lvIsFrozen : Boolean;
begin
  inherited;
  with Self.tvView2 do
  begin
    lvRowCount := Controller.SelectedRowCount;
    for I := 0 to lvRowCount-1 do
    begin
      if Controller.SelectedRows[i] is TcxGridGroupRow then begin
        vValue :=  Controller.SelectedRows[i].Values[tvGroupName.Index];
        Child := TfrmCustomerPost.Create(nil);
        try
          Child.cxTextEdit1.Text := VarToStr(vValue);
          Child.ShowModal;
          lvModifyName := Child.cxTextEdit1.Text;
        finally
        end;
        ModifyData(VarToStr(vValue) , lvModifyName);
      end else begin
        s := Controller.SelectedRows[i].Values[tvView2Col20.Index]; 
        lvIsFrozen := StrToBoolDef( VarToStr(s) , True );
        if lvIsFrozen then
        begin
          dwADO.ADOIsEdit(Self.dsMaster);
        end else
        begin
          Application.MessageBox('冻结数据禁止编辑！','提示：',MB_OK + MB_ICONINFORMATION);
        end;    
      //  KeyDown(13);
      end;
      Break;
    end;
    DataController.UpdateData;
  end;
end;

procedure TfrmCustomerPlate.RzBut4Click(Sender: TObject);
var
  lvNode : TTreeNode;

begin
  lvNode := Self.tv.Selected;
  if Assigned(lvNode) then
  begin
    if dwADO.UpdateTableData(g_Table_Decorate_OfferList,'NoId',0,Self.dsMaster) then
    begin
      ShowMessage('保存成功!');
      Self.dsMaster.MergeChangeLog;
      ReadData(dwNodeIndex);//读数据
    end else
    begin
      ShowMessage('保存失败');
    end;
  end;
end;

procedure TfrmCustomerPlate.RzBut5Click(Sender: TObject);
begin
  inherited;
  if MessageBox(handle, PChar('是否要删除该项目? 如需撤销本次删除可按 Ctrl+Z 恢复'),'提示',
     MB_ICONQUESTION + MB_YESNO + MB_DEFBUTTON2) = IDYES then
  begin
    Self.tvView2.DataController.DeleteSelection;
    GetGroupName(False);
    N15.Click;
  end;
end;

procedure TfrmCustomerPlate.RzBut6Click(Sender: TObject);
begin
  inherited;
  dslib.Close;
  dwTreeOffer := TNewUtilsTree.Create(Self.tv1,DM.ADOconn,g_Table_Maintain_QuotationTree,'报价库');
  dwTreeOffer.GetTreeTable(0);

  //  g_Table_sk_Decorate_OfferList    : string = 'sk_Decorate_OfferList'; //报价明细列表
end;

procedure TfrmCustomerPlate.RzBut8Click(Sender: TObject);
begin
  Close;
end;

function TfrmCustomerPlate.GetGridSummary(lpColIndex , lpIndex : Integer):Currency;
var
  I: Integer;
  c: Currency;
  s: string;
  lvTaxGold : string;
  lvDesignCost:string;
  lvOfferType:Integer;
begin
  Result := 0;
  with Self.tvView2.ViewData do
  begin
    for I := 0 to RowCount-1 do
    begin
      if Rows[i] is TcxGridGroupRow then
      begin
      //  Self.mmo1.Lines.Add( VarToStr( Rows[i].Values[ lpColIndex ] ) );
      end else
      begin

        case lpIndex of
          0: Result := Result + StrToCurrDef(VarToStr( Rows[i].Values[ lpColIndex ] ) , 0); //汇总总额
          1:
           begin
              s := VarToStr(Rows[i].Values[tvGroupName.Index]);
              lvTaxGold := VarToStr(Rows[i].Values[tvProjectName.Index]);
              if (s = dwExpensesName) and (lvTaxGold = g_AutoCalcParameter[0] ) then
              begin
                Result := StrToCurrDef(VarToStr( Rows[i].Values[ lpColIndex ] ) , 0);
                Break;
              end;
           end;
           2:
           begin
             s := VarToStr(Rows[i].Values[tvView2Column3.Index]);
             if StrToIntDef(s,0) <> 2 then  //不含主材
               Result := Result + StrToCurrDef(VarToStr( Rows[i].Values[ lpColIndex ] ) , 0); //汇总总额

           end;
           3:
           begin
             s := VarToStr(Rows[i].Values[tvView2Column3.Index]);
             if StrToIntDef(s,0) = 3 then  //人工组
               Result := Result + StrToCurrDef(VarToStr( Rows[i].Values[ lpColIndex ] ) , 0); //汇总总额

           end;
           4:
           begin
             lvDesignCost := VarToStr(Rows[i].Values[tvProjectName.Index]);
             s := VarToStr(Rows[i].Values[tvGroupName.Index]);
             if (s = dwExpensesName) and (SameText( lvDesignCost , '设计费') ) then
             begin
               Result := StrToCurrDef(VarToStr( Rows[i].Values[ lpColIndex ] ) , 0);
               Break;
             end;
           end;
        end;

      end;

    end;
  end;

end;


procedure TfrmCustomerPlate.RzButClick(Sender: TObject);
var
  Child : TfrmCustomerSummaryU;
  lvManpowerSummary  : Currency;
  a : Currency;
  b : Currency;
  lvTaxGold : Currency;  //税金

  lvTmpASummary , lvTmpBSummary ,  lvTmpCSummary : Currency;
  lvWholeSummary : Currency;
begin
  inherited;
  Child := TfrmCustomerSummaryU.Create(Application);
  try
    IsAlwaysExpanded(True);
    lvManpowerSummary     := SQLTotal( GroupSQLText( tvView2Column2.DataBinding.FieldName,3));
    a := GetGridSummary(Self.tvView2Col24.Index,0); // 综合组折前
    b := GetGridSummary(Self.tvView2Col25.Index,0);
    lvTaxGold := GetGridSummary(tvView2Col25.Index,1); //税金
  //  Self.mmo1.Lines.Add('A:' + CurrToStr(A) + ' B:' + CurrToStr(B) + ' lvTaxGold:' + CurrToStr(lvTaxGold));
  //  dwDirectFee := lvSummary;
  //  dwExtrasSummary //费用

  //  汇总合计 = 折后 + 费用组 + 人工组

  //  优惠前合计= 综合组折前+ 费用组 + 人工组

  //  优惠合计 = 优惠前合计- 汇总合计

    Child.dxGroupname.Active := True;
    Child.dxGroupname.Edit;
    lvWholeSummary:= (B  - dwExtrasSummary ) / 100 * dwWholeListdiscount;
    lvTmpASummary := lvWholeSummary + dwExtrasSummary + lvManpowerSummary;

    Child.dxGroupnameASmall.Value := lvTmpASummary;
    Child.dxGroupnameALarge.Value := MoneyConvert( lvTmpASummary );

    lvTmpBSummary := A + lvManpowerSummary; //+ dwExtrasSummary
    Child.dxGroupnameBSmall.Value := lvTmpBSummary;
    Child.dxGroupnameBLarge.Value := MoneyConvert(lvTmpBSummary);
    //优惠合计
    lvTmpCSummary := lvTmpBSummary - lvTmpASummary;
    Child.dxGroupnameCSmall.Value := lvTmpCSummary;
    Child.dxGroupnameCLarge.Value := MoneyConvert(lvTmpCSummary);
    //明细列表
    Child.dxGroupnameManpowerSmall.Value := lvManpowerSummary;  //人工
    Child.dxGroupnameTaxGoldFront.Value  := Child.dxGroupnameASmall.Value - lvTaxGold ;

    Child.dxGroupnameDirectTotal.Value   := lvWholeSummary + lvManpowerSummary;
  //  Child.dxGroupnameExtrasSmall.Value   := dwExtrasSummary;
    Child.dxGroupnameExtras.Value := Format('%2.2m , 税金:%2.2m',
    [Roundto( dwExtrasSummary ,-2 ), Roundto( lvTaxGold , -2 )]);
    Child.dxGroupnameColligate.Value:= Format('折前:%2.2m , 折后:%2.2m , 整单折后:%2.2m',
    [a - dwExtrasSummary,b - dwExtrasSummary , lvWholeSummary]);
    IsAlwaysExpanded(False);
    Child.ShowModal;
  finally
    Child.Free;
  end;
end;

procedure TfrmCustomerPlate.RzInfoResize(Sender: TObject);
var
  t : Double;
  a : Integer;

begin
  inherited;
  a := Self.RzInfo.Height - Self.cxButton1.Height - 200;
  Self.cxGroupBox1.Left := 40;
  Self.cxGroupBox1.Top  := 40;
  Self.cxGroupBox1.Height := a;
  t := Self.RzInfo.Height- Self.cxGroupBox1.Height;
  Self.cxButton1.Top   := Round(Self.cxGroupBox1.Height +  t  / 2 - 14);
  Self.cxButton1.Left  := Self.cxGroupBox1.Width + 50 - Round( cxButton1.Width / 2 );

  Self.cxGroupBox2.Left := Self.cxGroupBox1.Width + 60;
  Self.cxGroupBox2.Top  := 40;
  Self.cxGroupBox2.Height := a;

end;

procedure TfrmCustomerPlate.RzToolButton18Click(Sender: TObject);

  function InDeleteData(ATable,code : string):Integer;stdcall;
  var
    s : string;
  begin

    if Length(ATable) <>  0 then
    begin
      s := 'delete * from '+ ATable +' where tages in('+ Code  +')';
      with  DM.Qry do
      begin
        Close;
        SQL.Clear;
        SQL.Text := s;
        Result   := ExecSQL;
      end;

    end;

  end;
var
  Node : TTreeNode;
  szCodeList : string;

begin
  if Sender = Self.RzToolButton18 then
  begin
    TreeAddName(Self.tv);
  end else
  if Sender = Self.RzToolButton19 then
  begin
    Self.tv.Selected.EditText;
  end else
  if Sender = Self.RzToolButton20 then
  begin
  
    if MessageBox(handle, '是否删除报价分类节点？', '提示', MB_ICONQUESTION + MB_YESNO) = IDYES then
    begin
      Node := Self.Tv.Selected;
      if Assigned(Node) then
      begin
        if Node.Level > 1 then
        begin
          Self.dsClientInfo.Close;
          Self.dsMaster.Close;
          Self.dxGroupName.Active := False;

          GetIndexList(Node,szCodeList);
          InDeleteData( g_Table_Decorate_OfferList,szCodeList);
          InDeleteData( g_Table_Decorate_OfferInfo,szCodeList);
          InDeleteData( g_Table_Decorate_OfferSumCheck,szCodeList);

          dwTreeView.DeleteTree(Node);

        //  ReadData(dwNodeIndex);//读数据
          tvDblClick(Sender);
        end else
        begin
          MessageBox(handle, '往来单位节点禁止删除？', '提示', MB_ICONQUESTION + MB_YESNO) 
        end;  
      end;
      
    end;

  end;

end;

procedure  TfrmCustomerPlate.SaveInfo();
var
  lvSQLText : string;
  ErrorCount : Integer;
begin
  lvSQLText := dsClientInfo.CommandText;
  DM.ADOQuery1.Close;
  DM.ADOQuery1.SQL.Text := lvSQLText;

  with dsClientInfo do
  begin
    if State in [DsEdit, DSInsert] then Post;

    if ChangeCount > 0 then
    begin
      DM.DataSetProvider1.ApplyUpdates(Delta,0,ErrorCount);
      MergeChangeLog;
    end;
  end;
end;

procedure TfrmCustomerPlate.RzToolButton2Click(Sender: TObject);
begin
  inherited;
  SaveInfo();
  RzClient.ActivePage := RzCustomer;
  ReadData(dwNodeIndex);//读数据
end;

procedure TfrmCustomerPlate.RzToolButton4Click(Sender: TObject);
var
  lvProjectName , lvSpec : string;
begin
  lvProjectName := Self.cxLookupComboBox1.Text;
  lvSpec := Self.cxLookupComboBox3.Text;
  with Self.dslib do
  begin
    if State <> dsInactive then
    begin
      Filtered := False;
    //  Filter := 'ProjectName ='''+ lvProjectName +'''' + '; Spec="' + lvSpec +'"';
      Filter := 'ProjectName Like ''%'+ lvProjectName +'%''' + '; Spec like ''%' + lvSpec +'%''';
      Filtered := true;
    end;
  end;

end;

procedure TfrmCustomerPlate.RzToolButton5Click(Sender: TObject);
var
  s0 : string;
  s1 : string;
  lvSQLText : string;
  lvTages : string;
  lvFieldName : string;

begin
  IsAlwaysExpanded(True) ;
  s0 := Self.cxLookupComboBox2.Text;
  lvFieldName := cxLookupComboBox2.Properties.KeyFieldNames;
  lvTages := 'tages=' + IntToStr(dwNodeIndex);
  lvSQLText := 'Select * from '+ g_Table_Decorate_OfferList +
               ' WHERE ' + lvTages + ' AND OfferType=' + IntToStr(dwOfferType) + ' AND ' + lvFieldName + '="' + s0 + '"'; // + ' order by iRec'
  dwADO.OpenSQL(Self.dsMaster,lvSQLText);
  GetGroupName(True); //整理分组
  IsAlwaysExpanded(False) ;
end;

procedure TfrmCustomerPlate.tv1Click(Sender: TObject);
var
  PData: PNodeData;
  lvNode : TTreeNode;
  lvSQLText : string;
  tag:Integer;
  lvFieldName : string;
begin
  inherited;
  Self.dslib.Close;
  lvNode:=tv1.Selected;
  if Assigned(lvNode) then
  begin
    if lvNode.Level >= 1 then
    begin
      PData := PNodeData(lvNode.Data);
      if Assigned(PData) then
      begin
        tag := PData^.Index;
      //  dwNodeIndex := PData^.Index;
      //  (PData^.Index);//读数据
        //报价库
        lvSQLText := 'Select * from '+ g_Table_Maintain_QuotationList +
                     ' WHERE Sign_Id=' + IntToStr(PData^.Index);// + szSearchText;
        dslib.Filtered := False;
        dslib.Filter := '';
        dwADO.OpenSQL(Self.dslib,lvSQLText);
         {
           1 = 主材报价
           2 = 费用报价
           3 = 综合报价
           4 = 人工报价
         }

        with Self.tvView3.DataController.DataSet do
        begin

           case FieldByName('tage').AsInteger of
             1:    lvFieldName := 'MainMaterialPrice';// = 主材单价
             2..3: lvFieldName := 'SyntheticalPrice';//  = 综合单价
             4: lvFieldName := 'ManpowerPrice'; // = 人工报价
           end;
           tvView3Column2.DataBinding.FieldName := lvFieldName;
        end;

      end;

    end;

  end;

end;

procedure TfrmCustomerPlate.tvDblClick(Sender: TObject);

  procedure GetOfferInfo();
  var lvSQLText : string;
    lvCustomerName : Variant;
    lvFieldName : string;
    lvCustomerphone: Variant;

  begin
    lvSQLText := 'Select * from ' + g_Table_Decorate_OfferInfo + ' Where tages=' + IntToStr(dwNodeIndex);
    dwADO.OpenSQL(dsClientInfo,lvSQLText);
    dsClientInfo.CommandText := lvSQLText;
    with dsClientInfo do
    begin
      if State <> dsInactive then
      begin

        if RecordCount = 0 then
        begin
          First;
          Insert;
        end else
          Edit;

        lvFieldName := cxDBTextEdit1.DataBinding.DataField;
        lvCustomerphone := cxDBSpinEdit2.DataBinding.DataField;
        if Length(dwTopLevelName) <> 0 then
        begin
          FieldByName(lvFieldName).Value     := dwTopLevelName;
        end;

        if dwCustomerPhoneNumber <> Null then
        begin
          FieldByName(lvCustomerphone).Value := dwCustomerPhoneNumber;
        end;

        Post;
        dwWholeListdiscount := FieldByName(cxDBSpinEdit7.DataBinding.DataField).AsInteger;
        dwAcreage := FieldByName(cxDBSpinEdit1.DataBinding.DataField).AsFloat;

      end;

    end; 
    Self.cxButton1.Enabled := True;    
  end;

var
  PData: PNodeData;
  lvNode : TTreeNode;
  lvSQLText : string;
begin
  inherited;
  lvNode:=tv.Selected;
  if Assigned(lvNode) then
  begin
    dwTopLevelName := GetTreeLevelName(lvNode);
    if lvNode.Level > 1 then
    begin
    
      PData := PNodeData(lvNode.Data);
      if Assigned(PData) then
      begin
      //  Edit1.Text := IntToStr(dwNodeIndex) + ' - ' + IntToStr( PData^.Index );
        if (dwNodeIndex <> 0) and ( dwNodeIndex <> PData^.Index) then
        begin
        
          if Application.MessageBox('是否切换工程','提示:',MB_YESNO + MB_ICONEXCLAMATION) = IDNO then
          begin
            Exit;
          end; 
          
        end;  
        
        dwNodeIndex := PData^.Index;
        Self.tvView2.Bands[0].Caption := PData.SignName;
        if RzClient.ActivePage = RzInfo then
        begin
          GetOfferInfo;
        end else
        if RzClient.ActivePage = RzCustomer then
        begin
          GetOfferInfo;
          ReadData(dwNodeIndex);//读数据
        end;

      end;

    end;

  end;

end;

procedure TfrmCustomerPlate.tvEdited(Sender: TObject; Node: TTreeNode;
  var S: string);
begin
  IstreeEdit(s,Node);
end;

procedure TfrmCustomerPlate.tvGroupIndexGetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
var
  vIndex : Integer;

  s: Variant;
  i: Integer;
  vExIst : Boolean;
begin
  i := 0;
  {
  vIndex := tvSpaceName.Index;
  with Self.tvView2.ViewData do
  begin
    i := 0;
    for I := 0 to RowCount-1 do
    begin
      vExIst := False;
      if ARecord.Values[vIndex] = Records[i].Values[vIndex] then
      begin

        vExIst := True;
        Break;
      end;

    end;
  end;
  if vExIst=False then i := 0;
  }
  with ARecord do
  begin
    if IsData then
    begin
      AText := IntToStr(Index - (ParentRecord.Index + i));
    end;
  end;

//  AText := IntToStr( TcxCustomGridRow(ARecord).Index );
//  AText := IntToStr( TcxGridGroupRow( ARecord ).Index );
  {
  with AViewInfo, GridRecord do
  begin
    if not IsData or (Item.Index <> tvGroupIndex.Index) then
      Exit;
    ACanvas.DrawTexT(inttostr(RecordIndex - ParentRecord.RecordIndex),Bounds, cxAlignHCenter or cxAlignVCenter);
    ADone := true;
  end;
  }
end;

procedure TfrmCustomerPlate.tvGroupNameCompareRowValuesForCellMerging(
  Sender: TcxGridColumn; ARow1: TcxGridDataRow;
  AProperties1: TcxCustomEditProperties; const AValue1: Variant;
  ARow2: TcxGridDataRow; AProperties2: TcxCustomEditProperties;
  const AValue2: Variant; var AAreEqual: Boolean);
begin
  inherited;
  {
  if ARow1.Values[DBView1SYDID.Index] = ARow2.Values[DBView1SYDID.Index] then
    AAreEqual := True
  else
    AAreEqual := False;
  }

  AAreEqual := (AValue1 = AValue2) and (AValue2 <> dwExpensesName) ; //dwExpensesName
//  AAreEqual := (AValue1 = AValue2) and (AValue2 <> '主材') ;
end;

procedure TfrmCustomerPlate.tvGroupNameCustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  FBounds: TRect;
  sGroupName: string;            //分组行显示的标题
  i, iChildCount: Integer;
begin
  {
  iChildCount := 0;

  //获得当前分组的下级数量
  for i := 0 to Self.tvView2.DataController.RecordCount - 1 do
  begin
    if VarToStr(AViewInfo.GridRecord.Values[0]) = VarToStr(Self.tvView2.DataController.Values[i, 0]) then
    begin
      inc(iChildCount);
      sGroupName := Self.tvView2.DataController.Values[i, 1];      //取type列的名字作为分组行标题的一部分
    end;
  end;
  //格式化分组行
  sGroupName := Format('>> %s:%s(%d)%s', [
    Self.tvView2.GroupedColumns[0].Caption,
    AViewInfo.GridRecord.Values[0],
    iChildCount,
    sGroupName
  ]);

  //打印分组行
  FBounds := AViewInfo.Bounds;
  ACanvas.FillRect(FBounds);
  OffsetRect(FBounds, 25, 0);

  ACanvas.Font.Style := [fsBold];
  ACanvas.DrawTexT(sGroupName, FBounds, cxAlignLeft or cxAlignVCenter or cxDontClip);
  ADone := True;
  }
  {
  AGroupTask := TCustomTaskDTO(GetGroupNameTaskAddress(AViewInfo.GridRecord.Values[0]));

  iTaskChargeItemsCount := 0;
  for i := 0 to tbvwChargeItemList.DataController.RecordCount - 1 do
  if GetTaskDTO(i) = AGroupTask then
  iTaskChargeItemsCount := iTaskChargeItemsCount + tbvwChargeItemList.DataController.Values[i, tbvwCILCol_Count.Index];

  FBounds := AViewInfo.Bounds;
  ACanvas.FillRect(FBounds);
  OffsetRect(FBounds, 25, 0);

  sGroupName := GetGroupNameWithoutExtraInfo(AViewInfo.GridRecord.Values[0]) + ' [' + IntToStr(iTaskChargeItemsCount) + ']';

  ACanvas.Font.Style := [fsBold];
  ACanvas.Font.Color := clGreen;
  ACanvas.DrawTexT('>> ' + sGroupName, FBounds, cxAlignLeft or cxAlignVCenter or cxDontClip);
  ADone := True;
  }
end;

procedure TfrmCustomerPlate.tvGroupNameGetDataText(
  Sender: TcxCustomGridTableItem; ARecordIndex: Integer; var AText: string);
var
  s: string;
begin
  if (Length(AText) <> 0) and (dwIsExcel = False) then
  begin
    s := AText;// tvView2.DataController.Values[ARecordIndex, 0];
    if s = '费用' then
      AText := '主重中之重'
    //  AText := s
    else
      AText := s;
  end;
end;

procedure TfrmCustomerPlate.tvGroupNameGetDisplayText(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  var AText: string);
begin
  inherited;
  if Length(AText) <> 0 then
  begin
    if pos('重',AText) > 0 then
    begin
      AText := '费用'
    end;
  end;
end;

procedure TfrmCustomerPlate.tvKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  case Key of
    13:
    begin
      Self.tv.Selected.Parent.Selected := True;
      Self.RzToolButton18.Click;
    end;
    46:Self.RzToolButton20.Click;
    38: N21.Click;//KeyUp ;
    40: N22.Click;//KeyDown;
  end;
end;

function TfrmCustomerPlate.DoAutoCalcValidate(const AValue: Variant;
  var AErrorText: TCaption): Boolean;
begin
  Result := VarToStr(AValue) = '0';
  if Result then
    AErrorText := 'Please enter a value';
end;

function TfrmCustomerPlate.GetPersonFullName(ARecord: TcxCustomGridRecord): string;
var
  AFirstName, ALastName: string;
begin
  {
  AFirstName := VarToStr(ARecord.Values[cxGridTableViewColumnFirstName.Index]);
  ALastName  := VarToStr(ARecord.Values[cxGridTableViewColumnLastName.Index]);
  if (Trim(AFirstName) > '') and (Trim(ALastName) > '') then
    Result := Format('%s %s', [AFirstName, ALastName])
  else
    Result := AFirstName + ALastName;
  }
end;


procedure TfrmCustomerPlate.tvSpaceNameCompareRowValuesForCellMerging(
  Sender: TcxGridColumn; ARow1: TcxGridDataRow;
  AProperties1: TcxCustomEditProperties; const AValue1: Variant;
  ARow2: TcxGridDataRow; AProperties2: TcxCustomEditProperties;
  const AValue2: Variant; var AAreEqual: Boolean);
var
  lvCol : TcxGridDBBandedColumn;

begin
  inherited;
//  lvCol := Sender AS TcxGridDBBandedColumn;
//  lvCol.Index;
  {
  if VarToStr(AValue1) = '费用' then
    AAreEqual := False
  else
    AAreEqual := True;
  }
  {
  if ARow1.Values[DBView1SYDID.Index] = ARow2.Values[DBView1SYDID.Index] then

  else
    AAreEqual := False;
  }

end;

procedure TfrmCustomerPlate.tvView1DblClick(Sender: TObject);
var
  s : Variant;
begin
  s := Self.tvView1Col1.EditValue;
  GridBanbsLocateRecord(Self.tvView2,Self.tvGroupName.DataBinding.FieldName,VarToStr( s ) )
end;

procedure TfrmCustomerPlate.tvView2Col16PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);

begin
  inherited;
  //综合单价
  Self.tvView2Col16.EditValue := DisplayValue;
  CalculatData(Sender);
end;

procedure TfrmCustomerPlate.tvView2Col1PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmCompanyManage;
  lvSignName : Variant;

begin
  Child := TfrmCompanyManage.Create(Application);
  try
    Child.dwIsReadonly := True;
    Child.ShowModal;
    lvSignName :=  Child.dwSignName;
    tvView2Col1.EditValue := lvSignName;
  finally
    Child.Free;
  end;
end;

procedure TfrmCustomerPlate.tvView2Col22PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  //折扣率
  Self.tvView2Col22.EditValue := DisplayValue;
  CalculatData(Sender);
end;

procedure TfrmCustomerPlate.tvView2Col22StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  i : Currency;

begin
  inherited;
  {
  with AViewInfo, GridRecord do
  begin
  //  if not IsData or (Item.Index <> tvGroupIndex.Index) then

    if Item.Index = tvView2Col22.Index  then
    begin
      i := StrToCurrDef( Item.EditValue ,0);
      if ( i < 100) and ( i > 50 ) then
      begin
        ACanvas.Font.Color := clGray;
      end else
      if i < 50 then
      begin
        ACanvas.Font.Color := clRed;
      end;

    end;

  end;
  }
end;

procedure TfrmCustomerPlate.tvView2Col24PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  Self.tvView2Col24.EditValue := DisplayValue;
  Self.tvView2Col25.EditValue := DisplayValue;
end;

procedure TfrmCustomerPlate.tvView2Col24ValidateDrawValue(
  Sender: TcxCustomGridTableItem; ARecord: TcxCustomGridRecord;
  const AValue: Variant; AData: TcxEditValidateInfo);
var AErrorText : TCaption;
begin
  inherited;
  if ( VarToStr( ARecord.Values[tvGroupName.Index] ) = dwExpensesName) and
     ( ARecord.Values[tvAutoCalc.Index] = True) then
  begin
    AData.ErrorType := eetCustom;
    AData.ErrorIcon.Assign(icCustomIconListItem1.Picture.Bitmap);
    AData.ErrorText := AErrorText;
  end;
  {
  if DoAutoCalcValidate(AValue,AErrorText) then
  begin
    //警告图标
    AData.ErrorIcon.Assign(icCustomIcon1.Picture.Bitmap);
  end;
  }
end;

procedure TfrmCustomerPlate.tvView2Col3PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmWorkType;
begin
  Child := TfrmWorkType.Create(Application);
  try
    Child.dwIsReadOnly := True;
    Child.ShowModal;
    tvView2Col3.EditValue := Child.tvWorkCol2.EditValue;
  finally
    Child.Free;
  end;
end;

procedure TfrmCustomerPlate.tvView2Col4PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmCompanyManage;
begin
  Child := TfrmCompanyManage.Create(Application);
  try
    Child.dwIsReadonly := True;
    Child.ShowModal;
    tvView2Col4.EditValue := Child.dwSignName;
  finally
    Child.Free;
  end;
end;

procedure TfrmCustomerPlate.tvView2Col6PropertiesButtonClick(Sender: TObject;
  AButtonIndex: Integer);
var
  Child : TfrmCustomerAmount;
begin
  inherited;
  {
  Child := TfrmCustomerAmount.Create(Application);
  try
    Child.ShowModal;
  finally
    Child.Free;
  end; }
end;

//工程直接费 =

procedure TfrmCustomerPlate.tvView2Col6PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
var
  lvFieldName : string;
  lvCustomer  : Double;
  lvDisplayValue : Double;

begin
  //工程量
  lvDisplayValue := StrToFloatDef( VarToStr(DisplayValue) , 0 );

  lvFieldName := Self.tvView2Col6.DataBinding.FieldName;
  if lvFieldName = dwQuantities[0] then
  begin
    //客户工程量
  end;

  if lvFieldName = dwQuantities[1] then
  begin
    //实收工程量
    with Self.dsMaster do
    begin
      if State <> dsInactive then
      begin
        lvCustomer := FieldByName(dwQuantities[0]).AsFloat;
      end;
    end;

    if lvDisplayValue > lvCustomer then
    begin
      Application.MessageBox('实收工程量不得大于报价工程量','提示:',MB_OK + MB_ICONEXCLAMATION);

      DisplayValue := 0;
      Exit;
    end;
  end;
  //DisplayValue := DisplayValue;
  Self.tvView2Col6.EditValue := DisplayValue;
  CalculatData(Sender);
end;

procedure TfrmCustomerPlate.tvView2Col8PropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  inherited;
  Self.tvView2Col8.EditValue := DisplayValue;
  CalculatData(Sender);
end;

procedure TfrmCustomerPlate.tvView2CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
var
  i : Integer;
  AChildPosition : Integer;
  AChildDataGroupsCount : Integer;
  AChildDataGroupIndex  : Integer;
  lvGroupName : string;
begin
  {
  with TcxDataControllerGroups(AViewInfo.GridView.DataController.Groups) do
  begin

    for i := 1 to GroupingItemCount do
    begin
      AChildDataGroupsCount := ChildCount[i -2];
      for AChildPosition := 0 to AChildDataGroupsCount - 1 do
      begin
        AChildDataGroupIndex := ChildDataGroupIndex[i -2, AChildPosition]; //组Index
        lvGroupName := VarToStr( GroupValues[AChildDataGroupIndex] );
     //   Self.mmo1.Lines.Add(lvGroupName + ' GroupRow:' + IntToStr( AChildDataGroupsCount ));
        if lvGroupName = Trim( dwExpensesName ) then
        begin
          with AViewInfo.GridView.DataController.Summary do
          begin
          //  GroupSummaryValues[AChildDataGroupIndex , 0] := Null;

            GroupSummaryValues[AChildDataGroupIndex , 1] := Null;
            GroupSummaryValues[AChildDataGroupIndex , 3] := Null;
            GroupSummaryValues[AChildDataGroupIndex , 4] := Null;

          end;

        end;

      end;

    end;

  end;
  }
  {
  with AViewInfo, GridRecord do
  begin

    if not IsData or (Item.Index <> tvGroupIndex.Index) then
    begin

    end;
   {
    if not IsData or (Item.Index <> tvGroupIndex.Index) then
      Exit;
    }
  //end;
  //}

  //行颜色
  if AViewInfo.RecordViewInfo.Index   mod   2   =   0   then begin
    ACanvas.Canvas.Brush.Color   := RGB(148,232,249);
    ACanvas.Font.Color := clblack;
  end else begin
    ACanvas.Canvas.Brush.Color:= RGB(203,232,249);
    ACanvas.Font.Color := clblack;
  end;

  if AViewInfo.Selected then
  begin
    ACanvas.Canvas.Brush.Color := clmenuhighlight;
    ACanvas.Font.Color := clwhite;
  end;

//  ADone := True;
end;

procedure TfrmCustomerPlate.tvView2DataControllerSummaryAfterSummary(
  ASender: TcxDataSummary);
var
  i : Integer;
  AChildDataGroupsCount: Integer;
  AChildDataGroupIndex, AParentDataGroupIndex: TcxDataGroupIndex;
  AChildPosition: Integer;
  lvGroupName : string;
begin
  inherited;

  with TcxDataControllerGroups( tvView2.DataController.Groups ) do
  begin
    for i := 1 to GroupingItemCount do
    begin
      AChildDataGroupsCount := ChildCount[i -2];
      for AChildPosition := 0 to AChildDataGroupsCount - 1 do
      begin
        AChildDataGroupIndex := ChildDataGroupIndex[i -2, AChildPosition]; //组Index
        lvGroupName := VarToStr( GroupValues[AChildDataGroupIndex] );

        if lvGroupName = Trim( dwExpensesName ) then
        begin
          with tvView2.DataController.Summary do
          begin
            GroupSummaryValues[AChildDataGroupIndex , 0] := Null;
            GroupSummaryValues[AChildDataGroupIndex , 1] := Null;
            GroupSummaryValues[AChildDataGroupIndex , 3] := Null;
            GroupSummaryValues[AChildDataGroupIndex , 4] := Null;

          end;

        end;

      end;

    end;

  end;
  tmrClock.Enabled := True;
end;

procedure TfrmCustomerPlate.tvView2DblClick(Sender: TObject);
var
  i : Integer;
  s : Variant;
begin
  inherited;
  with Self.tvView2.Controller do
  begin
    for I := 0 to SelectedRowCount-1 do
    begin
      if SelectedRows[i] is TcxGridGroupRow then
      begin

      end else begin

        if  (VarToStr( SelectedRows[i].Values[tvGroupName.Index] ) = dwExpensesName )  then
        begin

          if StrToBoolDef( VarToStr( SelectedRows[i].Values[tvAutoCalc.Index] ) ,False ) then
          begin
            //自动计算区域
            if SelectedColumns[i].Index = tvView2Col24.Index then N27.Click;

            tvView2Col22.Options.Editing := False;
            tvView2Col6.Options.Editing  := False;
            tvView2Col5.Options.Editing  := False;

          end else
          begin

            if (SelectedColumns[i].Index = tvView2Col24.Index)  then tvView2Col24.Options.Editing := True;
            if (SelectedColumns[i].Index = tvProjectName.Index) then tvProjectName.Options.Editing:= True;
            tvView2Col22.Options.Editing := True;
            tvView2Col6.Options.Editing  := True;
            tvView2Col5.Options.Editing  := True;
          end;

        end else
        begin
          tvView2Col22.Options.Editing := True;
          tvView2Col6.Options.Editing  := True;
          tvView2Col5.Options.Editing  := True;
        end;

        Self.RzBut3.Click;
      end;
      Break;
    end;
  end;
end;

procedure TfrmCustomerPlate.tvView2Editing(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; var AAllow: Boolean);
//var
begin
  inherited;
  if dwOfferType = 0 then
    AAllow := False
  else
    dwADO.ADOIsEditing(Self.dsMaster,AAllow);

  if AAllow = False then
  begin
    tvView2Col24.Options.Editing := False;
  //  tvProjectName.Options.Editing= False;
  end;

end;

procedure TfrmCustomerPlate.tvView2EditKeyDown(Sender: TcxCustomGridTableView;
  AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin

    Self.dsMaster.Post;
    AEdit.EditModified := True;
  end;
end;

procedure TfrmCustomerPlate.tvView2GetCellHeight(Sender: TcxCustomGridTableView;
  ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
  ACellViewInfo: TcxGridTableDataCellViewInfo; var AHeight: Integer);
begin
  if AHeight < 26 then AHeight := 26;
end;

procedure TfrmCustomerPlate.tvView2KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if (Key = 46) and (dwOfferType <> 0) and (dwNucleusCount = False) then RzBut5.Click;
end;

procedure TfrmCustomerPlate.tvView2MouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
var
  AView: TcxGridTableView;
  AHitTest: TcxCustomGridHitTest;
  I: Integer;
  s : Variant;
begin
  // Note that the Sender parameter is a Site
  AView := (Sender as TcxGridSite).GridView as TcxGridTableView;
  AHitTest := AView.ViewInfo.GetHitTest(X, Y);

  // The point belongs to the [+]/[-] button area ?
  if (AHitTest is TcxGridExpandButtonHitTest) and
      not TcxGridExpandButtonHitTest(AHitTest).GridRecord.IsData and
      TcxGridGroupRow(TcxGridExpandButtonHitTest(AHitTest).GridRecord).Expanded then
  begin
    with AView.ViewData do
    begin
      for I := RowCount - 1 downto 0 do
      if (Rows[I] is TcxGridGroupRow) and TcxGridGroupRow(Rows[I]).Expanded and
          not (Rows[I] = TcxGridExpandButtonHitTest(AHitTest).GridRecord) then
      begin
        (Rows[I] as TcxGridGroupRow).Collapse(false);
      end;
    end;
  end;
  s := Self.tvGroupName.EditValue;
  GridLocateRecord(Self.tvView1,Self.tvView1Col1.DataBinding.FieldName,VarToStr( s ) ) ;

//  Self.mmo1.Lines.Add('X:' + IntToStr(vPoint.X) + ' y:' + IntToStr(vPoint.Y) + ' Index:' + IntToStr(vIndex));
end;

procedure TfrmCustomerPlate.tvView2MouseEnter(Sender: TObject);
var
  AView: TcxGridTableView;
  vPoint :TPoint; //用来存放坐标
  hw:HWND; //用来存放窗口句柄
  vIndex : Integer;
  vCount : Integer;
  i : Integer;

begin
  inherited;
  {
  GetCursorPos(vPoint);  //取得鼠标坐标,并存放进a中
  hw := WindowFromPoint(vPoint); //取得变量a 对应的 窗口句柄
  AView := (Sender as TcxGridSite).GridView as TcxGridTableView;
  with AView.Controller do
  begin
    vCount := SelectedColumnCount;
    if  vCount <> 0  then
    begin
      for I := 0 to vCount-1 do
      begin
        vIndex :=  SelectedColumns[i].Index;
        if vIndex = tvGroupIndex.Index then
        begin
          pm3.Popup( vPoint.X,  vPoint.Y);
        end;
        Self.mmo1.Lines.Add('Index:' + IntToStr(vIndex));
      end;
    end;
  end;
  }
end;

procedure TfrmCustomerPlate.tvView2StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  lvRowValue : Variant;

begin
  inherited;
  lvRowValue := ARecord.Values[tvView2Col20.Index];
  if lvRowValue<>null then
  begin
    if lvRowValue = False then
      AStyle := DM.cxStyle222;
  end;
end;

procedure TfrmCustomerPlate.tvView3DblClick(Sender: TObject);
  function GetChildRowCount():Integer;
  var
    i : Integer;
    GRowIndex , GRowID: Integer;
  begin
    with Self.tvView2.Controller,Self.tvView2.DataController do
    begin
      if SelectedRowCount = 0 then
      begin
        FocusedRowIndex := 0;
      end;
      for i := 0 to SelectedRowCount - 1 do
      begin
        //判断选择的是否为组的行
        if Self.tvView2.Controller.SelectedRows[i] is TcxGridGroupRow   then
        begin

        end;
      //  ShowMessage(IntToStr( TcxGridGroupRow(SelectedRows[i]).Index ));

        //取得组的行的index;
        GRowIndex := SelectedRows[i].Index;
        //取得组的行的ID；
        GRowID :=  Groups.DataGroupIndexByRowIndex[GRowIndex];
        //这里得到的是组的行的那个标题
      //  ShowMessage(Groups.GroupValues[GRowID]);
        //这里是组的行数
        Result := Groups.ChildCount[GRowID];
      //  ShowMessage(IntToStr( Groups.ChildCount[GRowID]));
      //  Self.tvView2.DataController.Groups.ChildRecordIndex
        Break;
      end;
    end;
  end;
  function SetRowIndex(lpFileName : string):Boolean;
  var
    s: string;
    i:Integer;
    iRec : Integer;
    lvIndex : Integer;
    lvIsUpdate : Boolean;
    t:Integer;
    j: Integer;
    lvWhereRow: Integer;

  begin
    with Self.tvView2 do
    begin
      iRec := 0;
      lvWhereRow := 0;
      lvIndex := tvGroupIndex.Index;
      lvIsUpdate := False;
      t:=GetCurrentTime;
      for i := 0 to ViewData.RecordCount-1 do
      begin

        s := ViewData.Records[i].Values[tvGroupName.Index];
        if ( s  = lpFileName ) then
        begin
          Application.ProcessMessages;
          s := Trim( VarToStr(ViewData.Records[i].Values[lvIndex]) );

          if (Self.dsMaster.State <> dsEdit) and (Self.dsMaster.State <> dsInsert) then
             Self.dsMaster.Edit;
          DataController.BeginUpdate;
          Self.dsMaster.DisableControls;
          Self.dsMaster.FieldByName('iRec').Value := iRec;
          Self.dsMaster.EnableControls;
          lvIsUpdate := True;
          Self.dsMaster.Next;
          DataController.EndUpdate;
          Inc(iRec);
        end;
      end;

    end;

  end;
var
  s : string;
  lvSpaceName : string;
  lvGroupName : Variant;
  lvRowIndex  : Integer;
  lvChildCount : Integer;
  i: Integer;
  lvNode : TTreeNode;
  lvData: PNodeData;
  DD1 : Variant;
  DD2 : Variant;
  DD3 : Variant;
  DD4 : Variant;
  DD5 : Variant;
  DD6 : Variant;
  DD7 : Variant;
  DD8 : Variant;
  DD9 : Variant;
  DD10: Variant;
  DD11: Variant;

  lvTage: Integer;
  lvCount : Integer;
begin
  inherited;
  if RzClient.ActivePage = RzCustomer then
  begin
    if Self.tvView3.Controller.FocusedRowIndex >= 0 then
    begin

      lvNode := tv1.Selected;
      if Assigned(lvNode) then
      begin
        lvData := PNodeData(lvNode.Data);
        if Assigned(lvData) then
        begin
          lvSpaceName := lvData.SignName;
           with Self.tvView3.DataController.DataSet do
           begin
             lvTage := FieldByName('tage').AsInteger;
             {
               1 = 主材报价
               2 = 费用报价
               3 = 综合报价
               4 = 人工报价
             }
             DD1 := FieldByName(tvProjectName.DataBinding.FieldName).Value;  //项目名称
             DD2 := FieldByName(tvView2Col32.DataBinding.FieldName).Value ;  //施工工艺
             DD3 := FieldByName(tvView2Col5.DataBinding.FieldName).Value;    //单位

             case dwOfferType of
               1: DD4 := FieldByName(tvView2Col16.DataBinding.FieldName).Value;   //综合单价
               2: DD6 := FieldByName(tvView2Col8.DataBinding.FieldName).Value;    //主材报价
               3: DD8 := FieldByName(tvView2Column1.DataBinding.FieldName).Value; //人工报价
             end;

             DD5 := FieldByName(tvView2Col9.DataBinding.FieldName ).Value; //人工单价
             DD7 := FieldByName(tvView2Col10.DataBinding.FieldName).Value; //材料成本
             DD9 := FieldByName(tvView2Col11.DataBinding.FieldName).Value; //成本单价
             DD10:= FieldByName(tvView2Col2.DataBinding.FieldName).Value;  //规格
             DD11:= FieldByName(tvView2Col7.DataBinding.FieldName).Value;  //品牌
           end;

           Self.Grid2.SetFocus;
           lvGroupName := tvGroupName.EditValue; //选择小组
           if lvTage = 2 then
           begin
             lvGroupName := dwExpensesName; //选中报价库等于费用
             lvSpaceName := dwExpensesName; //
           end;
           if (lvTage <> 2) and ( lvGroupName = dwExpensesName) then
           begin
             Application.MessageBox(PWideChar( lvSpaceName +  ' 项目不可以添加到费用组中..' ),'提示:',MB_OK + MB_ICONEXCLAMATION);
             Exit;
           end else
           begin
             case dwOfferType of
               1:
               begin
                 //客户报价  = 综合报价
                 if (lvTage <> 3) and (lvTage <> 2) then
                 begin
                   Exit;
                 end;
               end;
               2:
               begin
                 //主材报价
                 if lvTage <> 1 then
                 begin
                   Exit;
                 end;
               end;
               3:
               begin
                 //人工报价
                 if lvTage <> 4 then
                 begin
                   Exit;
                 end;
               end;
             end;
           end;

           if (dwOfferType = 1) or (dwOfferType = 2)  or (dwOfferType = 3) then
           begin
              dwGroupName := VarToStr(lvGroupName);
              if lvGroupName <> null then
              begin
                //定位
                {
                with Self.tvView2 do
                begin
                  for I := 0 to ViewData.RecordCount -1 do
                  begin
                    if ( VarToStr( ViewData.Records[i].Values[ Self.tvGroupName.Index] ) = dwGroupName)  and
                       ( VarToStr( ViewData.Records[i].Values[ Self.tvSpaceName.Index] ) = VarToStr( lvSpaceName )) then
                    begin
                      Controller.FocusedRowIndex := i;
                    //  ShowMessage('i:' + IntToStr(i));
                      Break;
                    end;
                  end;
                end;
                }
                IsAlwaysExpanded(True);
                  with Self.tvView2.DataController do
                  begin
                    if DataSet.State <> dsInactive then
                    begin
                      DataSet.DisableControls;
                      if Self.tvSpaceName.EditValue <> null then
                      //  Append
                        Insert
                      else
                        Edit;

                      DataSet.FieldByName(tvSpaceName.DataBinding.FieldName).Value   := Trim( lvSpaceName );
                      DataSet.FieldByName(tvProjectName.DataBinding.FieldName).Value := DD1;
                      DataSet.FieldByName(tvView2Col32.DataBinding.FieldName).Value  := DD2;
                      DataSet.FieldByName(tvView2Col5.DataBinding.FieldName).Value   := DD3;
                      DataSet.FieldByName(tvView2Col16.DataBinding.FieldName).Value  := DD4;
                      DataSet.FieldByName(tvView2Col8.DataBinding.FieldName).Value   := DD6;
                      DataSet.FieldByName(tvView2Col9.DataBinding.FieldName).Value   := DD5;
                      DataSet.FieldByName(tvView2Col10.DataBinding.FieldName).Value  := DD7;
                      DataSet.FieldByName(tvView2Column1.DataBinding.FieldName).Value:= DD8;
                      DataSet.FieldByName(tvView2Col11.DataBinding.FieldName).Value  := DD9;
                      DataSet.FieldByName(tvView2Col2.DataBinding.FieldName).Value   := DD10;
                      DataSet.FieldByName(tvView2Col7.DataBinding.FieldName).Value   := DD11;
                      if lvGroupName = dwExpensesName then
                        lvCount := 1
                      else
                        lvCount := 0;
                      DataSet.FieldByName(tvView2Col6.DataBinding.FieldName).Value := lvCount;
                      Post();
                      DataSet.EnableControls;
                    end;

                  end;
                IsAlwaysExpanded(False);
              //  N15.Click;
              //  if lvGroupName = dwExpensesName then CalculatData(Sender);
              end else begin
                ShowMessage('请选择需要添加的小组');
              end;

           end;

        end;

      end;

    end;
  end;

end;

procedure TfrmCustomerPlate.tvView4CustomDrawCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableDataCellViewInfo; var ADone: Boolean);
begin
//  mmo1.Lines.Add(VarToStr( AViewInfo.Value )) ;
//  iRec := 0;
  {
  //获得当前分组的下级数量
  for i := 0 to tvView2.DataController.RecordCount - 1 do
  begin
    AViewInfo.Value
    if VarToStr(ARecord.Values[tvView2Column36.Index]) = VarToStr(tvView2.DataController.Values[i, 0]) then
      inc(iRec);
  end;
  mmo1.Lines.Add( IntToStr(iRec) ) ;
//  AText := IntToStr(iRec);
  }
end;

procedure TfrmCustomerPlate.tvView4GetCellHeight(Sender: TcxCustomGridTableView;
  ARecord: TcxCustomGridRecord; AItem: TcxCustomGridTableItem;
  ACellViewInfo: TcxGridTableDataCellViewInfo; var AHeight: Integer);
begin
  if AHeight < 26 then AHeight := 26;
end;

procedure TfrmCustomerPlate.tvView4CustomDrawGroupCell(
  Sender: TcxCustomGridTableView; ACanvas: TcxCanvas;
  AViewInfo: TcxGridTableCellViewInfo; var ADone: Boolean);
var
  FBounds: TRect;
  sGroupName: string;            //分组行显示的标题
  i, iChildCount: Integer;
  s : Variant;
begin
  {
  iChildCount := 0;
  //获得当前分组的下级数量
  for i := 0 to tvView2.DataController.RecordCount - 1 do
  begin
  //  mmo1.Lines.Add(VarToStr(AViewInfo.GridRecord.Values[0]) + ' - ' + VarToStr(tvView2.DataController.Values[i, 0]));
    if VarToStr(AViewInfo.GridRecord.Values[0]) = VarToStr(tvView2.DataController.Values[i, 0]) then
    begin
      inc(iChildCount);
      sGroupName := tvView2.DataController.Values[i, 0];      //取type列的名字作为分组行标题的一部分
    end;
  end;

  mmo1.Lines.Add(AViewInfo.Text + ' - ' + IntToStr(iChildCount));
//  Self.tvView2Column1.EditValue := iChildCount;
  //格式化分组行
  sGroupName := Format('>> %s:%s(%d)% s', [
    tvView2.GroupedColumns[0].Caption,
    AViewInfo.GridRecord.Values[0],
    iChildCount,
    sGroupName
  ]);
  //打印分组行
  FBounds := AViewInfo.Bounds;
  ACanvas.FillRect(FBounds);
  OffsetRect(FBounds, 25, 0);
  s := tvView2Column36.EditValue ;
  if s <> null then
  begin
    sGroupName := VarToStr( s );
  end;
  ACanvas.Font.Style := [fsBold];
  ACanvas.DrawTexT(sGroupName, FBounds, cxAlignLeft or cxAlignVCenter or cxDontClip);
  ADone := True;
  }
end;

end.
