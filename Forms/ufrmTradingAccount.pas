unit ufrmTradingAccount;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, RzPanel, StdCtrls, RzEdit, ComCtrls, RzButton, Mask, RzBckgnd,
  ExtCtrls, RzTabs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  cxTextEdit, cxDBEdit, cxCurrencyEdit, cxMaskEdit, cxDropDownEdit, cxCalendar,
  cxMemo, DB, ADODB, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, dxSkinVS2010,
  dxSkinWhiteprint;

type
  TfrmAccountdetails = class(TForm)
    RzPageControl1: TRzPageControl;
    TabSheet1: TRzTabSheet;
    Panel9: TPanel;
    Label13: TLabel;
    Label14: TLabel;
    Label15: TLabel;
    Label16: TLabel;
    Label17: TLabel;
    Label18: TLabel;
    Label21: TLabel;
    Label9: TLabel;
    Label25: TLabel;
    ComboBox2: TComboBox;
    RzEdit12: TRzEdit;
    RzBitBtn1: TRzBitBtn;
    RzBitBtn17: TRzBitBtn;
    RzBitBtn6: TRzBitBtn;
    RzEdit3: TRzEdit;
    AddEndDate: TDateTimePicker;
    AddStartDate: TDateTimePicker;
    RzMemo2: TRzMemo;
    TabSheet2: TRzTabSheet;
    Panel2: TPanel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label11: TLabel;
    RzBitBtn2: TRzBitBtn;
    RzBitBtn3: TRzBitBtn;
    RzBitBtn4: TRzBitBtn;
    RzPanel3: TRzPanel;
    cxDBComboBox1: TcxDBComboBox;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBTextEdit2: TcxDBTextEdit;
    cxDBDateEdit1: TcxDBDateEdit;
    cxDBMemo1: TcxDBMemo;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    Label1: TLabel;
    cxCurrencyEdit1: TcxCurrencyEdit;
    Label8: TLabel;
    Label10: TLabel;
    cxCurrencyEdit2: TcxCurrencyEdit;
    procedure RzBitBtn17Click(Sender: TObject);
    procedure RzBitBtn3Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RzBitBtn6Click(Sender: TObject);
    procedure RzBitBtn1Click(Sender: TObject);
    procedure RzEdit5KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormActivate(Sender: TObject);
    procedure cxCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure cxDBCurrencyEdit1PropertiesChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    { Private declarations }
    function GetEnclosurePath():string;
  public
    { Public declarations }
    g_ProjectDir : string;
    g_PostCode   : string;
    g_PostNumbers : string;
  end;

var
  frmAccountdetails: TfrmAccountdetails;
  g_TableName : string = 'expenditure';

implementation

uses
  global,uDataModule,ufrmManage;

{$R *.dfm}


function TfrmAccountdetails.GetEnclosurePath():string;
begin
  //打开目录
  Result := Concat(g_ProjectDir,'\' + g_PostNumbers);
end;

procedure TfrmAccountdetails.cxCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.Label8.Caption := MoneyConvert( Self.cxCurrencyEdit1.Value );
end;

procedure TfrmAccountdetails.cxDBCurrencyEdit1PropertiesChange(Sender: TObject);
begin
  Self.Label10.Caption := MoneyConvert( Self.cxDBCurrencyEdit1.Value );
end;

procedure TfrmAccountdetails.FormActivate(Sender: TObject);
var
  s : string;
  i : Integer;
begin
  case Self.RzPageControl1.ActivePageIndex of
    0:
    begin
      g_PostNumbers := DM.getDataMaxDate(g_TableName);
    end;
    1:
    begin

      with Self.ADOQuery1 do
      begin
        Close;
        SQL.Clear;
        SQL.Text := 'Select * from '+ g_TableName +' where Numbers ="'+ g_PostNumbers + '"';
        Open;
      end;

    end;    
  end;

end;

procedure TfrmAccountdetails.FormCreate(Sender: TObject);
var
  i : Integer;

begin
  Self.Position := poScreenCenter;
  for i := 0 to Self.RzPageControl1.PageCount - 1 do Self.RzPageControl1.Pages[i].TabVisible := False;//隐藏

  AddStartDate.Date := Date;
  AddEndDate.Date   := Date;
  Self.Label8.Caption := '';
  Self.Label10.Caption:= '';
end;

procedure TfrmAccountdetails.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then Close;
  
end;

procedure TfrmAccountdetails.FormShow(Sender: TObject);
var
  s : string;
begin
  with DM.Qry do
  begin
    Close;
    SQL.Clear;
    SQL.Text := 'Select * from unit where chType=0';
    Open;

    if 0 <> RecordCount then
     begin
       
       Self.ComboBox2.Clear;

       while not Eof do
       begin
         s := FieldByName('name').AsString;
         Self.cxDBComboBox1.Properties.Items.Add(s);
         Self.ComboBox2.Items.Add( s );
         Next;
       end;
       Self.ComboBox2.ItemIndex := 0;
     end;

  end;

end;

procedure TfrmAccountdetails.RzBitBtn17Click(Sender: TObject);
var
  s : string;
  szPayee: string;
  szCaption: string;
  szContent : string;
  szPrice: Currency;
  szNum  : Single;
  szRemarks: string;
  szUnit   : string;
  szPic    : string;
  szPhoto  : string;
  szPayType: Integer;
  szTruck  : Integer;
  szDate   : TDateTime;
  i : Integer;
  pData : PPaytype;
  parent: string;

begin
  if Length(g_PostCode) = 0 then
  begin
    Application.MessageBox('请选择合同',m_title,MB_OK + MB_ICONHAND);
  end
  else
  begin
    if DM.IsNumbers(g_TableName,g_PostNumbers) then
    begin
       Application.MessageBox('编号已存在',m_title,MB_OK + MB_ICONHAND);
       Exit;
    end else
    begin
      szUnit := Self.ComboBox2.Text;
      if Length(szUnit) = 0 then
      begin
        Application.MessageBox('请选择单位',m_title,MB_OK + MB_ICONHAND);
        Self.ComboBox2.DroppedDown := True;
      end
      else
      begin
        szPrice := Self.cxCurrencyEdit1.Value;
           szNum   := Self.cxCurrencyEdit2.Value;
           if szNum = 0 then
           begin
             Application.MessageBox('请输入数量',m_title,MB_OK + MB_ICONHAND);
           end
           else
           begin

               szCaption := Self.RzEdit3.Text;  //名称
               szContent := Self.RzEdit12.Text; //内容
               szRemarks := Self.RzMemo2.Text;  //备注
               szTruck   := 1; //车等于 1
               szDate    := AddStartDate.DateTime;
               parent    := ''; //主合同

                if DM.AddPayInfo(g_TableName,
                                  g_PostCode,
                                      parent,
                                      szCaption,
                                      szContent,
                                      szUnit,
                                      szPhoto,
                                      szRemarks,
                                      szDate,
                                      AddEndDate.DateTime,
                                      szPrice,
                                      szTruck,
                                      g_PostNumbers,
                                      szNum,
                                      g_pacttype) <> 0 then
                begin
                  Self.FormActivate(Sender);
                  Application.MessageBox('添加成功',m_title,MB_OK + MB_ICONQUESTION );
                end
                else
                begin
                  Application.MessageBox('添加失败',m_title,MB_OK + MB_ICONQUESTION );
                end;
                      
           end;
      end;
    end;

  end;

end;

procedure TfrmAccountdetails.RzBitBtn1Click(Sender: TObject);
var
  s : string;
begin
  //打开目录
  s := GetEnclosurePath;
  CreateOpenDir(Handle,s,True);
end;

procedure TfrmAccountdetails.RzBitBtn3Click(Sender: TObject);
begin
  Self.ADOQuery1.UpdateBatch(arAll);
  Application.MessageBox('编辑完成',m_title,MB_OK + MB_ICONQUESTION);
end;

procedure TfrmAccountdetails.RzBitBtn6Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmAccountdetails.RzEdit5KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 13 then
  begin
    Self.RzBitBtn17.Click;
  end;  
end;

end.

