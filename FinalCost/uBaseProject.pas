unit uBaseProject;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, Vcl.ComCtrls, dxCore,
  cxDateUtils, cxStyles, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxNavigator, Data.DB, cxDBData, cxCalendar, cxTextEdit,
  cxCurrencyEdit, cxSpinEdit, cxDropDownEdit, cxMemo, cxGridLevel,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxClasses,
  cxGridCustomView, cxGrid, cxMaskEdit, cxLabel, RzTabs, RzPanel, RzButton,
  Vcl.ExtCtrls, Vcl.Menus, Vcl.StdCtrls, cxButtons, cxGroupBox,ufrmBaseController,
  cxDBEdit;

type
  TfrmBaseProject = class(TfrmBaseController)
    Left: TPanel;
    TreeView1: TTreeView;
    RzToolbar3: TRzToolbar;
    RzSpacer16: TRzSpacer;
    RzToolButton17: TRzToolButton;
    RzSpacer17: TRzSpacer;
    RzToolButton18: TRzToolButton;
    RzSpacer18: TRzSpacer;
    RzToolButton19: TRzToolButton;
    RzPanel19: TRzPanel;
    RzPanel3: TRzPanel;
    RzPanel2: TRzPanel;
    RzPanel15: TRzPanel;
    RzPanel17: TRzPanel;
    RzPanel18: TRzPanel;
    RzPanel16: TRzPanel;
    RzPanel5: TRzPanel;
    RzPanel11: TRzPanel;
    RzPanel12: TRzPanel;
    RzPanel13: TRzPanel;
    RzPanel14: TRzPanel;
    RzPanel6: TRzPanel;
    RzPanel7: TRzPanel;
    RzPanel9: TRzPanel;
    RzPanel10: TRzPanel;
    RzPanel8: TRzPanel;
    pcSys: TRzPageControl;
    TabSheet1: TRzTabSheet;
    RzToolbar1: TRzToolbar;
    RzSpacer1: TRzSpacer;
    RzToolButton1: TRzToolButton;
    RzSpacer2: TRzSpacer;
    RzToolButton2: TRzToolButton;
    RzSpacer3: TRzSpacer;
    RzToolButton3: TRzToolButton;
    RzSpacer4: TRzSpacer;
    RzToolButton4: TRzToolButton;
    RzToolButton5: TRzToolButton;
    RzToolButton6: TRzToolButton;
    RzSpacer7: TRzSpacer;
    RzSpacer19: TRzSpacer;
    RzSpacer20: TRzSpacer;
    RzSpacer21: TRzSpacer;
    RzSpacer11: TRzSpacer;
    RzToolButton15: TRzToolButton;
    cxLabel1: TcxLabel;
    cxDateEdit1: TcxDateEdit;
    cxLabel2: TcxLabel;
    cxDateEdit2: TcxDateEdit;
    cxGrid2: TcxGrid;
    tvDealGrid: TcxGridDBTableView;
    tvDealGridColumn1: TcxGridDBColumn;
    tvDealGridColumn2: TcxGridDBColumn;
    tvDealGridColumn3: TcxGridDBColumn;
    tvDealGridColumn4: TcxGridDBColumn;
    tvDealGridColumn5: TcxGridDBColumn;
    tvDealGridColumn6: TcxGridDBColumn;
    tvDealGridColumn7: TcxGridDBColumn;
    tvDealGridColumn8: TcxGridDBColumn;
    tvDealGridColumn9: TcxGridDBColumn;
    tvDealGridColumn15: TcxGridDBColumn;
    tvDealGridColumn16: TcxGridDBColumn;
    Lv1: TcxGridLevel;
    Splitter1: TSplitter;
    tvDealGridColumn18: TcxGridDBColumn;
    tvDealGridColumn19: TcxGridDBColumn;
    RzPanel1: TRzPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cxGroupBox1: TcxGroupBox;
    cxLabel6: TcxLabel;
    cxLabel7: TcxLabel;
    cxLabel5: TcxLabel;
    cxLabel4: TcxLabel;
    cxLabel3: TcxLabel;
    cxLabel8: TcxLabel;
    cxLabel9: TcxLabel;
    tvDealGridColumn10: TcxGridDBColumn;
    cxDBCurrencyEdit1: TcxDBCurrencyEdit;
    cxDBTextEdit1: TcxDBTextEdit;
    cxDBSpinEdit1: TcxDBSpinEdit;
    cxDBTextEdit2: TcxDBTextEdit;
    cxDBDateEdit1: TcxDBDateEdit;
    cxDBDateEdit2: TcxDBDateEdit;
    cxDBCurrencyEdit2: TcxDBCurrencyEdit;
    procedure RzToolButton6Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmBaseProject: TfrmBaseProject;

implementation

uses
   uDataModule;

{$R *.dfm}

procedure TfrmBaseProject.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmBaseProject.RzToolButton6Click(Sender: TObject);
begin
  Close;
end;

end.
